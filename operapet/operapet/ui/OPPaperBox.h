//
//  OPPaperBox.h
//  operapet
//
//  Created by Gary KK on 21/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface OPPaperBox : UIView

@property (nonatomic) BOOL shouldDrawDepth;

@end
