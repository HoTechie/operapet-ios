//
//  OPCircleButton.m
//  myislands
//
//  Created by Gary KK on 27/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "OPCircleButton.h"
#import "Constants.h"

@implementation OPCircleButton

- (void)awakeFromNib {
    [super awakeFromNib];
    self.tintColor = [UIColor clearColor];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(c, UIColorFromRGB(0xaaaaaa).CGColor);
    CGContextFillEllipseInRect(c, rect);
}


@end
