//
//  RatingLowerView.h
//  myislands
//
//  Created by alan tsoi on 6/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MIRateView.h"

@interface RatingLowerView : UIView

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet MIRateView *ratingView;

@end
