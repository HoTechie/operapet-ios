//
//  WhereToStayView.h
//  myislands
//
//  Created by alan tsoi on 6/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WhereToStayView : UIView

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;

@end
