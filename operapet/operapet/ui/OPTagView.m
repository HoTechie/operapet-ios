//
//  OPTagView.m
//  operapet
//
//  Created by Gary KK on 14/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPTagView.h"

@implementation OPTagView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setOpTag:(OPTag *)opTag {
    _opTag = opTag;
    self.lblTag.text = opTag._description;
}

@end
