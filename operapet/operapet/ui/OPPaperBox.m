//
//  OPPaperBox.m
//  operapet
//
//  Created by Gary KK on 21/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPaperBox.h"

@implementation OPPaperBox

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    self.shouldDrawDepth = YES;
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    self.layer.masksToBounds = NO;
    self.layer.shadowOffset = CGSizeMake(0, 0);
    self.layer.shadowRadius = 4;
    self.layer.shadowOpacity = 0.25;
    
    CGRect tRect = CGRectInset(rect, 5, 0);
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:tRect];
    
    CGFloat xpad = 4;
    CGContextRef c = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(c, [UIColor whiteColor].CGColor);
    CGContextAddPath(c, path.CGPath);
    CGContextFillPath(c);
    
    if(self.shouldDrawDepth) {
        CGContextSetFillColorWithColor(c, [UIColor colorWithWhite:0.95 alpha:1.0].CGColor);
        CGContextFillRect(c, CGRectMake(tRect.origin.x, tRect.origin.y, xpad, tRect.size.height));
        CGContextFillRect(c, CGRectMake(tRect.origin.x + tRect.size.width - xpad, tRect.origin.y, xpad, tRect.size.height));
    }
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [self setNeedsDisplay];
}

-(void)setShouldDrawDepth:(BOOL)shouldDrawDepth {
    _shouldDrawDepth = shouldDrawDepth;
    [self setNeedsDisplay];
}

@end
