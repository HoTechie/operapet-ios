//
//  MITightLayout.m
//  myislands
//
//  Created by alan tsoi on 7/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "MITightLayout.h"
#import "BaseCellFactory.h"

@interface MITightLayout ()
@property NSMutableArray *cachedAttr;
@property CGFloat contentHeight;
@end

@implementation MITightLayout

-(void)prepareLayout {
    id delegate = self.collectionView.delegate;
    _cachedAttr = [NSMutableArray array];
    
    NSInteger num_col = 2;
    CGFloat offsetY[num_col];
    
    for(NSInteger i = 0; i < num_col; i++) {
        offsetY[i] = 0;
    }

    NSInteger col = 0;
    for(NSInteger i = 0; i < [self.collectionView numberOfItemsInSection:0]; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
        UICollectionViewLayoutAttributes *attrs = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
        CGFloat lineSpacing = [delegate collectionView:self.collectionView layout:self minimumLineSpacingForSectionAtIndex:i];
        CGSize cellSize = [delegate collectionView:self.collectionView layout:self sizeForItemAtIndexPath:indexPath];
        
        CGRect frame;
        frame.size = cellSize;
        
        CGFloat offsetX = self.sectionInset.left + col * (self.collectionView.frame.size.width / num_col);
        frame.origin = CGPointMake(offsetX, offsetY[col]);
        attrs.frame = frame;
        offsetY[col] += cellSize.height + lineSpacing;
        
        col = (col + 1) % num_col;
        
        [_cachedAttr addObject:attrs];
    }
    
    for(NSInteger i = 0; i < num_col; i++) {
        _contentHeight = MAX(_contentHeight, offsetY[i]);
    }
}

-(CGSize)collectionViewContentSize {
    return CGSizeMake(self.collectionView.frame.size.width, _contentHeight);
}

- (NSArray<__kindof UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSMutableArray *result = [NSMutableArray array];
    for(UICollectionViewLayoutAttributes *attr in self.cachedAttr) {
        if(CGRectIntersectsRect(attr.frame, rect)) [result addObject:attr];
    }
    
    return result;
}

@end
