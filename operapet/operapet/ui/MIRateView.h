//
//  MIRateView.h
//  myislands
//
//  Created by user120226 on 9/4/16.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface MIRateView : UIView

@property IBInspectable UIImage *imgScored;
@property IBInspectable UIImage *imgUnScored;
@property (nonatomic) NSInteger score;

@end
