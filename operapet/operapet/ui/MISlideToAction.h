//
//  MISlideToAction.h
//  myislands
//
//  Created by Gary KK on 30/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    MISlideToActionStateTap,
    MISlideToActionStateSlideIdle,
    MISlideToActionStateSlideSliding,
    MISlideToActionSlided
} MISlideToActionState;

typedef void(^ActionCallback)();

@interface MISlideToAction : UIView

@property MISlideToActionState state;
@property NSString * titleTap;
@property NSString * titleSlide;
@property ActionCallback action;
@property ActionCallback tapAction;

@end
