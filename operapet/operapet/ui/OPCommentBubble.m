//
//  OPCommentBubble.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPCommentBubble.h"

@implementation OPCommentBubble

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    self.layer.masksToBounds = NO;
    self.layer.shadowOffset = CGSizeMake(0, 0);
    self.layer.shadowRadius = 3;
    self.layer.shadowOpacity = 0.25;
    
    CGFloat r = 8;
    CGFloat i = 4;
    CGFloat x = rect.origin.x;
    CGFloat y = rect.origin.y;
    CGFloat h = rect.size.height;
    CGFloat w = rect.size.width;
    CGFloat xl = 15;
    CGFloat yl = 15;
    CGFloat yo = 40;
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(x + i + xl, y + i, w - i * 2 - xl, h - i * 2) cornerRadius:r];
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextAddPath(c, path.CGPath);
    
    CGContextSetFillColorWithColor(c, [UIColor whiteColor].CGColor);
    CGContextFillPath(c);
    
    CGContextBeginPath(c);
    CGContextMoveToPoint(c, x + i, y + i + yo);
    CGContextAddLineToPoint(c, x + i + xl, y + i + yo - yl);
    CGContextAddLineToPoint(c, x + i + xl, y + i + yo + yl);
    CGContextClosePath(c);
    CGContextFillPath(c);
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [self setNeedsDisplay];
}

@end
