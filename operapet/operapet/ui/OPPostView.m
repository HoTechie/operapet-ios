//
//  OPPostView.m
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPostView.h"
#import "UIImageView+WebCache.h"

#import "SessionManager.h"
#import "OPServiceManager.h"

#import "OPNavigationController.h"
#import "OPWallViewController.h"

#import "Constants.h"

@interface OPPostView ()

@end

@implementation OPPostView

-(void)awakeFromNib {
    [super awakeFromNib];
    
    self.imgActionBarBg.image = [self.imgActionBarBg.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.btnLike setBackgroundImage:[self.btnLike.currentBackgroundImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.btnComment setBackgroundImage:[self.btnComment.currentBackgroundImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.btnShare setBackgroundImage:[self.btnShare.currentBackgroundImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    
    self.tagFactory = [[OPTagFactory alloc]initWithCollectionView:self.collectionTags];
    
    [self.btnLike addTarget:self action:@selector(onClickBtnLike) forControlEvents:UIControlEventTouchUpInside];
    [self.btnComment addTarget:self action:@selector(onClickBtnComment) forControlEvents:UIControlEventTouchUpInside];
    [self.btnShare addTarget:self action:@selector(onClickBtnShare) forControlEvents:UIControlEventTouchUpInside];
}

- (void)onClickBtnLike {
    if(self.onLike) self.onLike(self.post);
}

- (void)onClickBtnShare {
    if(self.onShare) self.onShare(self.post);
}

- (void)onClickBtnComment {
    if(self.onComment) self.onComment(self.post);
}

- (void)onClickProfilePic {
    [[OPNavigationController sharedInstance]pushViewController:[[OPWallViewController alloc]initWithProfileId:self.post.profile.id] animated:YES];
}

- (void)setPost:(OPPost *)post {
    _post = post;
    OPPostCategory *cat = [[SessionManager sharedInstance]postCategoryForId:post.category_id];
    
    self.imgIconCategory.image = [[UIImage imageNamed:cat.iconFileName]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.lblCategory.text = cat.name;
    [self.imgProfilePic sd_setImageWithURL:[NSURL URLWithString:[[OPServiceManager sharedInstance]getProfileImageURLForId:post.profile.image_id]]];
    [self.btnProfilePic addTarget:self action:@selector(onClickProfilePic) forControlEvents:UIControlEventTouchUpInside];
    self.lblName.text = post.profile.user_name;
    self.lblMemberLevel.text = [[SessionManager sharedInstance]memberLevelString:post.profile.member_level];
    
    self.lblDate.text = [[SessionManager sharedInstance]formattedDateString:post.create_time];
    self.lblContent.text = post._description;
    
    /* Tag */
    [self.tagFactory loadData:post.tags ? post.tags : @[]];
    
    /* Image */
    if(post.images && post.images.count) {
        self.imgPost.hidden = NO;
        self.constraintImageHeight.active = NO;
        [self.imgPost sd_setImageWithURL:[NSURL URLWithString:[[OPServiceManager sharedInstance]getPostImageURLForId:[post.images.firstObject integerValue]]]];
    } else {
        self.imgPost.hidden = YES;
        self.constraintImageHeight.active = YES;
    }
    
    /* Action Bar */
    self.lblLikeCount.text = [NSString stringWithFormat:ls(@"post_like_format"), (int)post.no_of_like];
    self.lblCommentCount.text = [NSString stringWithFormat:ls(@"post_comment_format"), (int)post.no_of_comment];
    
    [self setCategory:cat];
}

- (void)setCategory:(OPPostCategory *)cat {
    self.lblCategory.textColor = cat.color;
    self.imgIconCategory.tintColor = cat.color;
    self.viewCatSeparator.backgroundColor = cat.color;
    self.imgActionBarBg.tintColor = cat.color;
    
    self.btnLike.tintColor = self.post.is_like ? color_main_text_color_invert : cat.colorDarker;
    self.btnComment.tintColor = cat.colorDarker;
    self.btnShare.tintColor = cat.colorDarker;
    self.lblCommentCount.textColor = cat.colorDarker;
    self.lblLikeCount.textColor = cat.colorDarker;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
