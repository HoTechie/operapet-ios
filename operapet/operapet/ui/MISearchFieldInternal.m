//
//  MISearchFieldInternal.m
//  myislands
//
//  Created by alan tsoi on 3/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "MISearchFieldInternal.h"

@interface MISearchFieldInternal()<UITextFieldDelegate>

@end

@implementation MISearchFieldInternal

-(id)initWithParentView:(UIView *)parent {
    self = [super init];
    
    self.view = [[[NSBundle mainBundle]loadNibNamed:NSStringFromClass([self class]) owner:self options:nil]firstObject];
    self.txtSearchField.delegate = self;
    [self.txtSearchField setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [parent addSubview:self.view];
    [self.view setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *constraint;
    constraint = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:parent attribute:NSLayoutAttributeTop multiplier:1. constant:0.0];
    [parent addConstraint:constraint];
    constraint = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:parent attribute:NSLayoutAttributeLeading multiplier:1. constant:0.0];
    [parent addConstraint:constraint];
    constraint = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual
                                                 toItem:parent attribute:NSLayoutAttributeTrailing multiplier:1. constant:0.0];
    [parent addConstraint:constraint];
    constraint = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:parent attribute:NSLayoutAttributeBottom multiplier:1. constant:0.0];
    [parent addConstraint:constraint];
    
    return self;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *result = [textField.text stringByReplacingCharactersInRange:range withString:string];
    self.imgMagnifyGlass.hidden = result.length;
    return YES;
}

/*
-(void)textFieldDidEndEditing:(UITextField *)textField {
    self.imgMagnifyGlass.hidden = textField.text.length;
}*/

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if(self.onDone)self.onDone();
    return NO;
}

@end
