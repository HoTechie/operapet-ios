//
//  MISearchField.h
//  myislands
//
//  Created by alan tsoi on 3/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MISearchFieldInternal.h"

@interface MISearchField : UIView

@property MISearchFieldInternal *internal;
@property (nonatomic) NSString *text;
@property (nonatomic) BOOL showBorder;
@property (copy) void (^onDone)();

@end
