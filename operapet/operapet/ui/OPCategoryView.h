//
//  OPCategoryView.h
//  operapet
//
//  Created by Gary KK on 13/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "HTView.h"
#import "OPRoundedView.h"
#import "OPPostCategory.h"

@interface OPCategoryView : HTView

@property (nonatomic) BOOL selected;

@property (strong, nonatomic) IBOutlet UIImageView *imgIcon;
@property (strong, nonatomic) IBOutlet UIImageView *imgIndicator;
@property (strong, nonatomic) IBOutlet OPRoundedView *roundedView;
@property (strong, nonatomic) IBOutlet UILabel *lblCatName;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintHideIcon;
@property (nonatomic) OPPostCategory *postCat;



@end
