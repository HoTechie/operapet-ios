//
//  MIRateView.m
//  myislands
//
//  Created by user120226 on 9/4/16.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "MIRateView.h"

@implementation MIRateView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

-(void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
}


- (void)drawRect:(CGRect)rect {
    CGFloat dimen = rect.size.width / 5.0;
    CGFloat x = self.imgUnScored ? 0.0 : rect.size.width - dimen;
    
    for (NSInteger i = 1; i <= self.score && self.imgScored; i++) {
        [self.imgScored drawInRect:CGRectMake(x, 0, dimen, dimen)];
        
        if(self.imgUnScored) x += dimen;
        else x -= dimen;
    }
    
    for (NSInteger i = 1; (i <= (5 - self.score)) && self.imgUnScored; i++) {
        [self.imgUnScored drawInRect:CGRectMake(x, 0, dimen, dimen)];
        x += dimen;
    }
}


- (void)prepareForInterfaceBuilder {
    self.score = 5;
}

- (void)setScore:(NSInteger)score {
    _score = score;
    [self setNeedsDisplay];
}

@end
