//
//  OPPetCell.h
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPPet.h"
#import "OPPetBorderView.h"

@interface OPPetCell : UICollectionViewCell

@property (nonatomic) OPPet *pet;

@property (strong, nonatomic) IBOutlet UIImageView *imgThumbnail;
@property (strong, nonatomic) IBOutlet OPPetBorderView *petBorderView;


@end
