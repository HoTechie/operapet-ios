//
//  OPPostDetailIdentityCell.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPPost.h"

@interface OPPostDetailIdentityCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgProfilePic;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblMemberLevel;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;

@property (nonatomic) OPPost *post;

@end
