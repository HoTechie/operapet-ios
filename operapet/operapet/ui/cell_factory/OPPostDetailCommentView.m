//
//  OPPostDetailCommentView.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPostDetailCommentView.h"
#import "OPServiceManager.h"
#import "SessionManager.h"
#import "Constants.h"
#import "UIImageView+WebCache.h"

@implementation OPPostDetailCommentView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.btnLike setBackgroundImage:[self.btnLike.currentBackgroundImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.imgProfilePic.layer.masksToBounds = YES;
    
    [self.btnLike addTarget:self action:@selector(onClickBtnLike) forControlEvents:UIControlEventTouchUpInside];
}

- (void)onClickBtnLike {
    if(self.onLikeComment) self.onLikeComment(self.comment);
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.imgProfilePic.layer.cornerRadius = self.imgProfilePic.frame.size.height / 2.0;
}

-(void)setComment:(OPComment *)comment {
    _comment = comment;
    
    [self.imgProfilePic sd_setImageWithURL:urlprofilepic(comment.profile.image_id)];
    self.lblName.text = comment.profile.user_name;
    self.lblDate.text = [[SessionManager sharedInstance]formattedDateString:comment.create_time];
    self.lblText.text = comment._description;
    self.lblLikeCount.text = [@(comment.no_of_like) stringValue];
    self.btnLike.tintColor = comment.is_like ? color_theme_color : color_main_text_color_lighter;
    
    [self layoutIfNeeded];
}

@end
