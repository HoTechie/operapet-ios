//
//  OPPostDetailActionBar.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPPost.h"

@interface OPPostDetailActionBar : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIButton *btnLike;
@property (strong, nonatomic) IBOutlet UIButton *btnShare;

@property (strong, nonatomic) IBOutlet UILabel *lblLikeCount;
@property (strong, nonatomic) IBOutlet UILabel *lblCommentCount;

@property (nonatomic) OPPost *post;

@property void(^onLike)(OPPost *post);
@property void(^onShare)(OPPost *post);



@end
