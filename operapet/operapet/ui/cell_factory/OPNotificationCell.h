//
//  OPNotificationCell.h
//  operapet
//
//  Created by Gary KK on 19/4/2017.
//  Copyright © 2017 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OPNotificationCell : UICollectionViewCell


@property (strong, nonatomic) IBOutlet UIImageView *imgThumbnail;
@property (strong, nonatomic) IBOutlet UILabel *lblDescription;

- (void)setImageURL:(NSString *)imageUrl description:(NSString *)description;

@end
