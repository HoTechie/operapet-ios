//
//  OPPetDetailBMIRecordsCell.h
//  operapet
//
//  Created by Gary KK on 18/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPPet.h"

@interface OPPetDetailBMIRecordsCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UITextField *txtBMI1Year;
@property (strong, nonatomic) IBOutlet UITextField *txtBMI1Month;
@property (strong, nonatomic) IBOutlet UITextField *txtBMI1Day;
@property (strong, nonatomic) IBOutlet UITextField *txtBMI1Value;

@property (strong, nonatomic) IBOutlet UITextField *txtBMI2Year;
@property (strong, nonatomic) IBOutlet UITextField *txtBMI2Month;
@property (strong, nonatomic) IBOutlet UITextField *txtBMI2Day;
@property (strong, nonatomic) IBOutlet UITextField *txtBMI2Value;

@property (strong, nonatomic) IBOutlet UITextField *txtBMI3Year;
@property (strong, nonatomic) IBOutlet UITextField *txtBMI3Month;
@property (strong, nonatomic) IBOutlet UITextField *txtBMI3Day;
@property (strong, nonatomic) IBOutlet UITextField *txtBMI3Value;

@property (nonatomic) OPPet *pet;


@end
