//
//  ReviewDetailImageCell.m
//  myislands
//
//  Created by alan tsoi on 9/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "ReviewDetailImageCell.h"

@implementation ReviewDetailImageCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib {
    [super awakeFromNib];
    [self.img setContentMode:UIViewContentModeScaleAspectFit];
}

-(UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    layoutAttributes.frame = CGRectMake(layoutAttributes.frame.origin.x, layoutAttributes.frame.origin.y, layoutAttributes.frame.size.width, layoutAttributes.frame.size.width * 9 / 16 * 0.8);
    return layoutAttributes;
}

@end
