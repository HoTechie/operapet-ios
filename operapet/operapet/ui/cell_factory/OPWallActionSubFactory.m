//
//  OPWallActionSubFactory.m
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPWallActionSubFactory.h"
#import "SessionManager.h"

#import "OPNavigationController.h"
#import "OPPetListViewController.h"
#import "OPWallFriendViewController.h"
#import "OPMemberService.h"

@implementation OPWallActionSubFactory

-(id)initWithCollectionView:(UICollectionView *)collectionView {
    self = [super initWithCollectionView:collectionView];
    ((UICollectionViewFlowLayout *)collectionView.collectionViewLayout).scrollDirection = UICollectionViewScrollDirectionHorizontal;
    return self;
}

- (void)setProfile:(OPProfile *)profile {
    _profile = profile;
    [self updateUI];
}

- (NSArray *)cellNibs {
    return @[NSStringFromClass([OPWallActionSubCell class])];
}

- (UIEdgeInsets)listInset {
    return UIEdgeInsetsZero;
}

- (CGFloat)cellInterSpacing {
    return 0.0;
}

- (CGFloat)cellLineSpacing {
    return 0.0;
}

- (void)updateUI {
    NSMutableArray *cms = [NSMutableArray array];
    CellModel *cm;
    if([SessionManager sharedInstance].currentProfile.id != self.profile.id) {
        cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPWallActionSubCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPWallActionSubCell class]) forIndexPath:indexPath];
            [cell setIcon:[UIImage imageNamed:@"icon_tab_friend"]];
            [cell setTitleUnselected:self.profile.isPendingFriend ? self.profile.isWaitingMyResponse ? ls(@"wall_add_friend_pending_my_response") : ls(@"wall_add_friend_pending")
                                    : ls(@"wall_add_friend")];
            [cell setTitleSelected:ls(@"wall_add_friend_confirmed")];
            [cell setChecked:self.profile.isFriend];
            if(self.profile.isPendingFriend) cell.imgIcon.tintColor = color_theme_color_darker;
            
            return cell;
        } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
            return CGSizeMake(pWidth/cms.count, pHeight);
        } action:^(CellModel *model) {
            if(!self.profile.isPendingFriend && !self.profile.isFriend && self.onAddFriend) self.onAddFriend();
            else if([self.profile isWaitingMyResponse]) self.onAcceptFriend();
            else if([self.profile isFriend]) self.onDeleteFriend();
        }];
        [cms addObject:cm];
    }
    
    cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPWallActionSubCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPWallActionSubCell class]) forIndexPath:indexPath];
        [cell setIcon:[UIImage imageNamed:@"icon_pet"]];
        [cell setTitleUnselected:ls(@"wall_pet")];
        [cell setChecked:NO];
        return cell;
    } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
        return CGSizeMake(pWidth/cms.count, pHeight);
    } action:^(CellModel *model) {
        OPViewController *vc = [[OPPetListViewController alloc]initWithProfileId:self.profile.id];
        [[OPNavigationController sharedInstance]pushViewController:vc animated:YES];
    }];
    [cms addObject:cm];
    
    cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPWallActionSubCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPWallActionSubCell class]) forIndexPath:indexPath];
        [cell setIcon:[UIImage imageNamed:@"icon_tab_friend"]];
        [cell setTitleUnselected:ls(@"wall_friend")];
        [cell setChecked:NO];
        return cell;
    } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
        return CGSizeMake(pWidth/cms.count, pHeight);
    } action:^(CellModel *model) {
        OPViewController *vc = [[OPWallFriendViewController alloc]initWithProfileId:self.profile.id];
        [[OPNavigationController sharedInstance]pushViewController:vc animated:YES];
    }];
    [cms addObject:cm];
    
    
    [self loadViewModels:cms];
}

@end
