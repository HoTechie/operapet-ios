//
//  OPPostListCellFactory.m
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPostListCellFactory.h"
#import "ImageCell.h"
#import "OPPostDetailViewController.h"
#import "OPAdService.h"
#import "UIImageView+WebCache.h"

//455 * 151
@implementation OPPostListCellFactory

- (NSArray *)cellNibs {
    return @[NSStringFromClass([ImageCell class])];
}

- (NSArray *)cellClasses {
    return @[[OPPostListCell class]];
}

- (UIEdgeInsets)listInset {
    return UIEdgeInsetsMake(0, 10, 20, 10);
}

- (void)loadData:(NSArray *)data {
    [self loadViewModels:[self cellModelsForPosts:data ads:self.ads]];
}

- (void)tryFetchAdsWithData:(NSArray *)data {
    [[[OPAdService alloc]init]getAdvListByType:[[HTListResponseHandler alloc]initWithOnResult:^(NSArray *array) {
        self.ads = array;
        [self loadData:data];
    } onFailure:^(NSInteger code, NSString *str) {
        
    }] type_id:ADV_TYPE_POST_LIST];
}

-(NSArray *)cellModelsForPosts:(NSArray *)data ads:(NSArray *)ads {
    if(!ads) {
        [self tryFetchAdsWithData:data];
    }
    
    NSMutableArray *cms = [NSMutableArray array];
    CellModel *cm;
    
    for(NSInteger i = 0; i < data.count; i++) {
        OPPost *post = data[i];
        
        if((i == 5 || i == 15 || i == 25 || i == 35) && ads && ads.count) {
            NSInteger index = MIN(i / 10, ads.count - 1);
            OPAdvert *ad = ads[index];
            
            if(ad.image_id > 0) {
                [cms addObject:[CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
                    ImageCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ImageCell class]) forIndexPath:indexPath];
                    cell.img.contentMode = UIViewContentModeScaleAspectFill;
                    [cell.img sd_setImageWithURL:[NSURL URLWithString:[[OPServiceManager sharedInstance]getAdvImageURLForId:ad.image_id]]];
                    return cell;
                } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
                    return CGSizeMake(pWidth, pWidth * (151.0/455));
                } action:^(CellModel *model) {
                    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:ad.link]];
                }]];
            }
        }
        
        cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPPostListCell *cell = [parent dequeueReusableCellWithReuseIdentifier:@"OPPostListCell" forIndexPath:indexPath];
            [cell.postView setPost:post];
            cell.postView.onLike = self.onLike;
            cell.postView.onShare = self.onShare;
            cell.postView.onComment = self.onComment;
            return cell;
        } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
            self.templateView = self.templateView ? self.templateView : [[NSBundle mainBundle]loadNibNamed:@"OPPostView" owner:nil options:0].firstObject;
            [self.templateView setPost:post];
            [self.templateView setMaxWidth:pWidth];
            
            CGFloat height = [self.templateView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
            return CGSizeMake(pWidth, height);
        } action:^(CellModel *model) {
            [[OPNavigationController sharedInstance]pushViewController:[[OPPostDetailViewController alloc]initWithPostId:post.id] animated:YES];
        }];
        
        [cms addObject:cm];
    }
    
    return cms;
}

- (CGFloat)cellHeight {
    return 200.0;
}

@end
