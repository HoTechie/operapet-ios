//
//  OPWallFactory.m
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPWallFactory.h"

@implementation OPWallFactory

-(UIEdgeInsets)listInset {
    return UIEdgeInsetsZero;
}

-(CGFloat)cellLineSpacing {
    return 0.0;
}

-(CGFloat)cellInterSpacing {
    return 0.0;
}

-(NSArray *)cellNibs {
    NSMutableArray * nibs = [NSMutableArray arrayWithArray:[super cellNibs]];
    [nibs addObjectsFromArray:@[
                                NSStringFromClass([OPWallOverviewCell class]),
                                NSStringFromClass([OPWallActionCell class]),
                                ]];
    return nibs;
}

-(void)setProfile:(OPProfile *)profile {
    _profile = profile;
    [self updateUI];
}
-(void)loadData:(NSArray *)data {
    self.posts = data;
    [self updateUI];
}

-(void)updateUI {
    NSMutableArray *cms = [NSMutableArray array];
    CellModel *cm;
    
    if(self.profile) {
        cm = [CellModel modelWithData:self renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPWallOverviewCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPWallOverviewCell class]) forIndexPath:indexPath];
            cell.profile = self.profile;
            return cell;
        } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
            return CGSizeMake(pWidth, 230);
        } action:nil];
        [cms addObject:cm];
        
        cm = [CellModel modelWithData:self renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPWallActionCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPWallActionCell class]) forIndexPath:indexPath];
            cell.factory.onAddFriend = self.onAddFriend;
            cell.factory.onAcceptFriend = self.onAcceptFriend;
            cell.factory.onDeleteFriend = self.onDeleteFriend;
            cell.profile = self.profile;
            return cell;
        } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
            return CGSizeMake(pWidth, 70);
        } action:nil];
        [cms addObject:cm];
    }
    
    [cms addObjectsFromArray:[self cellModelsForPosts:self.posts ads:self.ads]];
    [self loadViewModels:cms];
}

@end
