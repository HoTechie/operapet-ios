//
//  OPGiftPointsCell.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPGiftPointsCell.h"
#import "SessionManager.h"

@implementation OPGiftPointsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.viewInnerCircle.layer.masksToBounds = YES;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.viewInnerCircle.layer.cornerRadius = CGRectGetHeight(self.viewInnerCircle.frame) / 2.0;
}

- (void)setProfile:(OPProfile *)profile {
    self.lblPoints.text = [@(profile.remain_credit) stringValue];
}



@end
