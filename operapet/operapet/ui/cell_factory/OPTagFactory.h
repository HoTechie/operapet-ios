//
//  OPTagFactory.h
//  operapet
//
//  Created by Gary KK on 14/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListCellFactory.h"
#import "OPTagCell.h"

@interface OPTagFactory : BaseListCellFactory

@end
