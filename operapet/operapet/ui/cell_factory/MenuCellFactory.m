//
//  MenuCellFactory.m
//  operapet
//
//  Created by Gary KK on 6/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "MenuCellFactory.h"


@implementation MenuCellFactory

-(CGFloat)cellHeight {
    return 55.0;
}

-(void)loadData:(NSArray *)data {
    self.models = data;
}

-(NSArray *)cellNibs {
    return @[@"TextCell",@"MenuCell",@"OPMenuUserCell"];
}

-(UIEdgeInsets)listInset {
    return UIEdgeInsetsZero;
}

@end
