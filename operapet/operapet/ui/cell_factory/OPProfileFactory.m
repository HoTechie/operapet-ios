//
//  OPProfileFactory.m
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPProfileFactory.h"
#import "UIImageView+WebCache.h"
#import "OPMemberLevelViewController.h"


@implementation OPProfileFactory

-(UIEdgeInsets)listInset {
    return UIEdgeInsetsZero;
}

-(CGFloat)cellLineSpacing {
    return 0.0;
}

-(CGFloat)cellInterSpacing {
    return 0.0;
}

-(CGFloat)cellHeight {
    return 35.0;
}

-(NSArray *)cellNibs {
    return @[NSStringFromClass([OPProfileIdentityCell class]),
             NSStringFromClass([OPProfileHeaderCell class]),
             NSStringFromClass([OPProfileStandardCell class]),
             NSStringFromClass([OPProfileMemberCell class]),
             ];
}

- (void)loadProfile:(OPProfile *)profile fromVC:(OPViewController *)vc {
    _profile = [profile copy];
    _vc = vc;
    [self updateUI];
}

- (void)editUsername {
    [self.vc showInputDialog:@"" inputPlaceHolders:@[self.profile.firstname, self.profile.lastname] inputsHandler:^(MessageDialogAlertController *ac, UIAlertAction *action, NSArray *inputs) {
        NSString *firstname = inputs[0];
        NSString *lastname = inputs[1];
        self.profile.firstname = firstname.length ? firstname : self.profile.firstname;
        self.profile.lastname = lastname.length ? lastname : self.profile.lastname;
        [self updateUI];
    } nevigativeHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {
        
    }];
}

- (void)editEmail {
    [self.vc showInputDialog:@"" inputHandler:^(MessageDialogAlertController *ac, UIAlertAction *action, NSString *input) {
        self.profile.email = input.length ? input : self.profile.email;
        [self updateUI];
    } nevigativeHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {
       
    }];
}

- (void)editProfilePic {
    UIImagePickerController *controller = [[UIImagePickerController alloc]init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:controller.sourceType];
    controller.allowsEditing = NO;
    [[OPNavigationController sharedInstance]presentViewController:controller animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *img = [info objectForKey: UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
    self.profileImage = img;
    [self updateUI];
}

- (void)updateUI {
    NSMutableArray *cms = [NSMutableArray array];
    
    OPProfile *profile = self.profile;
    CellModel *cm;
    
    /* Field Entry */
    cm = [CellModel modelWithData:profile renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPProfileIdentityCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileIdentityCell class]) forIndexPath:indexPath];
        cell.profile = profile;
        [cell.btnEdit addTarget:self action:@selector(editUsername) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnProfilePic addTarget:self action:@selector(editProfilePic) forControlEvents:UIControlEventTouchUpInside];
        if(self.profileImage) {
            [cell.imgProfilePic sd_cancelCurrentImageLoad];
            cell.imgProfilePic.image = self.profileImage;
        }
        
        return cell;
    } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
        return CGSizeMake(pWidth, 230);
    } action:nil];
    [cms addObject:cm];
    
    
    /* Field Entry */
    cm = [CellModel modelWithData:profile renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPProfileHeaderCell *headerCell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileHeaderCell class]) forIndexPath:indexPath];
        headerCell.lblTitle.text = ls(@"profile_lb_phone");
        return headerCell;
    } action:nil];
    [cms addObject:cm];
    
    cm = [CellModel modelWithData:profile renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPProfileStandardCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileStandardCell class]) forIndexPath:indexPath];
        cell.imgIcon.image = [UIImage imageNamed:@"icon_phone_orange"];
        cell.lblTitle.text = profile.mobile;
        cell.btnPen.hidden = YES;
        return cell;
    } action:nil];
    [cms addObject:cm];
    
    /* Field Entry */
    cm = [CellModel modelWithData:profile renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPProfileHeaderCell *headerCell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileHeaderCell class]) forIndexPath:indexPath];
        headerCell.lblTitle.text = ls(@"profile_lb_email");
        return headerCell;
    } action:nil];
    [cms addObject:cm];
    
    cm = [CellModel modelWithData:profile renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPProfileStandardCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileStandardCell class]) forIndexPath:indexPath];
        cell.imgIcon.image = [UIImage imageNamed:@"icon_email"];
        cell.lblTitle.text = self.profile.email;
        cell.btnPen.hidden = NO;
        [cell.btnPen addTarget:self action:@selector(editEmail) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    } action:nil];
    [cms addObject:cm];
    
    /* Field Entry */
    cm = [CellModel modelWithData:profile renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPProfileHeaderCell *headerCell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileHeaderCell class]) forIndexPath:indexPath];
        headerCell.lblTitle.text = ls(@"profile_lb_member");
        return headerCell;
    } action:nil];
    [cms addObject:cm];
    
    cm = [CellModel modelWithData:profile renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPProfileMemberCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileMemberCell class]) forIndexPath:indexPath];
        cell.lblMemberLevel.text = self.profile.memberLevelString;
        cell.lblPoint.text = [@(self.profile.credit_total) stringValue];
        cell.onHelp = ^(){
            [[OPNavigationController sharedInstance]pushViewController:[[OPMemberLevelViewController alloc]initWithProfile:profile] animated:YES];
        };
        return cell;
    } action:nil];
    [cms addObject:cm];
    
    /* Field Entry */
    cm = [CellModel modelWithData:profile renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPProfileHeaderCell *headerCell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileHeaderCell class]) forIndexPath:indexPath];
        headerCell.lblTitle.text = ls(@"profile_lb_credit_usable");
        return headerCell;
    } action:nil];
    [cms addObject:cm];
    
    cm = [CellModel modelWithData:profile renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPProfileStandardCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileStandardCell class]) forIndexPath:indexPath];
        cell.imgIcon.image = [UIImage imageNamed:@"icon_credit"];
        cell.lblTitle.text = [@(self.profile.remain_credit) stringValue];
        cell.btnPen.hidden = YES;
        return cell;
    } action:nil];
    [cms addObject:cm];
    

    [self loadViewModels:cms];
    
}

@end
