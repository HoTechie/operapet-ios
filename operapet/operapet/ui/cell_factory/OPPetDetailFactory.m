//
//  OPPetDetailFactory.m
//  operapet
//
//  Created by Gary KK on 18/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPetDetailFactory.h"
#import "SessionManager.h"
#import "ActionSheetDatePicker.h"
#import "OPPetPinCardViewController.h"
#import "UIImageView+WebCache.h"
#import "OPButtonCell.h"
#import "OPPetService.h"

@implementation OPPetDetailFactory

-(UIEdgeInsets)listInset {
    return UIEdgeInsetsZero;
}

-(CGFloat)cellLineSpacing {
    return 0.0;
}

-(CGFloat)cellInterSpacing {
    return 0.0;
}

-(CGFloat)cellHeight {
    return 35.0;
}

-(NSArray *)cellNibs {
    return @[
             NSStringFromClass([OPProfileIdentityCell class]),
             NSStringFromClass([OPProfileHeaderCell class]),
             NSStringFromClass([OPPetDetailStandardCell class]),
             NSStringFromClass([OPPetDetailGenderCell class]),
             NSStringFromClass([OPPetDetailPetTypeCell class]),
             NSStringFromClass([OPPetDetailPinCardCell class]),
             NSStringFromClass([OPPetDetailLastBodyCheckCell class]),
             NSStringFromClass([OPPetDetailBMICalculatorCell class]),
             NSStringFromClass([OPPetDetailBMIRecordsCell class]),
             NSStringFromClass([OPPetDetailPinCardDetailCell class]),
             NSStringFromClass([OPButtonCell class])
             ];
}

-(void)loadPet:(OPPet *)pet fromVC:(OPViewController *)vc onDelete:(void (^)(OPPet *))onDelete {
    _isBMIRecordInserted = NO;
    _pet = pet;
    _vc = vc;
    _onDelete = onDelete;
    [self updateUI];
}

-(BOOL)editable {
    if(!self.pet || ![SessionManager sharedInstance].currentProfile) return NO;
    return self.pet.member_id == [SessionManager sharedInstance].currentProfile.id;
}

-(BOOL)deletable {
    return [self editable] && self.pet.id != PET_ID_NEW;
}

-(void) updateUI {
    __weak OPPetDetailFactory *_self = self;
    NSMutableArray *cms = [NSMutableArray array];
    
    OPPet *pet = self.pet;
    CellModel *cm;
    
    Formater headerFormat = ^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight){
        return CGSizeMake(pWidth, 25);
    };
    
    /* Field Entry */
    cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPProfileIdentityCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileIdentityCell class]) forIndexPath:indexPath];
        cell.pet = pet;
        cell.btnEdit.hidden = ![self editable];
        [cell.btnEdit addTarget:self action:@selector(editPetName) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnProfilePic addTarget:self action:@selector(editProfilePic) forControlEvents:UIControlEventTouchUpInside];
        if(self.petImage){
            [cell.imgProfilePic sd_cancelCurrentImageLoad];
            [cell.imgProfilePic setImage:self.petImage];
        }
        
        return cell;
    } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
        return CGSizeMake(pWidth, 230);
    } action:nil];
    [cms addObject:cm];
    
    
    /* Field Entry */
    cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPProfileHeaderCell *headerCell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileHeaderCell class]) forIndexPath:indexPath];
        headerCell.lblTitle.text = ls(@"pet_lb_gender");
        return headerCell;
    } formater:headerFormat action:nil];
    [cms addObject:cm];
    
    cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPPetDetailGenderCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPPetDetailGenderCell class]) forIndexPath:indexPath];
        [cell.btnMale setUserInteractionEnabled:[self editable]];
        [cell.btnFemale setUserInteractionEnabled:[self editable]];
        [cell setPet:pet];
        [cell setOnChangeGender:^{
            [_self updateUI];
        }];
        return cell;
    } action:nil];
    [cms addObject:cm];
    
    /* Field Entry */
    cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPProfileHeaderCell *headerCell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileHeaderCell class]) forIndexPath:indexPath];
        headerCell.lblTitle.text = ls(@"pet_lb_type");
        return headerCell;
    } formater:headerFormat action:nil];
    [cms addObject:cm];
    
    cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPPetDetailPetTypeCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPPetDetailPetTypeCell class]) forIndexPath:indexPath];
        [cell setPet:pet];
        [cell.btnPetTypePrimary setUserInteractionEnabled:[self editable]];
        [cell.btnPetTypeSecondary setUserInteractionEnabled:[self editable]];
        return cell;
    } action:nil];
    [cms addObject:cm];
    
    /* Field Entry */
    cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPProfileHeaderCell *headerCell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileHeaderCell class]) forIndexPath:indexPath];
        headerCell.lblTitle.text = ls(@"pet_lb_birthday");
        return headerCell;
    } formater:headerFormat action:nil];
    [cms addObject:cm];
    
    cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPPetDetailStandardCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPPetDetailStandardCell class]) forIndexPath:indexPath];
        cell.lblTitle.text = [pet formattedBirthdate];
        cell.btnPen.hidden = ![self editable];
        [cell.btnPen removeTarget:self action:nil forControlEvents:UIControlEventAllEvents];
        [cell.btnPen addTarget:self action:@selector(editBirthday:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    } action:nil];
    [cms addObject:cm];
    
    /* Field Entry */
    cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPProfileHeaderCell *headerCell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileHeaderCell class]) forIndexPath:indexPath];
        headerCell.lblTitle.text = ls(@"pet_lb_pin_card");
        return headerCell;
    } formater:headerFormat action:nil];
    [cms addObject:cm];
    
    cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPPetDetailPinCardCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPPetDetailPinCardCell class]) forIndexPath:indexPath];
        [cell.factory setOnSelectPinCard:^(OPPinCard * card) {
            if(_self.expandedPincard == card) _self.expandedPincard = nil;
            else _self.expandedPincard = card;
            [_self updateUI];
        }];
        [cell.factory setOnCreatePincard:^{
            [[OPNavigationController sharedInstance]pushViewController:[[OPPetPinCardViewController alloc]initWithCallback:^(OPPinCard * card) {
                NSMutableArray *result = [NSMutableArray arrayWithArray:pet.pin_card_list];
                [result addObject:card];
                pet.pin_card_list = result;
                [_self updateUI];
            }] animated:YES];
        }];
        [cell.factory setSelectedPincard:self.expandedPincard];
        [cell setPet:pet];
        return cell;
    } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
        NSInteger count = pet.pin_card_list.count + 1;
        NSInteger rows = (count / 3 + ((count % 3) ? 1 : 0));
        return CGSizeMake(pWidth, 35 * rows);
    } action:nil];
    [cms addObject:cm];
    
    if(self.expandedPincard) {
        cm = [CellModel modelWithData:self.expandedPincard renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPPetDetailPinCardDetailCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPPetDetailPinCardDetailCell class]) forIndexPath:indexPath];
            [cell setPincard:self.expandedPincard];
            [cell setPosIndicator: 0.25 + 0.25 * ([self.pet.pin_card_list indexOfObject:self.expandedPincard] % 3)];
            [cell setOnSavePinCard:^(OPPinCard * pincard) {
                [_self updateUI];
            }];
            return cell;
        } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
            return CGSizeMake(pWidth, 165);
        } action:^(CellModel *model) {
            
        }];
        [cms addObject:cm];
    }
    
    /* Field Entry */
    cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPProfileHeaderCell *headerCell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileHeaderCell class]) forIndexPath:indexPath];
        headerCell.lblTitle.text = ls(@"pet_lb_health_record");
        return headerCell;
    } formater:headerFormat action:nil];
    [cms addObject:cm];
    
    cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPPetDetailStandardCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPPetDetailStandardCell class]) forIndexPath:indexPath];
        cell.lblTitle.text = pet.medical_record;
        cell.btnPen.hidden = ![self editable];
        [cell.btnPen removeTarget:self action:nil forControlEvents:UIControlEventAllEvents];
        [cell.btnPen addTarget:self action:@selector(editMedicalRecord) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    } action:nil];
    [cms addObject:cm];
    
    /* Field Entry */
    cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPProfileHeaderCell *headerCell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileHeaderCell class]) forIndexPath:indexPath];
        headerCell.lblTitle.text = ls(@"pet_lb_last_health_check");
        return headerCell;
    } formater:headerFormat action:nil];
    [cms addObject:cm];
    
    cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPPetDetailLastBodyCheckCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPPetDetailLastBodyCheckCell class]) forIndexPath:indexPath];
        [cell setPet:pet];
        return cell;
    } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
        return CGSizeMake(pWidth, 100);
    } action:nil];
    [cms addObject:cm];
    
    /* Field Entry */
    cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPProfileHeaderCell *headerCell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileHeaderCell class]) forIndexPath:indexPath];
        headerCell.lblTitle.text = ls(@"pet_lb_last_hair_cutting");
        return headerCell;
    } formater:headerFormat action:nil];
    [cms addObject:cm];
    
    cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPPetDetailStandardCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPPetDetailStandardCell class]) forIndexPath:indexPath];
        cell.lblTitle.text = pet.last_bath_hair_cut_description;
        cell.btnPen.hidden = ![self editable];
        [cell.btnPen removeTarget:self action:nil forControlEvents:UIControlEventAllEvents];
        [cell.btnPen addTarget:self action:@selector(editLastHairCut) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    } action:nil];
    [cms addObject:cm];
    
    /* Field Entry */
    cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPProfileHeaderCell *headerCell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileHeaderCell class]) forIndexPath:indexPath];
        headerCell.lblTitle.text = ls(@"pet_lb_bmi");
        return headerCell;
    } formater:headerFormat action:nil];
    [cms addObject:cm];
    
    cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPPetDetailBMICalculatorCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPPetDetailBMICalculatorCell class]) forIndexPath:indexPath];
        [cell setOnResult:^(NSString * weight, NSString * length) {
            [pet updateBMIRecordWithWeight:weight.floatValue length:length.floatValue insert:!self.isBMIRecordInserted];
            _self.isBMIRecordInserted = YES;
        }];
        return cell;
    } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
        return CGSizeMake(pWidth, 70);
    } action:nil];
    [cms addObject:cm];
    
    /* Field Entry */
    cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPProfileHeaderCell *headerCell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileHeaderCell class]) forIndexPath:indexPath];
        headerCell.lblTitle.text = ls(@"pet_lb_bmi_record");
        return headerCell;
    } formater:headerFormat action:nil];
    [cms addObject:cm];
    
    cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPPetDetailBMIRecordsCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPPetDetailBMIRecordsCell class]) forIndexPath:indexPath];
        [cell setPet:pet];
        return cell;
    } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
        return CGSizeMake(pWidth, 160);
    } action:nil];
    [cms addObject:cm];
    
    if([self deletable]) {
        cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPButtonCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPButtonCell class]) forIndexPath:indexPath];
            [cell.btn addTarget:self action:@selector(deletePet) forControlEvents:UIControlEventTouchUpInside];
            [cell.btn setTitle:@"刪除寵物" forState:UIControlStateNormal];
            return cell;
        } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
            return CGSizeMake(pWidth, 40);
        } action:nil];
        [cms addObject:cm];
    }

    [self loadViewModels:cms];
}

- (void)deletePet {
    if(self.onDelete) self.onDelete(self.pet);
}

- (void)editPetName {
    [self.vc showInputDialog:@"" inputHandler:^(MessageDialogAlertController *ac, UIAlertAction *action, NSString *input) {
        self.pet._description = input;
        [self updateUI];
    } nevigativeHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {
       
    }];
}

- (void)editMedicalRecord {
    [self.vc showInputDialog:@"" inputHandler:^(MessageDialogAlertController *ac, UIAlertAction *action, NSString *input) {
        self.pet.medical_record = input;
        [self updateUI];
    } nevigativeHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {
        
    }];
}

- (void)editBirthday:(id)sender {
    [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        self.pet.birth_date = [self.pet stringFromDate:selectedDate];
        [self updateUI];
    } cancelBlock:nil origin:sender];
}

- (void)editLastHairCut {
    [self.vc showInputDialog:@"" inputHandler:^(MessageDialogAlertController *ac, UIAlertAction *action, NSString *input) {
        self.pet.last_bath_hair_cut_description = input;
        [self updateUI];
    } nevigativeHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {
        
    }];
}

- (void)editProfilePic {
    UIImagePickerController *controller = [[UIImagePickerController alloc]init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:controller.sourceType];
    controller.allowsEditing = NO;
    [[OPNavigationController sharedInstance]presentViewController:controller animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *img = [info objectForKey: UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
    self.petImage = img;
    [self updateUI];
}

@end
