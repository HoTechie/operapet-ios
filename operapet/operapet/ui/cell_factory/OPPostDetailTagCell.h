//
//  OPPostDetailTagCell.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPTagFactory.h"
#import "OPPost.h"

@interface OPPostDetailTagCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UICollectionView *collectionTag;
@property OPTagFactory *factory;
@property (nonatomic) OPPost *post;

@end
