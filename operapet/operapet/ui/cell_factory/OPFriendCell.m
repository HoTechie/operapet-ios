//
//  OPFriendCell.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPFriendCell.h"
#import "UIImageView+WebCache.h"
#import "OPServiceManager.h"

@implementation OPFriendCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.factory = [[OPFriendCellPetSubFactory alloc]initWithCollectionView:self.collectionPets];
    self.imgProfilePic.layer.masksToBounds = YES;
    
    [self.btnAccept addTarget:self action:@selector(acceptFriendRequest) forControlEvents:UIControlEventTouchUpInside];
    [self.btnDecline addTarget:self action:@selector(declineFriendRequest) forControlEvents:UIControlEventTouchUpInside];
    [self.btnAddFd addTarget:self action:@selector(addFriend) forControlEvents:UIControlEventTouchUpInside];
}

- (void)acceptFriendRequest {
    if(self.onAcceptFriendRequest) self.onAcceptFriendRequest(self.profile);
}

- (void)declineFriendRequest {
    if(self.onDeclineFriendRequest) self.onDeclineFriendRequest(self.profile);
}

- (void)addFriend {
    if(self.onAddFriend) self.onAddFriend(self.profile);
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.imgProfilePic.layer.cornerRadius = self.imgProfilePic.frame.size.height/2.0;
}

- (void)setProfile:(OPProfile *)profile {
    _profile = profile;
    
    [self layoutIfNeeded];
    self.lblName.text = profile.user_name;
    [self.imgProfilePic sd_setImageWithURL:urlprofilepic(profile.image_id)];
    [self.factory loadData:profile.pets ? profile.pets : @[]];
    
    
    if(profile.friend_relation.status == OPFriendRelationPending) {
        [self.btnAddFd setBackgroundImage:[[UIImage imageNamed:@"icon_tab_friend"]
                                           imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        self.btnAddFd.tintColor = color_theme_color_darker;
    } else {
        [self.btnAddFd setBackgroundImage:[[UIImage imageNamed:@"icon_add_friend"]
                                           imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    }
}

@end
