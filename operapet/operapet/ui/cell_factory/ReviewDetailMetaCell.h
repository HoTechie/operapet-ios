//
//  ReviewDetailMetaCell.h
//  myislands
//
//  Created by alan tsoi on 9/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewDetailMetaCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblMeta;

@end
