//
//  OPProfileHeaderCell.h
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OPProfileHeaderCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@end
