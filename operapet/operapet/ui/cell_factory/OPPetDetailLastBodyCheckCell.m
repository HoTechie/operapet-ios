//
//  OPPetDetailLastBodyCheckCell.m
//  operapet
//
//  Created by Gary KK on 18/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPetDetailLastBodyCheckCell.h"

@interface OPPetDetailLastBodyCheckCell()<UITextFieldDelegate>

@end

@implementation OPPetDetailLastBodyCheckCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self configTextField:self.txtLastCheckYear];
    [self configTextField:self.txtLastCheckMonth];
    [self configTextField:self.txtLastCheckDay];
    [self configTextField:self.txtNextCheckYear];
    [self configTextField:self.txtNextCheckMonth];
    [self configTextField:self.txtNextCheckDay];
}

- (void)configTextField:(UITextField *)txtField {
    txtField.delegate = self;
    [txtField setReturnKeyType:UIReturnKeyDone];
    [txtField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

- (void)setPet:(OPPet *)pet {
    _pet = pet;
    [self updateUI];
}

- (void)updateUI {
    NSDateComponents *lastCheckComps = [self.pet dateCompsFromString:self.pet.last_body_check_date];
    self.txtLastCheckYear.text = lastCheckComps.year ? [@(lastCheckComps.year) stringValue] : @"";
    self.txtLastCheckMonth.text = lastCheckComps.month ? [@(lastCheckComps.month) stringValue] : @"";
    self.txtLastCheckDay.text = lastCheckComps.day ? [@(lastCheckComps.day) stringValue] : @"";
    NSDateComponents *nextCheckComps = [self.pet dateCompsFromString:self.pet.next_body_check_date];
    self.txtNextCheckYear.text = nextCheckComps.year ? [@(nextCheckComps.year) stringValue] : @"";
    self.txtNextCheckMonth.text = nextCheckComps.month ? [@(nextCheckComps.month) stringValue] : @"";
    self.txtNextCheckDay.text = nextCheckComps.day ? [@(nextCheckComps.day) stringValue] : @"";
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *target = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    NSInteger charLimit = 2;
    if(textField == self.txtLastCheckYear || textField == self.txtNextCheckYear) {
        charLimit = 4;
    }
    
    return target.length <= charLimit;
}

-(NSDateComponents *)dateCompsFromYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day {
    if(!year || !month || !day) return nil;
    
    NSDateComponents *dateComps = [[NSDateComponents alloc]init];
    [dateComps setYear:year];
    [dateComps setMonth:month];
    [dateComps setDay:day];
    return dateComps;
}

-(void)textFieldDidChange:(UITextField *)textField {
    NSDateComponents *dateComps;
    
    dateComps = [self dateCompsFromYear:self.txtLastCheckYear.text.integerValue
                                 month:self.txtLastCheckMonth.text.integerValue
                                   day:self.txtLastCheckDay.text.integerValue];
    self.pet.last_body_check_date = dateComps ? [self.pet stringFromDateComps:dateComps] : nil;
    
    dateComps = [self dateCompsFromYear:self.txtNextCheckYear.text.integerValue
                                  month:self.txtNextCheckMonth.text.integerValue
                                    day:self.txtNextCheckDay.text.integerValue];
    self.pet.next_body_check_date = dateComps ? [self.pet stringFromDateComps:dateComps] : nil;
}


@end
