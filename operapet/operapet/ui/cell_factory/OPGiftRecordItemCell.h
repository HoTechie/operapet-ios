//
//  OPGiftRecordItemCell.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPGift.h"

@interface OPGiftRecordItemCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblPoints;
@property (strong, nonatomic) IBOutlet UILabel *lblRequired;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UIImageView *img;
@property (strong, nonatomic) IBOutlet UIImageView *imgLeftRibbon;
@property (strong, nonatomic) IBOutlet UILabel *lblStatus;

@property (nonatomic) OPGift *gift;

@end
