//
//  OPPetCell.m
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPetCell.h"
#import "OPServiceManager.h"
#import "Constants.h"

@implementation OPPetCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.imgThumbnail.layer.cornerRadius = self.imgThumbnail.frame.size.width/2.0;
    self.imgThumbnail.layer.masksToBounds = YES;
}

- (void)setPet:(OPPet *)pet {
    _pet = pet;
    [self updateUI];
}

- (void)updateUI {
    [self.imgThumbnail sd_setImageWithURL:urlpetimg(self.pet.image_id) placeholderImage:nil options:SDWebImageRetryFailed|SDWebImageCacheMemoryOnly completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    [self.petBorderView setTintColor:self.pet.gender == OPPetGenderMale ? color_pet_boy : color_pet_girl];
    
    [self layoutIfNeeded];
}

@end
