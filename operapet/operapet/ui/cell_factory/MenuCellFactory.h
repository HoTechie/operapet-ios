//
//  MenuCellFactory.h
//  operapet
//
//  Created by Gary KK on 6/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListCellFactory.h"

#import "OPMenuUserCell.h"
#import "TextCell.h"
#import "MenuCell.h"

@interface MenuCellFactory : BaseListCellFactory

@end
