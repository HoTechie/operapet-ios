//
//  OPTagCell.m
//  operapet
//
//  Created by Gary KK on 14/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPTagCell.h"

@implementation OPTagCell

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    self.tagView = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([OPTagView class]) owner:nil options:0].firstObject;
    [self.tagView addToSuperview:self];
    return self;
}

@end
