//
//  OPWallActionCell.m
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPWallActionCell.h"


@implementation OPWallActionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.factory = [[OPWallActionSubFactory alloc]initWithCollectionView:self.actionList];
}

- (void)setProfile:(OPProfile *)profile {
    _profile = profile;
    [self.factory setProfile:profile];
}

@end
