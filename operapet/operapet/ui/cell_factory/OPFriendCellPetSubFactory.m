//
//  OPFriendCellPetSubFactory.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPFriendCellPetSubFactory.h"
#import "ImageCell.h"

@implementation OPFriendCellPetSubFactory

-(id)initWithCollectionView:(UICollectionView *)collectionView {
    self = [super initWithCollectionView:collectionView];
    ((UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout).scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.collectionView.allowsSelection = NO;
    self.collectionView.userInteractionEnabled = NO;
    return self;
}

-(UIEdgeInsets)listInset {
    return UIEdgeInsetsZero;
}

-(CGFloat)cellLineSpacing {
    return 5.0;
}

-(CGFloat)cellInterSpacing {
    return 5.0;
}

- (CGSize)cellDefaultSizeWithParentWidth:(CGFloat)parentWidth height:(CGFloat)parentHeight {
    return CGSizeMake(parentHeight, parentHeight);
}

-(NSArray *)cellNibs {
    return @[
              NSStringFromClass([OPFriendCellPetSubCell class]),
             ];
}

-(void)loadData:(NSArray *)data {
    NSMutableArray *cms = [NSMutableArray array];
    CellModel *cm;
    
    for(OPPet *pet in data) {
        cm = [CellModel modelWithData:pet renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPFriendCellPetSubCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPFriendCellPetSubCell class]) forIndexPath:indexPath];
            [cell setPet:pet];
            return cell;
        } action:nil];
        [cms addObject:cm];
    }
    
    [self loadViewModels:cms];
}



@end
