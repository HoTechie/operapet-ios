//
//  ImageCell.m
//  myislands
//
//  Created by alan tsoi on 2/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "ImageCell.h"

@implementation ImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    layoutAttributes.frame = CGRectMake(layoutAttributes.frame.origin.x, layoutAttributes.frame.origin.y, 100, layoutAttributes.frame.size.height);
    return layoutAttributes;
}

@end
