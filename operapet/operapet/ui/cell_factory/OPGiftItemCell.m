//
//  OPGiftItemCell.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPGiftItemCell.h"
#import "UIImageView+WebCache.h"
#import "OPServiceManager.h"
#import "Constants.h"

@implementation OPGiftItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setGift:(OPGift *)gift {
    _gift = gift;
    
    self.lblDate.text = [gift formattedDateFromString:gift.create_time];
    self.lblName.text = gift.name;
    self.lblPoints.text = [@(gift.credit) stringValue];
    
    if(gift.images.count) {
        NSInteger imgId = [[gift.images firstObject] integerValue];
        [self.img sd_setImageWithURL:urlgiftimg(imgId)];
    }
    
    self.enabled = [SessionManager sharedInstance].currentProfile.remain_credit >= gift.credit;
}

- (void)setEnabled:(BOOL)enabled {
    self.imgLeftRibbon.image = enabled ? [self.imgLeftRibbon.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] : [self.imgLeftRibbon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.imgLeftRibbon.tintColor = color_main_text_color_lighter;
    
    self.lblRequired.textColor = enabled ? color_theme_color_darker : color_main_text_color_invert;
    self.lblPoints.textColor = enabled ? color_theme_color_darker : color_main_text_color_invert;
    self.lblName.textColor = enabled ? color_theme_color_darker : color_main_text_color;
}

@end
