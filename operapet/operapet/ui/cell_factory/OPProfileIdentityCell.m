//
//  OPProfileIdentityCell.m
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPProfileIdentityCell.h"
#import "UIImageView+WebCache.h"
#import "OPServiceManager.h"
#import "Constants.h"

@implementation OPProfileIdentityCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setProfile:(OPProfile *)profile {
    _profile = profile;
    [self.imgProfilePic sd_setImageWithURL:urlprofilepic(profile.image_id) placeholderImage:nil options:SDWebImageRefreshCached];
    [self.lblName setText:[NSString stringWithFormat:@"%@ %@", profile.firstname, profile.lastname]];
    
    self.borderProfilePic.hidden = NO;
    self.borderPetImage.hidden = YES;
}

- (void)setPet:(OPPet *)pet {
    _pet = pet;
    [self.imgProfilePic sd_setImageWithURL:urlpetimg(pet.image_id) placeholderImage:nil options:SDWebImageRetryFailed];
    [self.lblName setText:pet._description];
    
    self.borderProfilePic.hidden = YES;
    self.borderPetImage.hidden = NO;
    self.borderPetImage.tintColor = pet.gender == OPPetGenderMale ? color_pet_boy : color_pet_girl;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.imgProfilePic.layer.cornerRadius = self.imgProfilePic.frame.size.height/2.0;
    self.imgProfilePic.layer.masksToBounds = YES;
}

@end
