//
//  BaseListCellFactory.h
//  myislands
//
//  Created by alan tsoi on 3/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "BaseCellFactory.h"

@interface BaseListCellFactory : BaseCellFactory

-(UIEdgeInsets)listInset;
-(CGFloat)cellHeight;

@end
