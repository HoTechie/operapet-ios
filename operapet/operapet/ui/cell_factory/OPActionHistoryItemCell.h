//
//  OPActionHistoryItemCell.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPActionHistory.h"
#import "OPPaperBox.h"

@interface OPActionHistoryItemCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblDescription;
@property (strong, nonatomic) IBOutlet UILabel *lblPoints;
@property (strong, nonatomic) IBOutlet UIView *viewSeparator;
@property (strong, nonatomic) IBOutlet OPPaperBox *paperBox;

@property (nonatomic) OPActionHistory *history;

@end
