//
//  OPMenuUserCell.h
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListCollectionViewCell.h"

@interface OPMenuUserCell : BaseListCollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgUserIcon;
@property (strong, nonatomic) IBOutlet UILabel *lblUsername;

@end
