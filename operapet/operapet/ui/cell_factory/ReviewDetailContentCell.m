//
//  ReviewDetailContentCell.m
//  myislands
//
//  Created by alan tsoi on 9/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "ReviewDetailContentCell.h"

@implementation ReviewDetailContentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    self.lblContent.preferredMaxLayoutWidth = layoutAttributes.frame.size.width;
    CGFloat fittingHeight = [self.lblContent systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    
    layoutAttributes.frame = CGRectMake(layoutAttributes.frame.origin.x
                                        , layoutAttributes.frame.origin.y, layoutAttributes.frame.size.width, fittingHeight);
    return layoutAttributes;
}

@end
