//
//  OPGiftFactory.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListCellFactory.h"

@interface OPGiftFactory : BaseListCellFactory

@property NSArray *ads;

@end
