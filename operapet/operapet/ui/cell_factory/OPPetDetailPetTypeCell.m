//
//  OPPetDetailPetTypeCell.m
//  operapet
//
//  Created by Gary KK on 18/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPetDetailPetTypeCell.h"
#import "OPConstants.h"
#import "ActionSheetStringPicker.h"

@implementation OPPetDetailPetTypeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.btnPetTypePrimary addTarget:self action:@selector(onClickBtnPrimary:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPetTypeSecondary addTarget:self action:@selector(onClickBtnSecondary:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setPet:(OPPet *)pet {
    _pet = pet;
    [self updateUI];
}

- (void)updateUI {
    OPConstantPetCategory *cat = [[OPConstants sharedInstance] petCategoryForPetTypeId:self.pet.pet_type];
    OPConstantPetType *type = [[OPConstants sharedInstance] petTypeForPetTypeId:self.pet.pet_type];
    
    [self.btnPetTypePrimary setTitle:cat ? cat.name : @"" forState:UIControlStateNormal];
    [self.btnPetTypeSecondary setTitle:type ? type.name : @"" forState:UIControlStateNormal];
}

- (void)onClickBtnPrimary:(id)sender {
    NSArray *cats = [OPConstants sharedInstance].petCategories;
    NSMutableArray *catNames = [NSMutableArray array];
    for(OPConstantPetCategory *cat in cats) {
        [catNames addObject:cat.name];
    }
    
    [ActionSheetStringPicker showPickerWithTitle:@"" rows:catNames initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        OPConstantPetCategory *cat = cats[selectedIndex];
        OPConstantPetType *type = cat.types.firstObject;
        self.pet.pet_type = type.id;
        [self updateUI];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:sender];
}

- (void)onClickBtnSecondary:(id)sender {
    OPConstantPetCategory *cat = [[OPConstants sharedInstance]petCategoryForPetTypeId:self.pet.pet_type];
    
    if(cat && cat.types.count) {
        NSArray *types = cat.types;
        NSMutableArray *typeNames = [NSMutableArray array];
        for(OPConstantPetType *type in types) {
            [typeNames addObject:type.name];
        }
        
        [ActionSheetStringPicker showPickerWithTitle:@"" rows:typeNames initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            OPConstantPetType *type = types[selectedIndex];
            self.pet.pet_type = type.id;
            [self updateUI];
        } cancelBlock:^(ActionSheetStringPicker *picker) {
            
        } origin:sender];
    }
}

@end
