//
//  OPWallFactory.h
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPostListCellFactory.h"

#import "OPWallOverviewCell.h"
#import "OPWallActionCell.h"

@interface OPWallFactory : OPPostListCellFactory

@property (nonatomic) OPProfile *profile;
@property NSArray *posts;

@property void(^onAddFriend)();
@property void(^onAcceptFriend)();
@property void(^onDeleteFriend)();

@end
