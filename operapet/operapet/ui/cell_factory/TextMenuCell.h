//
//  TextMenuCell.h
//  myislands
//
//  Created by alan tsoi on 2/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "BaseCollectionViewCell.h"

@interface TextMenuCell : BaseCollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
