//
//  OPActionHistoryFactory.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPActionHistoryFactory.h"
#import "OPActionHistoryHeadCell.h"
#import "OPActionHistoryFootCell.h"
#import "OPActionHistoryItemCell.h"

@implementation OPActionHistoryFactory

-(UIEdgeInsets)listInset {
    return UIEdgeInsetsZero;
}

-(CGFloat)cellLineSpacing {
    return 0.0;
}

-(CGFloat)cellInterSpacing {
    return 0.0;
}

-(CGFloat)cellHeight {
    return 80.0;
}

-(NSArray *)cellNibs {
    return @[
             NSStringFromClass([OPActionHistoryHeadCell class]),
             NSStringFromClass([OPActionHistoryFootCell class]),
             NSStringFromClass([OPActionHistoryItemCell class]),
             ];
}

-(void)loadData:(NSArray *)data {
    NSMutableArray *cms = [NSMutableArray array];
    
    CellModel *cm;
    CGFloat headFootRatio = 16/229.0;
    
    cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPActionHistoryHeadCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPActionHistoryHeadCell class]) forIndexPath:indexPath];
        return cell;
    } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
        return CGSizeMake(pWidth, pWidth * headFootRatio );
    } action:nil];
    [cms addObject:cm];
    
    for(OPActionHistory *history in data) {
        cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPActionHistoryItemCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPActionHistoryItemCell class]) forIndexPath:indexPath];
            cell.viewSeparator.hidden = history == data.lastObject;
            [cell setHistory:history];
            return cell;
        } action:^(CellModel *model) {
            
        }];
        [cms addObject:cm];
    }
    
    cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPActionHistoryFootCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPActionHistoryFootCell class]) forIndexPath:indexPath];
        return cell;
    } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
        return CGSizeMake(pWidth, pWidth * headFootRatio );
    } action:nil];
    [cms addObject:cm];
    
    [self loadViewModels:cms];
}

@end
