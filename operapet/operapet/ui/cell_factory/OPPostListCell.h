//
//  OPPostListCell.h
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPPostView.h"

@interface OPPostListCell : UICollectionViewCell

@property OPPostView *postView;

@end
