//
//  OPWallActionCell.h
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPProfile.h"
#import "OPWallActionSubFactory.h"

@interface OPWallActionCell : UICollectionViewCell

@property (nonatomic) OPProfile *profile;
@property (strong, nonatomic) IBOutlet UICollectionView *actionList;
@property OPWallActionSubFactory *factory;

@end
