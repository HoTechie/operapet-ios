//
//  OPWallOverviewCell.h
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPProfile.h"

@interface OPWallOverviewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblShare;
@property (strong, nonatomic) IBOutlet UILabel *lblShareCount;
@property (strong, nonatomic) IBOutlet UILabel *lblPostCount;
@property (strong, nonatomic) IBOutlet UILabel *lblPost;
@property (strong, nonatomic) IBOutlet UIImageView *imgProfilePic;
@property (strong, nonatomic) IBOutlet UILabel *lblUsername;


@property (nonatomic) OPProfile *profile;
@end
