//
//  BaseCellFactory.h
//  myislands
//
//  Created by alan tsoi on 1/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@class CellModel;

typedef UICollectionViewCell* (^Renderer)(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath);
typedef CGSize (^Formater)(CellModel *model, CGFloat pWidth, CGFloat pHeight);
typedef void (^Action)(CellModel *model);

@interface CellModel : NSObject
@property id data;
@property Renderer render;
@property Formater format;
@property Action action;

+(instancetype) modelWithData:(id)data renderer:(Renderer)renderer formater:(Formater)formater action:(Action)action;
+(instancetype) modelWithData:(id)data renderer:(Renderer)renderer action:(Action)action;
+(instancetype) modelWithData:(id)data action:(Action)action;
-(id)initWithData:(id)data renderer:(Renderer)renderer formater:(Formater)formater action:(Action)action;
-(id)initWithData:(id)data renderer:(Renderer)renderer action:(Action)action;
-(id)initWithData:(id)data action:(Action)action;

@end

@interface BaseCellFactory : NSObject<UICollectionViewDelegateFlowLayout, UICollectionViewDataSource>

-(id)initWithCollectionView:(UICollectionView *)collectionView;

@property __weak UICollectionView *collectionView;
@property (nonatomic) NSArray *models;

-(NSArray *)cellNibs;
-(NSArray *)cellClasses;

-(CGFloat)cellInterSpacing;
-(CGFloat)cellLineSpacing;
-(CGSize)cellDefaultSizeWithParentWidth:(CGFloat)parentWidth height:(CGFloat)parentHeight;

-(void)loadData:(NSArray *)data;
-(void)loadViewModels:(NSArray *)viewModels;

@end
