//
//  BaseListCellFactory.m
//  myislands
//
//  Created by alan tsoi on 3/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "BaseListCellFactory.h"
#import "Constants.h"

@implementation BaseListCellFactory

-(id)initWithCollectionView:(UICollectionView *)collectionView {
    self = [super initWithCollectionView:collectionView];
    
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)collectionView.collectionViewLayout;
    layout.sectionInset = [self listInset];
    
    return self;
}

-(UIEdgeInsets)listInset {
    return UIEdgeInsetsMake(dimem_list_padding_y, dimem_list_padding_x, dimem_list_padding_y, dimem_list_padding_x);
}

-(CGFloat)cellLineSpacing {
    return dimem_list_line_spacing;
}

-(NSArray *)cellNibs {
    return @[@"BaseListCollectionViewCell"];
}

-(CGSize)cellDefaultSizeWithParentWidth:(CGFloat)parentWidth height:(CGFloat)parentHeight {
    return CGSizeMake(parentWidth, [self cellHeight]);
}

-(CGFloat)cellHeight {
    return 140;
}

@end
