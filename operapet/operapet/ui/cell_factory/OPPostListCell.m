//
//  OPPostListCell.m
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPostListCell.h"

@implementation OPPostListCell

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    self.postView = [[NSBundle mainBundle]loadNibNamed:@"OPPostView" owner:nil options:0].firstObject;
    [self addSubview:self.postView];
    [self constrainView:self.postView equalView:self];
    return self;
}

-(void)constrainView:(UIView *) view equalView:(UIView *)view2{
    [view setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *con1 = [NSLayoutConstraint constraintWithItem:view2 attribute:NSLayoutAttributeCenterX relatedBy:0 toItem:view attribute:NSLayoutAttributeCenterX multiplier:1 constant:0];
    NSLayoutConstraint *con2 = [NSLayoutConstraint constraintWithItem:view2 attribute:NSLayoutAttributeCenterY relatedBy:0 toItem:view attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
    NSLayoutConstraint *con3 = [NSLayoutConstraint constraintWithItem:view2 attribute:NSLayoutAttributeWidth relatedBy:0 toItem:view attribute:NSLayoutAttributeWidth multiplier:1 constant:0];
    NSLayoutConstraint *con4 = [NSLayoutConstraint constraintWithItem:view2 attribute:NSLayoutAttributeHeight relatedBy:0 toItem:view attribute:NSLayoutAttributeHeight multiplier:1 constant:0];
    NSArray *constraints = @[con1,con2,con3,con4];
    [self addConstraints:constraints];
}

@end
