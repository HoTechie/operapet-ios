//
//  OPActionHistoryItemCell.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPActionHistoryItemCell.h"

@implementation OPActionHistoryItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.paperBox.shouldDrawDepth = NO;
}

- (void)setHistory:(OPActionHistory *)history {
    _history = history;
    
    self.lblDescription.text = history._description;
    self.lblPoints.text = [NSString stringWithFormat:@"+%@",[@(history.credit) stringValue]];
}

@end
