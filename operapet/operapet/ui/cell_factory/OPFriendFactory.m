//
//  OPFriendFactory.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPFriendFactory.h"
#import "OPFriendCell.h"
#import "OPProfileHeaderCell.h"
#import "OPFriendRequestListViewController.h"
#import "OPWallViewController.h"

@implementation OPFriendFactory

-(UIEdgeInsets)listInset {
    return UIEdgeInsetsZero;
}

-(CGFloat)cellLineSpacing {
    return 0.0;
}

-(CGFloat)cellInterSpacing {
    return 0.0;
}

-(CGFloat)cellHeight {
    return 95.0;
}

-(NSArray *)cellNibs {
    return @[
             NSStringFromClass([OPFriendCell class]),
             @"OPFriendCellAllRequests",
             @"OPFriendCellRequest",
             @"OPFriendCellSearchResult",
             NSStringFromClass([OPProfileHeaderCell class])
             ];
}

-(void)loadData:(NSArray *)data {
    self.profiles = data;
    [self updateUI];
}

- (void)loadProfiles:(NSArray *)profiles requests:(NSArray *)requests {
    self.requests = requests;
    [self loadData:profiles];
}

- (void)loadProfiles:(NSArray *)profiles {
    [self loadData:profiles];
}

-(void)updateUI {
    NSMutableArray *cms = [NSMutableArray array];
    
    CellModel *cm;
    
    if(self.mode == OPFriendFactoryModeMyFriendList) {
        if(self.requests && self.requests.count) {
            cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
                OPProfileHeaderCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileHeaderCell class]) forIndexPath:indexPath];
                cell.lblTitle.text = ls(@"friend_title_friend_request");
                return cell;
            } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
                return CGSizeMake(pWidth, 25);
            } action:nil];
            [cms addObject:cm];
            
            OPProfile *profile = self.requests.firstObject;
            cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
                OPFriendCell *cell = [parent dequeueReusableCellWithReuseIdentifier:@"OPFriendCellAllRequests" forIndexPath:indexPath];
                cell.profile = profile;
                cell.lblCount.text = [@(self.requests.count) stringValue];
                return cell;
            } action:^(CellModel *model) {
                [[OPNavigationController sharedInstance]pushViewController:[[OPFriendRequestListViewController alloc]init] animated:YES];
            }];
            [cms addObject:cm];
        }
        
        
        cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPProfileHeaderCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPProfileHeaderCell class]) forIndexPath:indexPath];
            cell.lblTitle.text = ls(@"friend_title_my_friend");
            return cell;
        } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
            return CGSizeMake(pWidth, 25);
        } action:nil];
        [cms addObject:cm];
    }
    
    
    for(OPProfile *profile in self.profiles) {
        
        if(self.mode == OPFriendFactoryModeDefaultList || self.mode == OPFriendFactoryModeMyFriendList) {
            cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
                OPFriendCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPFriendCell class]) forIndexPath:indexPath];
                cell.profile = profile;
                [cell layoutIfNeeded];
                return cell;
            } action:^(CellModel *model) {
                [[OPNavigationController sharedInstance]pushViewController:[[OPWallViewController alloc]initWithProfileId:profile.id] animated:YES];
            }];
            [cms addObject:cm];
        }else if(self.mode == OPFriendFactoryModeRequestList) {
            cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
                OPFriendCell *cell = [parent dequeueReusableCellWithReuseIdentifier:@"OPFriendCellRequest" forIndexPath:indexPath];
                cell.profile = profile;
                cell.onAcceptFriendRequest = self.onAcceptFriendRequest;
                cell.onDeclineFriendRequest = self.onDeclineFriendRequest;
                return cell;
            } action:^(CellModel *model) {
                
            }];
            [cms addObject:cm];
        }else if(self.mode == OPFriendFactoryModeSearchResultList) {
            cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
                OPFriendCell *cell = [parent dequeueReusableCellWithReuseIdentifier:@"OPFriendCellSearchResult" forIndexPath:indexPath];
                cell.profile = profile;
                cell.onAddFriend = self.onAddFriend;
                return cell;
            } action:^(CellModel *model) {
                [[OPNavigationController sharedInstance]pushViewController:[[OPWallViewController alloc]initWithProfileId:profile.id] animated:YES];
            }];
            [cms addObject:cm];
        }
        
    }
    
    [self loadViewModels:cms];
}

-(void)setMode:(OPFriendFactoryMode)mode {
    _mode = mode;
    [self updateUI];
}

@end
