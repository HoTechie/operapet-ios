//
//  OPWallOverviewCell.m
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPWallOverviewCell.h"
#import "OPServiceManager.h"

@implementation OPWallOverviewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setProfile:(OPProfile *)profile {
    _profile = profile;
    
    self.lblPostCount.text = [@(profile.no_of_post) stringValue];
    self.lblShareCount.text = [@(profile.no_of_share) stringValue];
    [self.imgProfilePic sd_setImageWithURL:urlprofilepic(profile.image_id)];
    self.lblUsername.text = profile.user_name;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.imgProfilePic.layer.cornerRadius = self.imgProfilePic.frame.size.height / 2.0;
    self.imgProfilePic.layer.masksToBounds = YES;
}

@end
