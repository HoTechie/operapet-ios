//
//  TextMenuCell.m
//  myislands
//
//  Created by alan tsoi on 2/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "TextMenuCell.h"

@implementation TextMenuCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    [self.lblTitle sizeToFit];
    layoutAttributes.frame = CGRectMake(layoutAttributes.frame.origin.x, layoutAttributes.frame.origin.y,
                                        self.lblTitle.frame.size.width, layoutAttributes.frame.size.height);
    return layoutAttributes;
}

@end
