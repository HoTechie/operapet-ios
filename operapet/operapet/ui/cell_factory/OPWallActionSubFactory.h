//
//  OPWallActionSubFactory.h
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListCellFactory.h"
#import "OPWallActionSubCell.h"
#import "OPProfile.h"

@interface OPWallActionSubFactory : BaseListCellFactory

@property (nonatomic) OPProfile *profile;
@property void(^onAddFriend)();
@property void(^onAcceptFriend)();
@property void(^onDeleteFriend)();

@end
