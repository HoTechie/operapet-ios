//
//  TextCell.h
//  myislands
//
//  Created by alan tsoi on 12/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseListCollectionViewCell.h"

@interface TextCell : BaseListCollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblText;

@end
