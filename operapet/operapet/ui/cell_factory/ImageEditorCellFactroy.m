//
//  ImageEditorCellFactroy.m
//  myislands
//
//  Created by Gary KK on 3/10/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "ImageEditorCellFactroy.h"
#import "ImageCell.h"

@implementation ImageEditorCellFactroy

-(NSArray *)cellNibs {
    return @[@"ImageCell", @"ImageLikeCell"];
}

-(CGSize)cellDefaultSizeWithParentWidth:(CGFloat)parentWidth height:(CGFloat)parentHeight {
    return CGSizeMake(parentWidth, parentHeight);
}

-(CGFloat)cellLineSpacing {
    return 0;
}

-(CGFloat)cellInterSpacing {
    return 0;
}

-(UIEdgeInsets)listInset {
    return UIEdgeInsetsZero;
}

-(void)loadData:(NSArray *)data {
    NSMutableArray *cms = [NSMutableArray array];
    
    for(UIImage *image in data) {
        CellModel *cm = [CellModel modelWithData:image renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            ImageCell *imgCell = [parent dequeueReusableCellWithReuseIdentifier:@"ImageCell" forIndexPath:indexPath];
            imgCell.img.image = image;
            return imgCell;
        } action:^(CellModel *model) {
            
        }];
        [cms addObject:cm];
    }
    
    self.models = cms;
}


@end
