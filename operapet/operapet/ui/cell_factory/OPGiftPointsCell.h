//
//  OPGiftPointsCell.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
@class OPProfile;

@interface OPGiftPointsCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblPoints;
@property (strong, nonatomic) IBOutlet UIView *viewInnerCircle;

-(void)setProfile:(OPProfile *)profile;
@end
