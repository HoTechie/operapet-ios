//
//  BaseCollectionViewCell.h
//  myislands
//
//  Created by alan tsoi on 5/8/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblHome;

@end
