//
//  OPPostDetailIdentityCell.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPostDetailIdentityCell.h"
#import "UIImageView+WebCache.h"
#import "SessionManager.h"
#import "OPServiceManager.h"

@implementation OPPostDetailIdentityCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.imgProfilePic.layer.cornerRadius = self.imgProfilePic.frame.size.height / 2.0;
}

- (void)setPost:(OPPost *)post {
    _post = post;
 
    [self layoutIfNeeded];
    [self.imgProfilePic sd_setImageWithURL:urlprofilepic(post.profile.image_id)];
    self.lblName.text = post.profile.user_name;
    self.lblMemberLevel.text = [[SessionManager sharedInstance]memberLevelString:post.profile.member_level];
    
    self.lblDate.text = [[SessionManager sharedInstance]formattedDateString:post.create_time];
}

@end
