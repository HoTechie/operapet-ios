//
//  OPSearchTagCell.h
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListCollectionViewCell.h"
#import "OPTag.h"

@interface OPSearchTagCell : BaseListCollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgThumbnail;
@property (strong, nonatomic) IBOutlet UILabel *lblName;

-(void)setOPTag:(OPTag*)optag;

@end
