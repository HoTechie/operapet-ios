//
//  OPTagFactory.m
//  operapet
//
//  Created by Gary KK on 14/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPTagFactory.h"
#import "OPNavigationController.h"
#import "OPPostByTagListViewController.h"

@implementation OPTagFactory

-(id)initWithCollectionView:(UICollectionView *)collectionView {
    self = [super initWithCollectionView:collectionView];
    ((UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout).scrollDirection = UICollectionViewScrollDirectionHorizontal;
    return self;
}

-(UIEdgeInsets)listInset {
    return UIEdgeInsetsMake(0, 10, 0, 10);
}

-(CGFloat)cellLineSpacing {
    return 5.0;
}

-(CGFloat)cellInterSpacing {
    return 5.0;
}

-(NSArray*)cellClasses {
    return @[[OPTagCell class]];
}

-(void)loadData:(NSArray *)data {
    NSMutableArray *cms = [NSMutableArray array];
    
    CellModel *cm;
    for (OPTag *tag in data) {
        cm = [CellModel modelWithData:tag renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPTagCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPTagCell class]) forIndexPath:indexPath];
            [cell.tagView setOpTag:tag];
            return cell;
        } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
            OPTagView *tagView = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([OPTagView class]) owner:nil options:0].firstObject;
            [tagView setOpTag:tag];
            [tagView setMaxHeight:pHeight];
            
            CGFloat width = [tagView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].width;
            return CGSizeMake(width, pHeight);
        } action:^(CellModel *model) {
            [[OPNavigationController sharedInstance]pushViewController:[[OPPostByTagListViewController alloc]initWithOPTag:tag] animated:YES];
        }];
        [cms addObject:cm];
    }
    
    [self loadViewModels:cms];
}

@end
