//
//  MenuLanguageCell.h
//  myislands
//
//  Created by Gary KK on 27/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPCircleButton.h"

@interface MenuLanguageCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet OPCircleButton *btnLangEN;
@property (weak, nonatomic) IBOutlet OPCircleButton *btnLangZhHK;
@property (weak, nonatomic) IBOutlet OPCircleButton *btnLangZhCN;

@property (copy) void(^onClickBtnLangEN)();
@property (copy) void(^onClickBtnLangZhHK)();
@property (copy) void(^onClickBtnLangZhCN)();


@end
