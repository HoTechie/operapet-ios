//
//  OPPetDetailGenderCell.m
//  operapet
//
//  Created by Gary KK on 18/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPetDetailGenderCell.h"
#import "Constants.h"

@implementation OPPetDetailGenderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.btnMale setImage:[self.btnMale.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.btnFemale setImage:[self.btnFemale.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    
    [self.btnMale addTarget:self action:@selector(onClickBtnMale) forControlEvents:UIControlEventTouchUpInside];
    [self.btnFemale addTarget:self action:@selector(onClickBtnFemale) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)onClickBtnMale {
    self.pet.gender = OPPetGenderMale;
    if(self.onChangeGender)self.onChangeGender();
    [self updateUI];
}

- (void)onClickBtnFemale {
    self.pet.gender = OPPetGenderFemale;
    if(self.onChangeGender)self.onChangeGender();
    [self updateUI];
}

- (void) updateUI {
    self.btnMale.tintColor = self.pet.gender == OPPetGenderMale ? color_pet_boy : color_main_text_color_lighter;
    self.btnFemale.tintColor = self.pet.gender == OPPetGenderFemale ? color_pet_girl : color_main_text_color_lighter;
}

- (void)setPet:(OPPet *)pet {
    _pet = pet;
    [self updateUI];
}



@end
