//
//  OPPostDetailTagCell.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPostDetailTagCell.h"

@implementation OPPostDetailTagCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.factory = [[OPTagFactory alloc]initWithCollectionView:self.collectionTag];
}

- (void)setPost:(OPPost *)post {
    _post = post;
    [self.factory loadData:post.tags ? post.tags : @[]];
}

@end
