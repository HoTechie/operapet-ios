//
//  OPPostDetailTextView.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPostDetailTextView.h"

@implementation OPPostDetailTextView

- (void)setPost:(OPPost *)post {
    _post = post;
    self.lblText.text = post._description;
}

@end
