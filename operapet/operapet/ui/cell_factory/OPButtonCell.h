//
//  OPButtonCell.h
//  operapet
//
//  Created by Gary KK on 18/4/2017.
//  Copyright © 2017 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OPButtonCell : UICollectionViewCell

@property IBOutlet UIButton *btn;

@end
