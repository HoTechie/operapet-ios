//
//  OPPetDetailPinCardCell.h
//  operapet
//
//  Created by Gary KK on 18/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPPetDetailPinCardFactory.h"

@interface OPPetDetailPinCardCell : UICollectionViewCell

@property (nonatomic) OPPet *pet;
@property OPPetDetailPinCardFactory *factory;

@property (strong, nonatomic) IBOutlet UICollectionView *collectionPincard;

@end
