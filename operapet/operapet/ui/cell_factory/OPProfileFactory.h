//
//  OPProfileFactory.h
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListCellFactory.h"
#import "OPViewController.h"

#import "OPProfileIdentityCell.h"
#import "OPProfileHeaderCell.h"
#import "OPProfileStandardCell.h"
#import "OPProfileMemberCell.h"

@interface OPProfileFactory : BaseListCellFactory

@property (nonatomic, readonly) OPProfile *profile;
@property UIImage *profileImage;
@property (weak) OPViewController *vc;

-(void)loadProfile:(OPProfile *)profile fromVC:(OPViewController *)vc;

@end
