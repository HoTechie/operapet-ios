//
//  OPInviteFriendFactory.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListCellFactory.h"

@interface OPInviteFriendFactory : BaseListCellFactory
@property NSMutableArray *numbers;
@property void(^onSubmission)(NSArray *numbers);
@end
