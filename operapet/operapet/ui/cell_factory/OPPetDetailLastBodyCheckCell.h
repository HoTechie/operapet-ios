//
//  OPPetDetailLastBodyCheckCell.h
//  operapet
//
//  Created by Gary KK on 18/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPPet.h"

@interface OPPetDetailLastBodyCheckCell : UICollectionViewCell

@property (nonatomic) OPPet *pet;

@property (strong, nonatomic) IBOutlet UITextField *txtLastCheckYear;
@property (strong, nonatomic) IBOutlet UITextField *txtLastCheckMonth;
@property (strong, nonatomic) IBOutlet UITextField *txtLastCheckDay;
@property (strong, nonatomic) IBOutlet UITextField *txtNextCheckYear;
@property (strong, nonatomic) IBOutlet UITextField *txtNextCheckMonth;
@property (strong, nonatomic) IBOutlet UITextField *txtNextCheckDay;

@end
