//
//  OPCreatePostImageFactory.h
//  operapet
//
//  Created by Gary KK on 22/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListCellFactory.h"
#import "ImageCell.h"

@interface OPCreatePostImageFactory : BaseListCellFactory

@end
