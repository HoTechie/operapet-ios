//
//  OPPetDetailPinCardDetailCell.m
//  operapet
//
//  Created by Gary KK on 21/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPetDetailPinCardDetailCell.h"
#import "Constants.h"

#import "OPNavigationController.h"
#import "OPPetPinCardViewController.h"

@implementation OPPetDetailPinCardDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.posIndicator = 0.5;
    
    self.txtLastPinYear.enabled = NO;
    self.txtLastPinMonth.enabled = NO;
    self.txtLastPinDay.enabled = NO;
    self.txtLastPinYear.enabled = NO;
    self.txtNextPinYear.enabled = NO;
    self.txtNextPinMonth.enabled = NO;
    self.txtNextPinDay.enabled = NO;
    self.txtRemarks.enabled = NO;
    
    [self.btnPen addTarget:self action:@selector(onClickBtnEdit) forControlEvents:UIControlEventTouchUpInside];
}
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    self.posIndicator = 0.5;
    return self;
}

#define I_HEIGHT    15
#define I_WIDTH     25

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextMoveToPoint(c, rect.origin.x + rect.size.width * self.posIndicator - I_WIDTH/2, rect.origin.y + I_HEIGHT);
    CGContextAddLineToPoint(c, rect.origin.x + rect.size.width * self.posIndicator, rect.origin.y);
    CGContextAddLineToPoint(c, rect.origin.x + rect.size.width * self.posIndicator + I_WIDTH/2, rect.origin.y + I_HEIGHT);
    CGContextClosePath(c);
    CGContextSetFillColorWithColor(c, UIColorFromRGB(0xeeeeee).CGColor);
    CGContextFillPath(c);
}

- (void)setPosIndicator:(CGFloat)posIndicator {
    _posIndicator = posIndicator;
    [self setNeedsDisplay];
}

- (void)setPincard:(OPPinCard *)pincard {
    _pincard = pincard;
    
    self.txtRemarks.text = pincard._description;
    
    NSDateComponents *comps;
    
    if(self.pincard.recent_pin_record_date.length) {
        comps = [pincard dateCompsFromString:pincard.recent_pin_record_date];
        self.txtLastPinYear.text = [@(comps.year) stringValue];
        self.txtLastPinMonth.text = [@(comps.month) stringValue];
        self.txtLastPinDay.text = [@(comps.day) stringValue];
    } else {
        self.txtLastPinYear.text = @"";
        self.txtLastPinMonth.text = @"";
        self.txtLastPinDay.text = @"";
    }
    
    if(self.pincard.next_pin_record_date.length) {
        comps = [pincard dateCompsFromString:pincard.next_pin_record_date];
        self.txtNextPinYear.text = [@(comps.year) stringValue];
        self.txtNextPinMonth.text = [@(comps.month) stringValue];
        self.txtNextPinDay.text = [@(comps.day) stringValue];
    } else {
        self.txtNextPinYear.text = @"";
        self.txtNextPinMonth.text = @"";
        self.txtNextPinDay.text = @"";
    }
}

- (void)onClickBtnEdit {
    [[OPNavigationController sharedInstance]pushViewController:[[OPPetPinCardViewController alloc]initWithPincard:self.pincard
                                                                                                         callback:self.onSavePinCard] animated:YES];
}


@end
