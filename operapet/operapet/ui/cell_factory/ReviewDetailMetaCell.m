//
//  ReviewDetailMetaCell.m
//  myislands
//
//  Created by alan tsoi on 9/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "ReviewDetailMetaCell.h"

@implementation ReviewDetailMetaCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    layoutAttributes.frame = CGRectMake(layoutAttributes.frame.origin.x, layoutAttributes.frame.origin.y, layoutAttributes.frame.size.width, 60);
    return layoutAttributes;
}

@end
