//
//  OPNotificationCell.m
//  operapet
//
//  Created by Gary KK on 19/4/2017.
//  Copyright © 2017 Hotechie. All rights reserved.
//

#import "OPNotificationCell.h"
#import "UIImageView+WebCache.h"

@implementation OPNotificationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setImageURL:(NSString *)imageUrl description:(NSString *)description {
    self.imgThumbnail.layer.cornerRadius = self.imgThumbnail.frame.size.width / 2.0;
    self.imgThumbnail.layer.masksToBounds = YES;
    
    [self.imgThumbnail sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[description dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    self.lblDescription.attributedText = attrStr;
    
}

@end
