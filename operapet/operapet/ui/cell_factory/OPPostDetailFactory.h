//
//  OPPostDetailFactory.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListCellFactory.h"
#import "OPPost.h"

#import "OPPostDetailCommentView.h"
#import "OPPostDetailTextView.h"

@interface OPPostDetailFactory : BaseListCellFactory

@property OPPost *post;
@property NSInteger currentImageIndex;
@property OPPostDetailCommentView *templateComment;
@property OPPostDetailTextView *templateText;

@property void(^onLike)(OPPost *post);
@property void(^onShare)(OPPost *post);
@property void(^onLikeComment)(OPComment *comment);

-(void)loadPost:(OPPost *)post;

@end
