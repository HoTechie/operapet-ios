//
//  OPPostDetailCommentView.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "HTView.h"
#import "OPComment.h"

@interface OPPostDetailCommentView : HTView

@property (nonatomic) OPComment *comment;

@property (strong, nonatomic) IBOutlet UIImageView *imgProfilePic;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblText;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblLikeCount;
@property (strong, nonatomic) IBOutlet UIButton *btnLike;

@property void(^onLikeComment)(OPComment *comment);

@end
