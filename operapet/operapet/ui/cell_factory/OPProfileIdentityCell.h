//
//  OPProfileIdentityCell.h
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPProfile.h"
#import "OPPet.h"
#import "OPPetBorderView.h"

@interface OPProfileIdentityCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgProfilePic;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutlet UIButton *btnProfilePic;

@property (strong, nonatomic) IBOutlet OPPetBorderView *borderPetImage;
@property (strong, nonatomic) IBOutlet UIImageView *borderProfilePic;

@property (nonatomic) OPProfile *profile;
@property (nonatomic) OPPet *pet;

@end
