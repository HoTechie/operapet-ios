//
//  OPProfileMemberCell.m
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPProfileMemberCell.h"

@implementation OPProfileMemberCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.btnPointHelp addTarget:self action:@selector(onTapBtnHelp) forControlEvents:UIControlEventTouchUpInside];
}

- (void)onTapBtnHelp {
    if(self.onHelp) self.onHelp();
}

@end
