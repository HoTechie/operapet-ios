//
//  OPPetDetailBMICalculatorCell.m
//  operapet
//
//  Created by Gary KK on 18/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPetDetailBMICalculatorCell.h"

@interface OPPetDetailBMICalculatorCell()<UITextFieldDelegate>

@end

@implementation OPPetDetailBMICalculatorCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self configTextField:self.txtWeight];
    [self configTextField:self.txtLength];
    [self configTextField:self.txtBMI];
}

-(void)configTextField:(UITextField *)txtField {
    [txtField setDelegate:self];
    [txtField setReturnKeyType:UIReturnKeyDone];
    [txtField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

-(void)textFieldDidChange:(UITextField *)txtField {
    self.txtBMI.text = [NSString stringWithFormat:@"%0.2f",[self calculateBMI]];
    [self updateComment];
    
    if([self isValid] && self.onResult) {
        self.onResult(self.txtWeight.text, self.txtLength.text);
    }
}

- (float)calculateBMI {
    return self.txtWeight.text.floatValue / (self.txtLength.text.floatValue * self.txtLength.text.floatValue);
}

-(BOOL)isValid {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc]init];
    return [formatter numberFromString:self.txtBMI.text] && [[formatter numberFromString:self.txtBMI.text] floatValue] > 0;
}

-(void)updateComment {
    if([self isValid]) {
        float bmi = [self calculateBMI];
        self.lblBMI.text = [self commentForBMI:bmi];
        self.imgBMIWarning.image = [self isBMIPass:bmi] ? [UIImage imageNamed:@"normal"] : [UIImage imageNamed:@"overweight"];
        self.imgBMIWarning.hidden = NO;
    } else {
        self.lblBMI.text = @"";
        self.imgBMIWarning.hidden = YES;
    }
}

- (NSString *)commentForBMI:(float)bmi {
    if(bmi <= 18.0f) {
        return @"體重過輕";
    } else if (bmi <= 24.0f) {
        return @"正常範圍";
    } else if (bmi <= 27.0f) {
        return @"輕度肥胖";
    } else if (bmi <= 33.0f) {
        return @"中度肥胖";
    } else {
        return @"重度肥胖";
    }
}

- (BOOL)isBMIPass:(float)bmi {
    return (bmi > 18.0f && bmi <= 24.0f);
}

@end
