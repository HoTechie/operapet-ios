//
//  OPFriendFactory.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListCellFactory.h"

typedef enum{
    OPFriendFactoryModeDefaultList,
    OPFriendFactoryModeMyFriendList,
    OPFriendFactoryModeSearchResultList,
    OPFriendFactoryModeRequestList
}OPFriendFactoryMode;

@interface OPFriendFactory : BaseListCellFactory

@property (nonatomic) OPFriendFactoryMode mode;

@property NSArray *profiles;
@property NSArray *requests;

- (void)loadProfiles:(NSArray *)profiles requests:(NSArray *)requests;
- (void)loadProfiles:(NSArray *)profiles;

@property void(^onAcceptFriendRequest)(OPProfile * profile);
@property void(^onDeclineFriendRequest)(OPProfile * profile);
@property void(^onAddFriend)(OPProfile * profile);

@end
