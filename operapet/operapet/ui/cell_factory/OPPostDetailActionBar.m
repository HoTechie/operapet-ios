//
//  OPPostDetailActionBar.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPostDetailActionBar.h"
#import "Constants.h"

@implementation OPPostDetailActionBar

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.btnLike setBackgroundImage:[self.btnLike.currentBackgroundImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.btnShare setBackgroundImage:[self.btnShare.currentBackgroundImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    
    self.btnLike.tintColor = color_main_text_color_lighter;
    self.btnShare.tintColor = color_main_text_color_lighter;
    self.lblCommentCount.textColor = color_main_text_color_lighter;
    self.lblLikeCount.textColor = color_main_text_color_lighter;
    
    [self.btnLike addTarget:self action:@selector(onClickBtnLike) forControlEvents:UIControlEventTouchUpInside];
    [self.btnShare addTarget:self action:@selector(onClickBtnShare) forControlEvents:UIControlEventTouchUpInside];
}

- (void)onClickBtnLike {
    if(self.onLike) self.onLike(self.post);
}

- (void)onClickBtnShare {
    if(self.onShare) self.onShare(self.post);
}

- (void)setPost:(OPPost *)post {
    _post = post;
    
    self.btnLike.tintColor = post.is_like ? color_theme_color : color_main_text_color_lighter;
    self.lblLikeCount.text = [NSString stringWithFormat:ls(@"post_like_format"), (int)post.no_of_like];
    self.lblCommentCount.text = [NSString stringWithFormat:ls(@"post_comment_format"), (int)post.no_of_comment];
}



@end
