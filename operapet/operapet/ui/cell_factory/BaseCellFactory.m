//
//  BaseCellFactory.m
//  myislands
//
//  Created by alan tsoi on 1/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "BaseCellFactory.h"

@implementation CellModel

-(id)initWithData:(id)data renderer:(Renderer)renderer formater:(Formater)formater action:(Action)action {
    self = [super init];
    self.data = data;
    self.render = renderer;
    self.format = formater;
    self.action = action;
    return self;
}

+(instancetype)modelWithData:(id)data renderer:(Renderer)renderer formater:(Formater)formater action:(Action)action {
    return [[self alloc]initWithData:data renderer:renderer formater:formater action:action];
}

-(id)initWithData:(id)data renderer:(Renderer)renderer action:(Action)action {
    return [self initWithData:data renderer:renderer formater:nil action:action];
}

+(instancetype)modelWithData:(id)data renderer:(Renderer)renderer action:(Action)action {
    return [[self alloc]initWithData:data renderer:renderer action:action];
}

-(id)initWithData:(id)data action:(Action)action {
    return [self initWithData:data renderer:nil formater:nil action:action];
}

+(instancetype)modelWithData:(id)data action:(Action)action {
    return [[self alloc]initWithData:data action:action];
}

@end

@implementation BaseCellFactory

-(id)initWithCollectionView:(UICollectionView *)collectionView {
    self = [super init];
    self.collectionView = collectionView;
    [self registerCells];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.models = [NSArray array];
    return self;
}

-(void)registerCells {
    for (NSString *nib in [self cellNibs]) {
        [self.collectionView registerNib:[UINib nibWithNibName:nib bundle:nil] forCellWithReuseIdentifier:nib];
    }
    
    for(Class class in [self cellClasses]) {
        [self.collectionView registerClass:class forCellWithReuseIdentifier:NSStringFromClass(class)];
    }
}

-(void)setModels:(NSArray *)models {
    _models = models;
    [self.collectionView reloadData];
}

-(void)loadViewModels:(NSArray *)viewModels {
    _models = viewModels;
    [self.collectionView reloadData];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.models.count;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return [self cellInterSpacing];
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return [self cellLineSpacing];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CellModel *model = self.models[indexPath.item];
    return model.render(model, collectionView, indexPath);
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)collectionViewLayout;
    CGFloat parentWidth = collectionView.frame.size.width - layout.sectionInset.left - layout.sectionInset.right;
    CGFloat parentHeight = collectionView.frame.size.height;
    
    CellModel *model = self.models[indexPath.item];
    return model.format ? model.format(model, parentWidth, parentHeight) : [self cellDefaultSizeWithParentWidth:parentWidth height:parentHeight];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    CellModel *model = self.models[indexPath.item];
    if(model.action) model.action(model);
}

-(NSArray *)cellNibs {
    return [NSArray array];
}

-(NSArray *)cellClasses {
    return @[[UICollectionViewCell class]];
}

-(CGFloat)cellLineSpacing {
    return 0.0;
}

-(CGFloat)cellInterSpacing {
    return 0.0;
}

-(CGSize)cellDefaultSizeWithParentWidth:(CGFloat)parentWidth height:(CGFloat)parentHeight {
    return CGSizeZero;
}

-(void)loadData:(NSArray *)data { }




@end
