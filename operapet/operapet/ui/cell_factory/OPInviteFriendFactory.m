//
//  OPInviteFriendFactory.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPInviteFriendFactory.h"
#import "OPInviteFriendHeaderCell.h"
#import "OPInviteFriendNumberCell.h"
#import "OPInviteFriendSubmitCell.h"
#import "OPMemberService.h"

@interface OPInviteFriendFactory() <UITextFieldDelegate>
@end

@implementation OPInviteFriendFactory

-(id)initWithCollectionView:(UICollectionView *)collectionView {
    self = [super initWithCollectionView:collectionView];
    self.numbers = [NSMutableArray arrayWithArray:@[@"",@"",@"",@"",@""]];
    return self;
}

-(UIEdgeInsets)listInset {
    return UIEdgeInsetsZero;
}

-(CGFloat)cellLineSpacing {
    return 0.0;
}

-(CGFloat)cellInterSpacing {
    return 0.0;
}

-(CGFloat)cellHeight {
    return 65.0;
}

-(NSArray *)cellNibs {
    return @[
             NSStringFromClass([OPInviteFriendHeaderCell class]),
             NSStringFromClass([OPInviteFriendNumberCell class]),
             NSStringFromClass([OPInviteFriendSubmitCell class]),
             ];
}

-(void)loadData:(NSArray *)data {
    NSMutableArray *cms = [NSMutableArray array];
    
    CellModel *cm;
    cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPInviteFriendHeaderCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPInviteFriendHeaderCell class]) forIndexPath:indexPath];
        cell.lblText.text = ls(@"invite_friend_text");
        return cell;
    } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
        return CGSizeMake(pWidth, 130);
    } action:nil];
    [cms addObject:cm];
    
    for(NSString *number in self.numbers) {
        cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPInviteFriendNumberCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPInviteFriendNumberCell class]) forIndexPath:indexPath];
            [cell.txtNumber setText:number];
            [cell.txtNumber setIcon:[UIImage imageNamed:@"icon_phone"]];
            [cell.txtNumber setDelegate:self];
            [cell.txtNumber setReturnKeyType:UIReturnKeyDone];
            [cell.txtNumber setKeyboardType:UIKeyboardTypePhonePad];
            
            [cell.txtNumber removeTarget:self action:nil forControlEvents:UIControlEventAllEvents];
            [cell.txtNumber addTarget:self action:@selector(txtNumberEditingDidChange:) forControlEvents:UIControlEventEditingChanged];
            [cell.txtNumber setTag:indexPath.item];
            return cell;
        } action:^(CellModel *model) {
            
        }];
        [cms addObject:cm];
    }
    
    cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPInviteFriendSubmitCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPInviteFriendSubmitCell class]) forIndexPath:indexPath];
        [cell.button addTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
        return CGSizeMake(pWidth, 80);
    } action:^(CellModel *m){
        
    }];
    [cms addObject:cm];
    
    [self loadViewModels:cms];
}

- (void)submit {
    if(self.onSubmission) self.onSubmission(self.numbers);
}

- (void)txtNumberEditingDidChange:(UITextField *)txtFld {
    self.numbers[txtFld.tag] = txtFld.text;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

@end
