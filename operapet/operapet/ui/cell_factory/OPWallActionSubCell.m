//
//  OPWallActionSubCell.m
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPWallActionSubCell.h"
#import "Constants.h"

@implementation OPWallActionSubCell

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGFloat h = rect.size.height * 0.7;
    CGFloat w = 1;
    
    if(self.shouldDrawSeparator) {
        CGContextRef c = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(c, color_main_text_color_lighter.CGColor);
        CGContextFillRect(c, CGRectMake(rect.size.width - w, (rect.origin.y + rect.size.height - h) / 2.0, w, h));
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.shouldDrawSeparator = YES;
}

- (void)setShouldDrawSeparator:(BOOL)shouldDrawSeparator {
    _shouldDrawSeparator = shouldDrawSeparator;
    [self setNeedsDisplay];
}

- (void)setIcon:(UIImage *)img {
    self.imgIcon.image = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self updateUI];
}

- (void)updateUI {
    self.imgIcon.tintColor = self.checked ? color_theme_color : color_main_text_color_lighter;
    self.lblTitle.text = self.checked ? self.titleSelected : self.titleUnselected;
}

- (void)setChecked:(BOOL)checked{
    _checked = checked;
    [self updateUI];
}

- (void)setTitleSelected:(NSString *)titleSelected {
    _titleSelected = titleSelected;
    [self updateUI];
}

- (void)setTitleUnselected:(NSString *)titleUnselected {
    _titleUnselected = titleUnselected;
    [self updateUI];
}

@end
