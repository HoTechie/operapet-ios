//
//  ReviewDetailContentCell.h
//  myislands
//
//  Created by alan tsoi on 9/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewDetailContentCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblContent;

@end
