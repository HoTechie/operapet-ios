//
//  OPPostDetailTextView.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "HTView.h"
#import "OPPost.h"

@interface OPPostDetailTextView : HTView

@property (strong, nonatomic) IBOutlet UILabel *lblText;
@property (nonatomic) OPPost *post;

@end
