//
//  OPPetDetailPetTypeCell.h
//  operapet
//
//  Created by Gary KK on 18/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPPet.h"

@interface OPPetDetailPetTypeCell : UICollectionViewCell

@property (nonatomic) OPPet *pet;
@property (strong, nonatomic) IBOutlet UIButton *btnPetTypePrimary;
@property (strong, nonatomic) IBOutlet UIButton *btnPetTypeSecondary;

@end
