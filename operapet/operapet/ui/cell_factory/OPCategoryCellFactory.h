//
//  OPCategoryCellFactory.h
//  operapet
//
//  Created by Gary KK on 13/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListCellFactory.h"
#import "OPPostCategory.h"

@interface OPCategoryCellFactory : BaseListCellFactory

@property NSInteger currentCatId;
@property NSArray *cats;

@property void(^onChangeCategory)();

@end
