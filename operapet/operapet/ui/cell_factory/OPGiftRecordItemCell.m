//
//  OPGiftRecordItemCell.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPGiftRecordItemCell.h"
#import "OPServiceManager.h"
#import "SessionManager.h"
#import "Constants.h"

@implementation OPGiftRecordItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setGift:(OPGift *)gift {
    _gift = gift;
    
    self.lblDate.text = [gift formattedDateFromString:gift.create_time];
    self.lblName.text = gift.name;
    self.lblPoints.text = [@(gift.credit) stringValue];
    
    if(gift.images.count) {
        NSInteger imgId = [[gift.images firstObject] integerValue];
        [self.img sd_setImageWithURL:urlgiftimg(imgId)];
    }
    
    UIColor *color = [gift active] ? color_main_text_color_invert : color_theme_color_darker;
    self.lblRequired.textColor = color;
    self.lblPoints.textColor = color;
    self.lblName.textColor = [gift active] ? color_main_text_color : color_theme_color_darker;
    self.imgLeftRibbon.image = ![gift active] ? [UIImage imageNamed:@"gift_record_left_gery"] : [UIImage imageNamed:@"gift_record_left"];
    self.lblStatus.text = gift.statusString;
}


@end
