//
//  OPPetFactory.h
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListCellFactory.h"
#import "OPPetCell.h"
#import "OPPetAddCell.h"

@interface OPPetFactory : BaseListCellFactory
 
@property NSArray *pets;
@property UIImage *image;

@property void(^completion)();

@end
