//
//  OPPetFactory.m
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPetFactory.h"
#import "OPPetDetailViewController.h"

#import "OPNavigationController.h"
#import "OPPetDetailViewController.h"

#define CELL_PER_ROW    3

@implementation OPPetFactory

-(UIEdgeInsets)listInset {
    return UIEdgeInsetsZero;
}

-(CGFloat)cellLineSpacing {
    return 0.0;
}

-(CGFloat)cellInterSpacing {
    return 0.0;
}

-(NSArray *)cellNibs {
    return @[
             NSStringFromClass([OPPetCell class]),
             NSStringFromClass([OPPetAddCell class]),
             ];
}

-(void)loadData:(NSArray *)data {
    self.pets = data;
    [self updateUI];
}

- (CGSize)cellDefaultSizeWithParentWidth:(CGFloat)parentWidth height:(CGFloat)parentHeight {
    CGFloat d = parentWidth/CELL_PER_ROW;
    return CGSizeMake(d, d);
}

-(void)updateUI {
    NSMutableArray *cms = [NSMutableArray array];
    
    CellModel *cm;
    
    for(OPPet *pet in self.pets) {
        cm = [[CellModel alloc]initWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPPetCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPPetCell class]) forIndexPath:indexPath];
            [cell setPet:pet];
            return cell;
        } action:^(CellModel *model) {
            OPPetDetailViewController *vc = [[OPPetDetailViewController alloc]initWithPetId:pet.id];
            [[OPNavigationController sharedInstance]pushViewController:vc animated:YES];
            if(self.completion) vc.completion = self.completion;
            
        }];
        [cms addObject:cm];
    }
    
    cm = [[CellModel alloc]initWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPPetAddCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPPetAddCell class]) forIndexPath:indexPath];
        return cell;
    } action:^(CellModel *model) {
        OPPetDetailViewController *vc = [[OPPetDetailViewController alloc]init];
        [[OPNavigationController sharedInstance]pushViewController:vc animated:YES];
        if(self.completion) vc.completion = self.completion;
    }];
    [cms addObject:cm];
    
    [self loadViewModels:cms];
}


@end
