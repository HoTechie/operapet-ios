//
//  ImageCell.h
//  myislands
//
//  Created by alan tsoi on 2/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseCollectionViewCell.h"

@interface ImageCell : BaseCollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *img;

@end
