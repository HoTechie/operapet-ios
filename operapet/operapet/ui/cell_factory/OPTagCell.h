//
//  OPTagCell.h
//  operapet
//
//  Created by Gary KK on 14/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPTagView.h"

@interface OPTagCell : UICollectionViewCell

@property OPTagView *tagView;

@end
