//
//  BaseListCollectionViewCell.m
//  myislands
//
//  Created by alan tsoi on 3/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "BaseListCollectionViewCell.h"
#import "Constants.h"

@implementation BaseListCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.shouldShowBottomBorder = YES;
    self.bottomBorderColor = [UIColor whiteColor];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    if(self.shouldShowBottomBorder) {
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(context, self.bottomBorderColor.CGColor);
        CGContextFillRect(context, CGRectMake(0, rect.size.height - 1, rect.size.width, 1));
    }
}

@end
