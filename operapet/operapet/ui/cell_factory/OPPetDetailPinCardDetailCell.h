//
//  OPPetDetailPinCardDetailCell.h
//  operapet
//
//  Created by Gary KK on 21/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPPet.h"

IB_DESIGNABLE
@interface OPPetDetailPinCardDetailCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIButton *btnPen;

@property (strong, nonatomic) IBOutlet UITextField *txtLastPinYear;
@property (strong, nonatomic) IBOutlet UITextField *txtLastPinMonth;
@property (strong, nonatomic) IBOutlet UITextField *txtLastPinDay;
@property (strong, nonatomic) IBOutlet UITextField *txtNextPinYear;
@property (strong, nonatomic) IBOutlet UITextField *txtNextPinMonth;
@property (strong, nonatomic) IBOutlet UITextField *txtNextPinDay;
@property (strong, nonatomic) IBOutlet UITextField *txtRemarks;

@property (nonatomic) CGFloat posIndicator;
@property (nonatomic) OPPinCard *pincard;
@property (copy) void(^onSavePinCard)(OPPinCard *pincard);

@end
