//
//  MenuLanguageCell.m
//  myislands
//
//  Created by Gary KK on 27/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "MenuLanguageCell.h"

@implementation MenuLanguageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.btnLangEN addTarget:self action:@selector(onClickBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnLangZhHK addTarget:self action:@selector(onClickBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnLangZhCN addTarget:self action:@selector(onClickBtn:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)onClickBtn:(id)sender {
    if(sender == self.btnLangEN) {
        if(self.onClickBtnLangEN) self.onClickBtnLangEN();
    } else if(sender == self.btnLangZhHK) {
        if(self.onClickBtnLangZhHK) self.onClickBtnLangZhHK();
    } else if(sender == self.btnLangZhCN) {
        if(self.onClickBtnLangZhCN) self.onClickBtnLangZhCN();
    }
}

@end
