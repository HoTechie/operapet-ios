//
//  OPPostListCellFactory.h
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListCellFactory.h"
#import "OPPostListCell.h"

@interface OPPostListCellFactory : BaseListCellFactory

@property OPPostView *templateView;
@property NSArray *ads;

@property void(^onLike)(OPPost *post);
@property void(^onComment)(OPPost *post);
@property void(^onShare)(OPPost *post);

-(NSArray *)cellModelsForPosts:(NSArray *)data ads:(NSArray *)ads;

@end
