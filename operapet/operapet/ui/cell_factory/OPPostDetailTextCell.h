//
//  OPPostDetailTextCell.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPPostDetailTextView.h"

@interface OPPostDetailTextCell : UICollectionViewCell

@property OPPostDetailTextView *view;

@end
