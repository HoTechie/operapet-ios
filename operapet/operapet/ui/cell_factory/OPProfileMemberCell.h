//
//  OPProfileMemberCell.h
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OPProfileMemberCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblMemberLevel;
@property (strong, nonatomic) IBOutlet UILabel *lblPoint;
@property (strong, nonatomic) IBOutlet UIButton *btnPointHelp;
@property void(^onHelp)();

@end
