//
//  OPPetDetailPinCardSubCell.m
//  operapet
//
//  Created by Gary KK on 21/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPetDetailPinCardSubCell.h"
#import "Constants.h"

@implementation OPPetDetailPinCardSubCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectInset(rect, 5, 5) cornerRadius:4];
    CGContextAddPath(c, path.CGPath);
    
    if(self.selected) {
        CGContextSetFillColorWithColor(c,color_theme_color.CGColor);
        CGContextFillPath(c);
    } else {
        CGContextSetStrokeColorWithColor(c, [UIColor blackColor].CGColor);
        CGContextStrokePath(c);
    }
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    self.lblTitle.textColor = selected ? [UIColor whiteColor] : [UIColor blackColor];
    [self setNeedsDisplay];
}


@end
