//
//  TextMenuCellFactory.h
//  myislands
//
//  Created by alan tsoi on 2/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "BaseCellFactory.h"

@interface TextMenuItem : CellModel

@property UIColor *color;

+(instancetype)itemWithData:(id)data action:(Action)action;
+(instancetype)itemWithData:(id)data action:(Action)action color:(UIColor *)color;
-(id)initWithData:(id)data action:(Action)action color:(UIColor *)color;

@end

@interface TextMenuCellFactory : BaseCellFactory

-(id)initWithCollectionView:(UICollectionView *)collectionView separatorColor:(UIColor *)separatorColor tightPack:(BOOL)tightPack;

@property (nonatomic) NSArray *items;
@property UIColor *separatorColor;
@property BOOL tightPack;

@end
