//
//  OPFriendCellPetSubCell.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPFriendCellPetSubCell.h"
#import "UIImageView+WebCache.h"
#import "OPServiceManager.h"

@implementation OPFriendCellPetSubCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.img.layer.masksToBounds = YES;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.img.layer.cornerRadius = self.img.frame.size.height / 2.0;
}

- (void)setPet:(OPPet *)pet {
    _pet = pet;
    [self layoutIfNeeded];
    [self.img sd_setImageWithURL:urlpetimg(pet.image_id) placeholderImage:nil options:SDWebImageCacheMemoryOnly];
}

@end
