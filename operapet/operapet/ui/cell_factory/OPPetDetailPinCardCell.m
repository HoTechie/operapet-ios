//
//  OPPetDetailPinCardCell.m
//  operapet
//
//  Created by Gary KK on 18/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPetDetailPinCardCell.h"

@implementation OPPetDetailPinCardCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.factory = [[OPPetDetailPinCardFactory alloc]initWithCollectionView:self.collectionPincard];
    
}

- (void)setPet:(OPPet *)pet {
    _pet = pet;
    [self updateUI];
}

- (void)updateUI {
    [self layoutIfNeeded];
    [self.factory setPet:self.pet];
}



@end
