//
//  TextIconCell.h
//  myislands
//
//  Created by Gary KK on 3/10/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MILabelInset.h"

@interface TextIconCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet MILabelInset *lblText;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;

@end
