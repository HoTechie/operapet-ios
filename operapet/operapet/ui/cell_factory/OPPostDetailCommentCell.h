//
//  OPPostDetailCommentCell.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPPostDetailCommentView.h"
@interface OPPostDetailCommentCell : UICollectionViewCell

@property OPPostDetailCommentView *view;

@end
