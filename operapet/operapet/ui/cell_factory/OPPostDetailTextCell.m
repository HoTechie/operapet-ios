//
//  OPPostDetailTextCell.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPostDetailTextCell.h"

@implementation OPPostDetailTextCell

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    self.view = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([OPPostDetailTextView class]) owner:nil options:0].firstObject;
    [self.view addToSuperview:self];
    return self;
}


@end
