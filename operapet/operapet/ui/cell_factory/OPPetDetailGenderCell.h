//
//  OPPetDetailGenderCell.h
//  operapet
//
//  Created by Gary KK on 18/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPPet.h"
@interface OPPetDetailGenderCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIButton *btnMale;
@property (strong, nonatomic) IBOutlet UIButton *btnFemale;

@property (nonatomic) OPPet *pet;
@property (copy) void(^onChangeGender)();

@end
