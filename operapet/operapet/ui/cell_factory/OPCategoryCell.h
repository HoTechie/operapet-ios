//
//  OPCategoryCell.h
//  operapet
//
//  Created by Gary KK on 13/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPCategoryView.h"

@interface OPCategoryCell : UICollectionViewCell

@property OPCategoryView *catView;

@end
