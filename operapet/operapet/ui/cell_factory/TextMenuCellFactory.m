//
//  TextMenuCellFactory.m
//  myislands
//
//  Created by alan tsoi on 2/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "TextMenuCellFactory.h"
#import "TextMenuCell.h"

@implementation TextMenuItem

+(instancetype)itemWithData:(id)data action:(Action)action color:(UIColor *)color {
    return [[self alloc]initWithData:data action:action color:color];
}

+(instancetype)itemWithData:(id)data action:(Action)action {
    return [[self alloc]initWithData:data action:action];
}

-(id)initWithData:(id)data action:(Action)action color:(UIColor *)color {
    self = [super initWithData:data action:action];
    self.color = color;
    return self;
}

-(id)initWithData:(id)data action:(Action)action {
    self = [super initWithData:data action:action];
    self.color = [UIColor whiteColor];
    return self;
}

@end

@implementation TextMenuCellFactory

-(id)initWithCollectionView:(UICollectionView *)collectionView separatorColor:(UIColor *)separatorColor tightPack:(BOOL)tightPack{
    self = [self initWithCollectionView:collectionView];
    self.separatorColor = separatorColor;
    self.tightPack = tightPack;
    return self;
}

-(id)initWithCollectionView:(UICollectionView *)collectionView {
    self = [super initWithCollectionView:collectionView];
    
    self.tightPack = YES;
    self.separatorColor = [UIColor whiteColor];
    ((UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout).scrollDirection = UICollectionViewScrollDirectionHorizontal;
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)collectionView.collectionViewLayout;
    layout.estimatedItemSize = CGSizeMake(50, collectionView.frame.size.height);
    layout.sectionInset = UIEdgeInsetsZero;
    
    return self;
}

-(void)setItems:(NSArray *)items {
    NSArray *result = [NSArray arrayWithArray:items];
    for (TextMenuItem *item in items) {
        __weak TextMenuItem *_item = item;
        item.render = ^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            TextMenuCell * cell = [parent dequeueReusableCellWithReuseIdentifier:@"TextMenuCell" forIndexPath:indexPath];
            NSMutableAttributedString *aString;
            aString = self.tightPack ? [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@" | %@", model.data]] : [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"  %@  |", model.data]];
            
            if(self.tightPack) {
                [aString addAttribute:NSForegroundColorAttributeName value:self.separatorColor range:NSMakeRange(0, 3)];
                [aString addAttribute:NSForegroundColorAttributeName value:_item.color range:NSMakeRange(3, aString.length - 3)];
                cell.lblTitle.font = [UIFont fontWithName:@"Lucida Sans Unicode" size:14];
            } else {
                [aString addAttribute:NSForegroundColorAttributeName value:self.separatorColor range:NSMakeRange(aString.length - 2, 1)];
                [aString addAttribute:NSForegroundColorAttributeName value:_item.color range:NSMakeRange(0, aString.length - 1)];
            }
            
            cell.lblTitle.attributedText = aString;
            return cell;
        };
        item.format = ^CGSize(CellModel *model, CGFloat parentWidth, CGFloat parentHeight) {
            return CGSizeMake(30, parentHeight);
        };
    }
    
    self.models = result;
    [self.collectionView reloadData];
}

-(NSArray *)cellNibs {
    return @[@"TextMenuCell"];
}

@end
