//
//  OPPetDetailBMIRecordsCell.m
//  operapet
//
//  Created by Gary KK on 18/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPetDetailBMIRecordsCell.h"

@implementation OPPetDetailBMIRecordsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.txtBMI1Year.enabled = NO;
    self.txtBMI1Month.enabled = NO;
    self.txtBMI1Day.enabled = NO;
    self.txtBMI1Value.enabled = NO;
    
    self.txtBMI2Year.enabled = NO;
    self.txtBMI2Month.enabled = NO;
    self.txtBMI2Day.enabled = NO;
    self.txtBMI2Value.enabled = NO;
    
    self.txtBMI3Year.enabled = NO;
    self.txtBMI3Month.enabled = NO;
    self.txtBMI3Day.enabled = NO;
    self.txtBMI3Value.enabled = NO;
}


- (void)setPet:(OPPet *)pet {
    _pet = pet;
    [self updateUI];
}

- (void)updateUI {
    NSDateComponents *comps;
    if(self.pet.bmi_record_date_1.length) {
        comps = [self.pet dateCompsFromString:self.pet.bmi_record_date_1];
        self.txtBMI1Year.text = [@(comps.year) stringValue];
        self.txtBMI1Month.text = [@(comps.month) stringValue];
        self.txtBMI1Day.text = [@(comps.day) stringValue];
        self.txtBMI1Value.text = [self bmiWithWeight:self.pet.bmi_record_weight_1 length:self.pet.bmi_record_length_1].stringValue;
    }
    
    if(self.pet.bmi_record_date_2) {
        comps = [self.pet dateCompsFromString:self.pet.bmi_record_date_2];
        self.txtBMI2Year.text = [@(comps.year) stringValue];
        self.txtBMI2Month.text = [@(comps.month) stringValue];
        self.txtBMI2Day.text = [@(comps.day) stringValue];
        self.txtBMI2Value.text = [self bmiWithWeight:self.pet.bmi_record_weight_2 length:self.pet.bmi_record_length_2].stringValue;
    }
    
    if(self.pet.bmi_record_date_3) {
        comps = [self.pet dateCompsFromString:self.pet.bmi_record_date_3];
        self.txtBMI3Year.text = [@(comps.year)stringValue];
        self.txtBMI3Month.text = [@(comps.month) stringValue];
        self.txtBMI3Day.text = [@(comps.day) stringValue];
        self.txtBMI3Value.text = [self bmiWithWeight:self.pet.bmi_record_weight_3 length:self.pet.bmi_record_length_3].stringValue ;
    }
}

-(NSNumber *)bmiWithWeight:(float)weight length:(float)length {
    return @(roundf(weight / (length * length) * 100) / 100.0);
}

@end
