//
//  OPInviteFriendSubmitCell.h
//  operapet
//
//  Created by Gary KK on 26/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OPInviteFriendSubmitCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIButton *button;

@end
