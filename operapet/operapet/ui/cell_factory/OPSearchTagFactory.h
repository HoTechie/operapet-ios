//
//  OPSearchTagFactory.h
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListCellFactory.h"
#import "OPSearchTagCell.h"

@interface OPSearchTagFactory : BaseListCellFactory

@end
