//
//  OPCreatePostImageFactory.m
//  operapet
//
//  Created by Gary KK on 22/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPCreatePostImageFactory.h"

@implementation OPCreatePostImageFactory

-(UIEdgeInsets)listInset {
    return UIEdgeInsetsZero;
}

-(CGFloat)cellLineSpacing {
    return 0.0;
}

-(CGFloat)cellInterSpacing {
    return 0.0;
}

-(CGSize)cellDefaultSizeWithParentWidth:(CGFloat)parentWidth height:(CGFloat)parentHeight {
    return CGSizeMake(parentWidth/5.0, parentHeight);
}

-(NSArray *)cellNibs {
    return @[NSStringFromClass([ImageCell class])];
}

-(void)loadData:(NSArray *)data {
    NSMutableArray *cms = [NSMutableArray array];
    
    for(UIImage *image in data) {
        CellModel *cm = [CellModel modelWithData:image renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            ImageCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ImageCell class])
                                                                      forIndexPath:indexPath];
            [cell.img setImage:image];
            return cell;
        } action:^(CellModel *model) {
            
        }];
        [cms addObject:cm];
    }
    
    [self loadViewModels:cms];
}

@end
