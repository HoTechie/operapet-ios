//
//  OPPetDetailBMICalculatorCell.h
//  operapet
//
//  Created by Gary KK on 18/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OPPetDetailBMICalculatorCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UITextField *txtWeight;
@property (strong, nonatomic) IBOutlet UITextField *txtLength;
@property (strong, nonatomic) IBOutlet UITextField *txtBMI;
@property (strong, nonatomic) IBOutlet UILabel *lblBMI;
@property (strong, nonatomic) IBOutlet UIImageView *imgBMIWarning;
@property void(^onResult)(NSString *weight, NSString *length);

-(BOOL)isValid;

@end
