//
//  OPFriendCell.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPFriendCellPetSubFactory.h"
#import "OPProfile.h"

@interface OPFriendCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgProfilePic;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionPets;

/* All Requests Exclusive */
@property (strong, nonatomic) IBOutlet UILabel *lblCount;

/* Friend Request Exclusive */
@property (strong, nonatomic) IBOutlet UIButton *btnAccept;
@property (strong, nonatomic) IBOutlet UIButton *btnDecline;

/* Search Result Exclusive */
@property (strong, nonatomic) IBOutlet UIButton *btnAddFd;



@property OPFriendCellPetSubFactory *factory;
@property (nonatomic) OPProfile *profile;

@property void(^onAcceptFriendRequest)(OPProfile * profile);
@property void(^onDeclineFriendRequest)(OPProfile * profile);
@property void(^onAddFriend)(OPProfile * profile);

@end
