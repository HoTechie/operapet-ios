//
//  OPPetDetailPinCardFactory.h
//  operapet
//
//  Created by Gary KK on 21/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListCellFactory.h"
#import "OPPinCard.h"
#import "ImageCell.h"
#import "OPPetDetailPinCardSubCell.h"

@interface OPPetDetailPinCardFactory : BaseListCellFactory

@property (nonatomic) OPPet *pet;
@property (copy) void(^onCreatePincard)();
@property (copy) void(^onSelectPinCard)(OPPinCard * pincard);
@property OPPinCard *selectedPincard;

@end
