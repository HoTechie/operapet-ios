//
//  OPPetDetailPinCardFactory.m
//  operapet
//
//  Created by Gary KK on 21/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPetDetailPinCardFactory.h"

#import "OPNavigationController.h"
#import "OPPetPinCardViewController.h"

#define CELL_PER_ROW        3
@implementation OPPetDetailPinCardFactory

-(id)initWithCollectionView:(UICollectionView *)collectionView  {
    self = [super initWithCollectionView:collectionView];
    return self;
}

-(UIEdgeInsets)listInset {
    return UIEdgeInsetsMake(0, 10, 0, 10);
}

-(CGFloat)cellLineSpacing {
    return 0.0;
}

-(CGFloat)cellInterSpacing {
    return 00.0;
}

- (void)setPet:(OPPet *)pet {
    _pet = pet;
    [self updateUI];
}

-(NSArray*)cellNibs {
    return @[NSStringFromClass([ImageCell class]), NSStringFromClass([OPPetDetailPinCardSubCell class])];
}

-(void)updateUI {
    NSMutableArray *cellModels = [NSMutableArray array];
    CellModel *cm;
    
    OPPet *pet = self.pet;
    for(OPPinCard *pincard in self.pet.pin_card_list) {
        cm = [CellModel modelWithData:pincard renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPPetDetailPinCardSubCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPPetDetailPinCardSubCell class]) forIndexPath:indexPath];
            [cell.lblTitle setText:[NSString stringWithFormat:@"%@",pincard.name]];
            if(self.selectedPincard == pincard) cell.selected = YES;
            return cell;
        } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
            return CGSizeMake(pWidth/ CELL_PER_ROW, 35);
        } action:^(CellModel *model) {
            if(self.onSelectPinCard) self.onSelectPinCard(pincard);
        }];
        [cellModels addObject:cm];
    }
    
    cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        ImageCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ImageCell class]) forIndexPath:indexPath];
        [cell.img setImage:[UIImage imageNamed:@"btn_pin_card_add"]];
        [cell.img setContentMode:UIViewContentModeScaleAspectFit];
        return cell;
    } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
        return CGSizeMake(pWidth/ CELL_PER_ROW, 35);
    }  action:^(CellModel *model) {
        if(self.onCreatePincard) self.onCreatePincard();
    }];
    [cellModels addObject:cm];
    
    [self loadViewModels:cellModels];
}

@end
