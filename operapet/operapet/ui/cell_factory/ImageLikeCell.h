//
//  ImageLikeCell.h
//  myislands
//
//  Created by Gary KK on 1/10/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageLikeCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *lblLikeCount;

@end
