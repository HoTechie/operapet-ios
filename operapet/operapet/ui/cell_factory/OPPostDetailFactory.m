//
//  OPPostDetailFactory.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPostDetailFactory.h"

#import "OPPostDetailIdentityCell.h"
#import "OPPostDetailTextCell.h"
#import "OPPostDetailTagCell.h"
#import "OPPostDetailImageCell.h"
#import "OPPostDetailActionBar.h"
#import "OPPostDetailCommentCell.h"

#import "UIImageView+WebCache.h"
#import "OPServiceManager.h"

@implementation OPPostDetailFactory

-(UIEdgeInsets)listInset {
    return UIEdgeInsetsZero;
}

-(CGFloat)cellLineSpacing {
    return 0.0;
}

-(CGFloat)cellInterSpacing {
    return 0.0;
}

-(CGFloat)cellHeight {
    return 35.0;
}

-(NSArray *)cellNibs {
    return @[
             NSStringFromClass([OPPostDetailIdentityCell class]),
             NSStringFromClass([OPPostDetailTagCell class]),
             NSStringFromClass([OPPostDetailImageCell class]),
             NSStringFromClass([OPPostDetailActionBar class]),
             ];
}

-(NSArray *)cellClasses {
    return @[
             [OPPostDetailTextCell class],
             [OPPostDetailCommentCell class],
             [UICollectionViewCell class]
             ];
}

-(void)loadPost:(OPPost *)post {
    self.post = post;
    [self updateUI];
}

- (void)updateUI {
    NSMutableArray *cms = [NSMutableArray array];
    CellModel *cm ;
    
    __weak OPPostDetailFactory *_self = self;
    OPPost *post = self.post;
    
    cm = [CellModel modelWithData:self.post renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPPostDetailIdentityCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPPostDetailIdentityCell class]) forIndexPath:indexPath];
        cell.post = post;
        return cell;
    } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
        return CGSizeMake(pWidth, 60);
    } action:^(CellModel *model) {
        
    }];
    [cms addObject:cm];
    
    cm = [CellModel modelWithData:self.post renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPPostDetailTextCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPPostDetailTextCell class]) forIndexPath:indexPath];
        cell.view.post = post;
        return cell;
    } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
        self.templateText = self.templateText ? self.templateText : [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([OPPostDetailTextView class]) owner:nil options:0].firstObject;
        self.templateText.post = post;
        [self.templateText setMaxWidth:pWidth];
        CGFloat height = [self.templateText systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
        return CGSizeMake(pWidth, height);
    } action:^(CellModel *model) {
        
    }];
    [cms addObject:cm];
    
    if(self.post.tags && self.post.tags.count) {
        cm = [CellModel modelWithData:self.post renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPPostDetailTagCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPPostDetailTagCell class]) forIndexPath:indexPath];
            cell.post = post;
            return cell;
        } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
            return CGSizeMake(pWidth, 25);
        } action:^(CellModel *model) {
            
        }];
        [cms addObject:cm];
        
        cm = [CellModel modelWithData:self.post renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            return [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class]) forIndexPath:indexPath];
        } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
            return CGSizeMake(pWidth, 10);
        } action:nil];
        [cms addObject:cm];
    }
    
    if(post.images && post.images.count) {
        cm = [CellModel modelWithData:self.post renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPPostDetailImageCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPPostDetailImageCell class]) forIndexPath:indexPath];
            if(_self.currentImageIndex < post.images.count) {
                NSInteger imageId = [post.images[_self.currentImageIndex] integerValue];
                [cell.img sd_setImageWithURL:urlpostimg(imageId)];
            }
            
            [cell.btnNext removeTarget:_self action:nil forControlEvents:UIControlEventAllEvents];
            [cell.btnPrev removeTarget:_self action:nil forControlEvents:UIControlEventAllEvents];
            [cell.btnNext addTarget:_self action:@selector(onClickBtnNext) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnPrev addTarget:_self action:@selector(onClickBtnPrev) forControlEvents:UIControlEventTouchUpInside];
            
            cell.btnPrev.enabled = (_self.currentImageIndex != 0);
            cell.btnNext.enabled = _self.currentImageIndex < post.images.count - 1;
            return cell;
        } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
            return CGSizeMake(pWidth, pWidth);
        } action:^(CellModel *model) {
            
        }];
        [cms addObject:cm];
    }
    
    cm = [CellModel modelWithData:self.post renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPPostDetailActionBar *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPPostDetailActionBar class]) forIndexPath:indexPath];
        cell.post = post;
        cell.onLike = self.onLike;
        cell.onShare = self.onShare;
        return cell;
    } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
        return CGSizeMake(pWidth, 50);
    } action:^(CellModel *model) {
        
    }];
    [cms addObject:cm];
    
    for(OPComment *comment in post.cms) {
        cm = [CellModel modelWithData:self.post renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPPostDetailCommentCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPPostDetailCommentCell class]) forIndexPath:indexPath];
            cell.view.comment = comment;
            cell.view.onLikeComment = self.onLikeComment;
            return cell;
        } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
            self.templateComment = self.templateComment ? self.templateComment : [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([OPPostDetailCommentView class]) owner:nil options:0].firstObject;
            self.templateComment.comment = comment;
            self.templateComment.comment = comment;
            [self.templateComment setMaxWidth:pWidth];
            
            CGFloat height = [self.templateComment systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
            
            return CGSizeMake(pWidth, height);
        } action:^(CellModel *model) {
            
        }];
        [cms addObject:cm];
    }
    
    [self loadViewModels:cms];
}

- (void)onClickBtnNext {
    self.currentImageIndex = MOD((self.currentImageIndex + 1), self.post.images.count);
    [self updateUI];
}

- (void)onClickBtnPrev {
    self.currentImageIndex = MOD((self.currentImageIndex - 1), self.post.images.count);
    [self updateUI];
}

@end
