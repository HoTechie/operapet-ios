//
//  OPFriendCellPetSubCell.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OPPet.h"

@interface OPFriendCellPetSubCell : UICollectionViewCell

@property (nonatomic) OPPet *pet;
@property (strong, nonatomic) IBOutlet UIImageView *img;

@end
