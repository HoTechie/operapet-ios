//
//  OPPostDetailCommentCell.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPostDetailCommentCell.h"

@implementation OPPostDetailCommentCell

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    self.view = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([OPPostDetailCommentView class]) owner:nil options:0].firstObject;
    [self.view addToSuperview:self];
    return self;
}

@end
