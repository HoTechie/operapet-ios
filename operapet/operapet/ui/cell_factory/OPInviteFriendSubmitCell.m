//
//  OPInviteFriendSubmitCell.m
//  operapet
//
//  Created by Gary KK on 26/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPInviteFriendSubmitCell.h"

@implementation OPInviteFriendSubmitCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.button.layer.masksToBounds = YES;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.button.layer.cornerRadius = self.button.frame.size.height / 2.0;
}

@end
