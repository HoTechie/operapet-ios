//
//  OPCategoryCellFactory.m
//  operapet
//
//  Created by Gary KK on 13/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPCategoryCellFactory.h"
#import "OPCategoryCell.h"

@implementation OPCategoryCellFactory

-(id)initWithCollectionView:(UICollectionView *)collectionView {
    self = [super initWithCollectionView:collectionView];
    ((UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout).scrollDirection = UICollectionViewScrollDirectionHorizontal;
    return self;
}

-(UIEdgeInsets)listInset {
    return UIEdgeInsetsMake(0, 10, 0, 10);
}

-(CGFloat)cellLineSpacing {
    return 0.0;
}

-(CGFloat)cellInterSpacing {
    return 10.0;
}

-(void)loadData:(NSArray *)data {
    self.cats = data;
    [self updateUI];
}

-(NSArray*)cellClasses {
    return @[[OPCategoryCell class]];
}

-(void)updateUI {
    NSMutableArray *cellModels = [NSMutableArray array];
    CellModel *cm;
    for(OPPostCategory *cat in self.cats) {
        cm = [CellModel modelWithData:cat renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPCategoryCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPCategoryCell class]) forIndexPath:indexPath];
            [cell.catView setPostCat:cat];
            [cell.catView setSelected:cat.id == self.currentCatId];
            return cell;
        } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
            OPCategoryView *catView = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([OPCategoryView class]) owner:nil options:0].firstObject;
            [catView setPostCat:cat];
            [catView setMaxHeight:pHeight];
            
            CGFloat width = [catView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].width;
            return CGSizeMake(width, pHeight);
        } action:^(CellModel *model) {
            self.currentCatId = cat.id;
            [self updateUI];
            
            if(self.onChangeCategory) self.onChangeCategory();
        }];
        [cellModels addObject:cm];
    }
    
    self.models = cellModels;
}

@end
