//
//  OPPetDetailFactory.h
//  operapet
//
//  Created by Gary KK on 18/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListCellFactory.h"
#import "OPViewController.h"

#import "OPProfileIdentityCell.h"
#import "OPProfileHeaderCell.h"
#import "OPPetDetailStandardCell.h"

#import "OPPetDetailGenderCell.h"
#import "OPPetDetailPetTypeCell.h"
#import "OPPetDetailPinCardCell.h"
#import "OPPetDetailLastBodyCheckCell.h"
#import "OPPetDetailBMICalculatorCell.h"
#import "OPPetDetailBMIRecordsCell.h"
#import "OPPetDetailPinCardDetailCell.h"

@interface OPPetDetailFactory : BaseListCellFactory<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, readonly) OPPet *pet;
@property (weak) OPViewController *vc;
@property BOOL isBMIRecordInserted;
@property OPPinCard *expandedPincard;
@property UIImage *petImage;
@property void(^onDelete)(OPPet *pet);

-(void)loadPet:(OPPet *)pet fromVC:(OPViewController *)vc onDelete:(void(^)(OPPet *pet))onDelete;

@end
