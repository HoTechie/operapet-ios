//
//  OPGiftRecordFactory.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPGiftRecordFactory.h"
#import "OPGiftRecordItemCell.h"

@implementation OPGiftRecordFactory

-(UIEdgeInsets)listInset {
    return UIEdgeInsetsZero;
}

-(CGFloat)cellLineSpacing {
    return 0.0;
}

-(CGFloat)cellInterSpacing {
    return 0.0;
}

-(CGFloat)cellHeight {
    return 160.0;
}

-(NSArray *)cellNibs {
    return @[
             NSStringFromClass([OPGiftRecordItemCell class]),
             ];
}

-(void)loadData:(NSArray *)data {
    NSMutableArray *cms = [NSMutableArray array];
    
    CellModel *cm;
    for(OPGift *gift in data) {
        cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPGiftRecordItemCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPGiftRecordItemCell class]) forIndexPath:indexPath];
            cell.gift = gift;
            return cell;
        } action:^(CellModel *model) {
            if(self.onCollectGift) self.onCollectGift(gift);
        }];
        [cms addObject:cm];
    }
    
    [self loadViewModels:cms];
}

@end
