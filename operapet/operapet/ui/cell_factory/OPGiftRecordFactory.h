//
//  OPGiftRecordFactory.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListCellFactory.h"
#import "OPGift.h"

@interface OPGiftRecordFactory : BaseListCellFactory

@property void(^onCollectGift)(OPGift *gift);


@end
