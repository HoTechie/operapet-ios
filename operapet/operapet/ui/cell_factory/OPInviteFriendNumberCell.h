//
//  OPInviteFriendNumberCell.h
//  operapet
//
//  Created by Gary KK on 26/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTTextField.h"

@interface OPInviteFriendNumberCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet HTTextField *txtNumber;

@end
