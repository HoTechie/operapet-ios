//
//  OPGiftFactory.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPGiftFactory.h"
#import "OPGiftPointsCell.h"
#import "OPGiftItemCell.h"
#import "OPGiftDetailViewController.h"
#import "OPNavigationController.h"
#import "OPAdService.h"
#import "ImageCell.h"

@implementation OPGiftFactory

-(UIEdgeInsets)listInset {
    return UIEdgeInsetsZero;
}

-(CGFloat)cellLineSpacing {
    return 0.0;
}

-(CGFloat)cellInterSpacing {
    return 0.0;
}

-(CGFloat)cellHeight {
    return 160.0;
}

-(NSArray *)cellNibs {
    return @[
             NSStringFromClass([OPGiftPointsCell class]),
             NSStringFromClass([OPGiftItemCell class]),
             NSStringFromClass([ImageCell class]),
             ];
}

- (void)tryFetchAdsWithData:(NSArray *)data {
    [[[OPAdService alloc]init]getAdvListByType:[[HTListResponseHandler alloc]initWithOnResult:^(NSArray *array) {
        self.ads = array;
        [self loadData:data];
    } onFailure:^(NSInteger code, NSString *str) {
        
    }] type_id:ADV_TYPE_GIFT];
}

-(void)loadData:(NSArray *)data {
    if(!self.ads) [self tryFetchAdsWithData:data];
    
    NSMutableArray *cms = [NSMutableArray array];
    
    CellModel *cm;
    
    if(self.ads && self.ads.count) {
        OPAdvert *ad = self.ads[0];
        
        if(ad.image_id > 0) {
            [cms addObject:[CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
                ImageCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ImageCell class]) forIndexPath:indexPath];
                cell.img.contentMode = UIViewContentModeScaleAspectFill;
                [cell.img sd_setImageWithURL:[NSURL URLWithString:[[OPServiceManager sharedInstance]getAdvImageURLForId:ad.image_id]]];
                return cell;
            } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
                return CGSizeMake(pWidth, pWidth * (151.0/455));
            } action:^(CellModel *model) {
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:ad.link]];
            }]];
        }
    }
    
    cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPGiftPointsCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPGiftPointsCell class]) forIndexPath:indexPath];
        [cell layoutIfNeeded];
        [cell setProfile:[SessionManager sharedInstance].currentProfile];
        return cell;
    } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
        return CGSizeMake(pWidth, 200);
    } action:nil];
    [cms addObject:cm];
    

    for(OPGift *gift in data) {
        cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPGiftItemCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPGiftItemCell class]) forIndexPath:indexPath];
            [cell setGift:gift];
            return cell;
        } action:^(CellModel *model) {
            if([SessionManager sharedInstance].currentProfile.remain_credit >= gift.credit) {
                [[OPNavigationController sharedInstance]pushViewController:[[OPGiftDetailViewController alloc]initWithGiftId:gift.id] animated:YES];
            }
        }];
        [cms addObject:cm];
    }
    [self loadViewModels:cms];
}

@end
