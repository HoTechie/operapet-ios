//
//  OPSearchTagCell.m
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPSearchTagCell.h"
#import "UIImageView+WebCache.h"
#import "OPServiceManager.h"

@implementation OPSearchTagCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setOPTag:(OPTag*)optag {
    [self.imgThumbnail sd_setImageWithURL:[NSURL URLWithString:[[OPServiceManager sharedInstance]getProfileImageURLForId:optag.image_id]]];
    [self.lblName setText:[NSString stringWithFormat:@"# %@",optag._description]];
}

@end
