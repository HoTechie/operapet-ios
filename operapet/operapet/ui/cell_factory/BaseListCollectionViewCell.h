//
//  BaseListCollectionViewCell.h
//  myislands
//
//  Created by alan tsoi on 3/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "BaseCollectionViewCell.h"

@interface BaseListCollectionViewCell : BaseCollectionViewCell

@property BOOL shouldShowBottomBorder;
@property UIColor *bottomBorderColor;

@end
