//
//  OPSearchTagFactory.m
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPSearchTagFactory.h"

#import "OPNavigationController.h"
#import "OPPostByTagListViewController.h"

@implementation OPSearchTagFactory

-(UIEdgeInsets)listInset {
    return UIEdgeInsetsZero;
}

-(CGFloat)cellLineSpacing {
    return 0.0;
}

-(CGFloat)cellInterSpacing {
    return 0.0;
}

-(CGFloat)cellHeight {
    return 85.0;
}

-(NSArray *)cellNibs {
    return @[NSStringFromClass([OPSearchTagCell class])];
}

-(void)loadData:(NSArray *)data {
    NSMutableArray *cms = [NSMutableArray array];
    
    for(OPTag *tag in data) {
        CellModel *cm = [CellModel modelWithData:tag renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPSearchTagCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPSearchTagCell class])
                                                                      forIndexPath:indexPath];
            [cell setOPTag:tag];
            cell.bottomBorderColor = color_main_separator;
            return cell;
        } action:^(CellModel *model) {
            [[OPNavigationController sharedInstance]pushViewController:[[OPPostByTagListViewController alloc]initWithOPTag:tag] animated:YES];
        }];
        [cms addObject:cm];
    }
    
    [self loadViewModels:cms];
}


@end
