//
//  MIAdBanner.m
//  myislands
//
//  Created by Gary KK on 3/10/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "MIAdBanner.h"
#import "AdvertiseService.h"
#import "SDWebImageManager.h"
#import "BaseNavigationController.h"
#import "SessionManager.h"

@interface MIAdBanner()
@property Advertise *ad;
@end

@implementation MIAdBanner

-(void)awakeFromNib {
    [super awakeFromNib];
    [self addTarget:self action:@selector(onClickAd) forControlEvents:UIControlEventTouchUpInside];
}

-(void)didMoveToSuperview {
    [super didMoveToSuperview];
    [self loadAd];
}

-(void)onClickAd {
    NSString *path = [NSString stringWithFormat:@"%@/%@", self.ad.target_type, [@(self.ad.target_id) stringValue]];
    [[BaseNavigationController sharedInstance]navigateToPath:path animated:NO];
}

-(void)loadAd {
    AdvertiseService *svc = [AdvertiseService new];
    [svc getAdvertise:[[MIModelResponseHandler alloc]initWithOnResult:^(id object) {
        self.ad = object;
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:[NSURL URLWithString:self.ad.image.url]
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    [self setBackgroundImage:image forState:UIControlStateNormal];
                                }
                            }];
        
    } onFailure:^(NSString *msg){
        
    }] island_id:[SessionManager sharedInstance].currentIslandId type:AdvertiseTypeFixedBanner];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
