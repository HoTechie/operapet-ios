//
//  MIBorderedButton.h
//  myislands
//
//  Created by alan tsoi on 10/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface MIBorderedButton : UIButton

@end
