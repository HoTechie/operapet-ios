//
//  OPScoreChartView.h
//  operapet
//
//  Created by Gary KK on 19/4/2017.
//  Copyright © 2017 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface OPScoreChartView : UIView

@property (nonatomic) CGFloat progress;

@end
