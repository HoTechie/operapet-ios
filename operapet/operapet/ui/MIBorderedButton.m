//
//  MIBorderedButton.m
//  myislands
//
//  Created by alan tsoi on 10/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "MIBorderedButton.h"

@implementation MIBorderedButton


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(c, [UIColor blackColor].CGColor);
    CGContextFillRect(c, CGRectMake(0, 0, rect.size.width, 1));
    CGContextFillRect(c, CGRectMake(0, rect.size.height - 1, rect.size.width, 1));
}


@end
