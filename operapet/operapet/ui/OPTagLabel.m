//
//  OPTagLabel.m
//  operapet
//
//  Created by Gary KK on 14/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPTagLabel.h"
#import "Constants.h"

#define DEPTH_SLOPE     10
#define INSET_LEFT      20
#define INSET_RIGHT     10

@implementation OPTagLabel

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    CGFloat x = rect.origin.x;
    CGFloat y = rect.origin.y;
    CGFloat w = rect.size.width;
    CGFloat h = rect.size.height;
    CGFloat d = DEPTH_SLOPE;
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    
    CGContextBeginPath(c);
    CGContextMoveToPoint(c, x, y + h/2.0);
    CGContextAddLineToPoint(c, x + d, y);
    CGContextAddLineToPoint(c, w, y);
    CGContextAddLineToPoint(c, w, h);
    CGContextAddLineToPoint(c, x+d, h);
    CGContextClosePath(c);
    
    CGContextSetFillColorWithColor(c, color_tag_bg_color.CGColor);
    CGContextFillPath(c);
    
    [super drawRect:rect];
}


- (void)drawTextInRect:(CGRect)rect {
    UIEdgeInsets inset = UIEdgeInsetsMake(0, INSET_LEFT, 0, INSET_RIGHT);
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, inset)];
}

- (CGSize)intrinsicContentSize
{
    CGSize size = [super intrinsicContentSize];
    size.width  += (INSET_LEFT + INSET_RIGHT);
    return size;
}

@end
