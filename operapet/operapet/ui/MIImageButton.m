//
//  MIImageButton.m
//  myislands
//
//  Created by alan tsoi on 13/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "MIImageButton.h"

@implementation MIImageButton

-(void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    self.clipsToBounds = NO;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during anOimation.
- (void)drawRect:(CGRect)rect {
    CGFloat imgRatio = 0.6;
    
    if(self.image) {
        CGFloat desiredWidth = rect.size.height * imgRatio / (self.image.size.height / self.image.size.width);
        CGRect imgRect = CGRectMake((rect.size.width - desiredWidth) / 2, 3, desiredWidth, (rect.size.height - 3) * imgRatio);
        [self.image drawInRect:imgRect];
    }
    
    if(self.caption) {
        NSDictionary *dict = @{NSFontAttributeName:[UIFont fontWithName:@"Lucida Sans Unicode" size:12]};
        CGSize size = [self.caption sizeWithAttributes:dict];
        [self.caption drawInRect:CGRectMake((rect.size.width - size.width)/2.0, rect.size.height * imgRatio, rect.size.width, rect.size.height * (1 - imgRatio)) withAttributes:dict];
    }
    
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if(self.onClickButton) self.onClickButton();
}


@end
