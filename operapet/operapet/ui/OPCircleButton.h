//
//  OPCircleButton.h
//  myislands
//
//  Created by Gary KK on 27/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface OPCircleButton : UIButton

@end
