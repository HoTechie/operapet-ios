//
//  MISlideToAction.m
//  myislands
//
//  Created by Gary KK on 30/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "MISlideToAction.h"
#import "Constants.h"

#define SLIDE_RATIO_START 0.1

@interface MISlideToAction()
@property CGFloat slidedRatio;
@end

@implementation MISlideToAction

-(void)awakeFromNib {
    [super awakeFromNib];
    
    self.layer.borderWidth = 1.0;
    self.layer.borderColor = theme_color.CGColor;
    
    self.backgroundColor = [UIColor whiteColor];
}

- (void)drawRect:(CGRect)rect {
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(c, theme_color.CGColor);
    if(self.state == MISlideToActionStateTap) {
        CGContextFillRect(c, rect);
    } else {
        CGContextFillRect(c, CGRectMake(0, 0, rect.size.width * self.slidedRatio, rect.size.height));
    }
    
    UIFont *font = [UIFont fontWithName:@"Lucida Sans Unicode" size:15.0];
    CGFloat fontHeight = font.lineHeight;
    CGFloat yOffset = (rect.size.height - fontHeight) / 2.0;
    CGRect textRect = CGRectMake(0, yOffset, rect.size.width, fontHeight);
    
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    style.alignment = NSTextAlignmentCenter;
    NSDictionary *attr = @{NSFontAttributeName:font,
                           NSParagraphStyleAttributeName:style,
                           NSForegroundColorAttributeName: self.state == MISlideToActionStateSlideIdle ? theme_color : [UIColor whiteColor]};
    [(self.state == MISlideToActionStateTap ? self.titleTap : self.titleSlide) drawInRect:textRect withAttributes:attr];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    if(self.state == MISlideToActionStateSlideIdle) {
        UITouch *aTouch = [touches anyObject];
        CGPoint point = [aTouch locationInView:self];
        CGFloat ratio = point.x / self.frame.size.width;
        
        if(ratio < 0.2) self.state = MISlideToActionStateSlideSliding;
    } else if (self.state == MISlideToActionStateTap) {
        // Do nothing
    } else if (self.state == MISlideToActionStateSlideSliding) {
        // Should not happen
    } else if (self.state == MISlideToActionSlided) {
        // Do nothing
    }
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if(self.state == MISlideToActionStateSlideSliding) {
        UITouch *aTouch = [touches anyObject];
        CGPoint point = [aTouch locationInView:self];
        CGFloat ratio = point.x / self.frame.size.width;
        ratio = MAX(ratio, SLIDE_RATIO_START);
        ratio = MIN(ratio, 1.0);
        self.slidedRatio = ratio;
        [self setNeedsDisplay];
    } else if (self.state == MISlideToActionStateTap) {
        // Do nothing
    } else if (self.state == MISlideToActionStateSlideIdle) {
        // Do nothing
    } else if (self.state == MISlideToActionSlided) {
        // Do nothing
    }
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if(self.state == MISlideToActionStateSlideSliding) {
        UITouch *aTouch = [touches anyObject];
        CGPoint point = [aTouch locationInView:self];
        CGFloat ratio = point.x / self.frame.size.width;
        ratio = MAX(ratio, SLIDE_RATIO_START);
        ratio = MIN(ratio, 1.0);
        
        if(ratio >= 1.0) {
            self.state = MISlideToActionSlided;
            if(self.action) self.action();
        } else {
            ratio = SLIDE_RATIO_START;
            self.state = MISlideToActionStateSlideIdle;
        }
        
        self.slidedRatio = ratio;
        [self setNeedsDisplay];
    } else if(self.state == MISlideToActionStateTap) {
        self.state = MISlideToActionStateSlideIdle;
        if(self.tapAction) self.tapAction();
    } else if (self.state == MISlideToActionStateSlideIdle) {
        // Should not happen
    } else if (self.state == MISlideToActionSlided) {
        // Do nothing
    }

}

- (void)setState:(MISlideToActionState)state {
    _state = state;
    
    if(state == MISlideToActionStateSlideIdle) {
        self.slidedRatio = SLIDE_RATIO_START;
        [self setNeedsDisplay];
    }
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event {
    if(self.state == MISlideToActionStateSlideSliding) {
        UITouch *aTouch = [touches anyObject];
        CGPoint point = [aTouch locationInView:self];
        CGFloat ratio = point.x / self.frame.size.width;
        ratio = MAX(ratio, SLIDE_RATIO_START);
        ratio = MIN(ratio, 1.0);
        
        if(ratio >= 1.0) {
            self.state = MISlideToActionSlided;
            if(self.action) self.action();
        } else {
            ratio = SLIDE_RATIO_START;
            self.state = MISlideToActionStateSlideIdle;
        }
        
        self.slidedRatio = ratio;
        [self setNeedsDisplay];
    } else if(self.state == MISlideToActionStateTap) {
        self.slidedRatio = SLIDE_RATIO_START;
        self.state = MISlideToActionStateSlideIdle;
        [self setNeedsDisplay];
    } else if (self.state == MISlideToActionStateSlideIdle) {
        // Should not happen
    } else if (self.state == MISlideToActionSlided) {
        // Do nothing
    }
}

-(void)setTitleTap:(NSString *)titleTap {
    _titleTap = titleTap;
    [self setNeedsDisplay];
}

-(void)setTitleSlide:(NSString *)titleSlide {
    _titleSlide = titleSlide;
    [self setNeedsDisplay];
}

@end
