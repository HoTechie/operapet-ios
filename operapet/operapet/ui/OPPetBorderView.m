//
//  OPPetBorderView.m
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPetBorderView.h"

@implementation OPPetBorderView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(c, self.tintColor.CGColor);
    CGContextSetLineWidth(c, 1);
    CGContextStrokeEllipseInRect(c, CGRectInset(rect, 1, 1));
}

- (void)setTintColor:(UIColor *)tintColor {
    [super setTintColor:tintColor];
    [self setNeedsDisplay];
}


@end
