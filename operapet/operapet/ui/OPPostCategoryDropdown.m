//
//  OPPostCategoryDropdown.m
//  operapet
//
//  Created by Gary KK on 19/4/2017.
//  Copyright © 2017 Hotechie. All rights reserved.
//

#import "OPPostCategoryDropdown.h"
#import "OPConstants.h"
#import "TextCell.h"

#define DROPDOWN_WIDTH              120
#define DROPDOWN_ITEM_HEIGHT        35
#define DROPDOWN_SEPARATOR_HEIGHT   1

@implementation OPPostCategoryDropdown

+(id)showBelowSender:(UIView *)sender vc:(UIViewController *)vc onSelect:(void(^)(OPConstantPostCategory *cat))onSelect {
    OPPostCategoryDropdown * dropdown = [[OPPostCategoryDropdown alloc]initWithSender:sender vc:vc onSelect:onSelect];
    
    dropdown.translatesAutoresizingMaskIntoConstraints = NO;
    [vc.view addSubview:dropdown];
    [OPPostCategoryDropdown constraintView:dropdown inContainer:vc.view];
    
    return dropdown;
}

- (id)initWithSender:(UIView *)sender vc:(UIViewController *)vc onSelect:(void(^)(OPConstantPostCategory *cat))onSelect{
    self = [super init];
    
    self.onSelect = onSelect;
    CGRect senderFrame = [vc.view convertRect:sender.frame fromView:sender.superview];
    NSArray *cats = [OPConstants sharedInstance].postCategories;
    [self dismissInterceptorForView:self];
    [self dropdownWithCategories:cats forView:self position:CGPointMake(senderFrame.origin.x, senderFrame.origin.y + senderFrame.size.height)];
    
    return self;
}

-(UIView *)dropdownWithCategories:(NSArray *)cats forView:(UIView *)view position:(CGPoint)position {
    UIView *dropdown = [[UIView alloc]init];
    dropdown.backgroundColor = color_theme_color;
    dropdown.layer.shadowColor = [UIColor blackColor].CGColor;
    dropdown.layer.shadowRadius = 5.0;
    dropdown.layer.shadowOpacity = 0.5;
    dropdown.layer.shadowOffset = CGSizeMake(0.0, 3.0);
    dropdown.layer.masksToBounds = NO;
    [view addSubview:dropdown];
    dropdown.translatesAutoresizingMaskIntoConstraints = NO;
    [dropdown addConstraint:[NSLayoutConstraint constraintWithItem:dropdown attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:DROPDOWN_WIDTH]];
    [dropdown addConstraint:[NSLayoutConstraint constraintWithItem:dropdown attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:DROPDOWN_ITEM_HEIGHT * cats.count + DROPDOWN_SEPARATOR_HEIGHT * (cats.count - 1)]];
    [view addConstraint:[NSLayoutConstraint constraintWithItem:dropdown attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:position.x]];
    [view addConstraint:[NSLayoutConstraint constraintWithItem:dropdown attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeTop multiplier:1.0 constant:position.y]];
    
    UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:[[UICollectionViewFlowLayout alloc]init]];
    collectionView.backgroundColor = [UIColor clearColor];
    [dropdown addSubview:collectionView];
    [OPPostCategoryDropdown constraintView:collectionView inContainer:dropdown];
    
    self.factory = [[OPPostCategoryDropdownItemFactory alloc]initWithCollectionView:collectionView onSelect:^(OPConstantPostCategory *cat) {
        if(self.onSelect) self.onSelect(cat);
        [self dismiss];
    }];
    [self.factory loadData:cats];
    
    return dropdown;
}


-(UIView *)dismissInterceptorForView:(UIView *)view {
    UIButton *interceptor = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [view addSubview:interceptor];
    [OPPostCategoryDropdown constraintView:interceptor inContainer:view];
    
    [interceptor addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    
    return view;
}

+(void)constraintView:(UIView *)view inContainer:(UIView *)container {
    [view setTranslatesAutoresizingMaskIntoConstraints:NO];
    [container addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:container attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [container addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:container attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [container addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:container attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [container addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:container attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}

-(void)dismiss {
    [self removeFromSuperview];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

@implementation OPPostCategoryDropdownItemFactory

-(id)initWithCollectionView:(UICollectionView *)collectionView onSelect:(void (^)(OPConstantPostCategory *))onSelect {
    self = [super initWithCollectionView:collectionView];
    self.onSelect = onSelect;
    return self;
}

-(UIEdgeInsets)listInset {
    return UIEdgeInsetsZero;
}

-(CGFloat)cellLineSpacing {
    return 0;
}

-(CGFloat)cellInterSpacing {
    return 0;
}

-(NSArray *)cellNibs {
    return @[NSStringFromClass([TextCell class])];
}

-(void)loadData:(NSArray *)data {
    NSMutableArray *vms = [NSMutableArray array];
    
    for(OPConstantPostCategory *postCat in data) {
        [vms addObject:[CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            TextCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([TextCell class]) forIndexPath:indexPath];
            cell.lblText.text = postCat.name;
            [cell.lblText setTextAlignment:NSTextAlignmentCenter];
            cell.lblText.textColor = [UIColor whiteColor];
            cell.shouldShowBottomBorder = NO;
            return cell;
        } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
            return CGSizeMake(pWidth, DROPDOWN_ITEM_HEIGHT);
        } action:^(CellModel *model) {
            if(self.onSelect) self.onSelect(postCat);
        }]];
        
        if([data lastObject] != postCat) {
            [vms addObject:[CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
                UICollectionViewCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class]) forIndexPath:indexPath];
                cell.backgroundColor = [UIColor lightGrayColor];
                return cell;
            } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
                return CGSizeMake(pWidth, DROPDOWN_SEPARATOR_HEIGHT);
            } action:nil]];
        }
    }
    
    [self loadViewModels:vms];
}

@end





