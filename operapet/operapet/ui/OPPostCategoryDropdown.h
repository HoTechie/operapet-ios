//
//  OPPostCategoryDropdown.h
//  operapet
//
//  Created by Gary KK on 19/4/2017.
//  Copyright © 2017 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseListCellFactory.h"
#import "OPConstants.h"

@interface OPPostCategoryDropdown : UIView

+(id)showBelowSender:(UIView *)sender vc:(UIViewController *)vc onSelect:(void(^)(OPConstantPostCategory *cat))onSelect;


@property UIView *dropdown;
@property BaseListCellFactory *factory;
@property void(^onSelect)(OPConstantPostCategory *cat);

@end

@interface OPPostCategoryDropdownItemFactory : BaseListCellFactory

-(id)initWithCollectionView:(UICollectionView *)collectionView onSelect:(void(^)(OPConstantPostCategory *cat))onSelect;

@property void(^onSelect)(OPConstantPostCategory *cat);

@end

