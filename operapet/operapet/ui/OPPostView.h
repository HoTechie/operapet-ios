//
//  OPPostView.h
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "HTView.h"
#import "OPPost.h"
#import "OPTagFactory.h"

@interface OPPostView : HTView


@property (strong, nonatomic) IBOutlet UIImageView *imgPostBg;
@property (strong, nonatomic) IBOutlet UIImageView *imgActionBarBg;

@property (strong, nonatomic) IBOutlet UIImageView *imgIconCategory;
@property (strong, nonatomic) IBOutlet UILabel *lblCategory;
@property (strong, nonatomic) IBOutlet UIView *viewCatSeparator;
@property (strong, nonatomic) IBOutlet UIImageView *imgProfilePic;
@property (strong, nonatomic) IBOutlet UIButton *btnProfilePic;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblMemberLevel;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblContent;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionTags;

@property (strong, nonatomic) IBOutlet UIButton *btnLike;
@property (strong, nonatomic) IBOutlet UIButton *btnComment;
@property (strong, nonatomic) IBOutlet UIButton *btnShare;

@property (strong, nonatomic) IBOutlet UILabel *lblLikeCount;
@property (strong, nonatomic) IBOutlet UILabel *lblCommentCount;

@property (strong, nonatomic) IBOutlet UIImageView *imgPost;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintImageHeight;

@property (nonatomic) OPPost *post;
@property OPTagFactory *tagFactory;

@property void(^onLike)(OPPost *post);
@property void(^onComment)(OPPost *post);
@property void(^onShare)(OPPost *post);

@end
