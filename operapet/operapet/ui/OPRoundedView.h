//
//  OPRoundedView.h
//  operapet
//
//  Created by Gary KK on 13/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "HTView.h"

IB_DESIGNABLE
@interface OPRoundedView : HTView

@property (nonatomic) BOOL highlighted;

@end

