//
//  OPPetBorderView.h
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface OPPetBorderView : UIView

@end
