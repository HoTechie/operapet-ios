//
//  OPPostTopSection.m
//  operapet
//
//  Created by Gary KK on 13/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPostTopSection.h"

@implementation OPPostTopSection


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    self.layer.masksToBounds = NO;
    self.layer.shadowOffset = CGSizeMake(0, 0);
    self.layer.shadowRadius = 3;
    self.layer.shadowOpacity = 0.25;
    
    CGFloat r = 20;
    CGFloat x = rect.origin.x;
    CGFloat y = rect.origin.y;
    CGFloat h = rect.size.height;
    CGFloat w = rect.size.width;
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextMoveToPoint(c, x, h);
    CGContextAddLineToPoint(c, x, r);
    CGContextAddArcToPoint(c, x,y, x+r,y, r);
    CGContextAddLineToPoint(c, w-r, y);
    CGContextAddArcToPoint(c, w, y, w, r, r);
    CGContextAddLineToPoint(c, w, h);
    CGContextClosePath(c);
    
    CGContextSetFillColorWithColor(c, [UIColor whiteColor].CGColor);
    CGContextFillPath(c);
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [self setNeedsDisplay];
}
                    



@end
