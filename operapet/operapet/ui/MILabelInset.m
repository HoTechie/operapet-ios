//
//  MILabelInset.m
//  myislands
//
//  Created by Gary KK on 28/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "MILabelInset.h"

@implementation MILabelInset

- (void)drawTextInRect:(CGRect)rect {
    UIEdgeInsets inset = UIEdgeInsetsMake(0, self.leftInset, 0, 0);
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, inset)];
}

@end
