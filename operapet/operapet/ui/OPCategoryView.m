//
//  OPCategoryView.m
//  operapet
//
//  Created by Gary KK on 13/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPCategoryView.h"
#import "Constants.h"

@implementation OPCategoryView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib {
    [super awakeFromNib];
    self.selected = NO;
}

-(void)setSelected:(BOOL)selected {
    _selected = selected;
    
    self.roundedView.highlighted = selected;
    self.imgIndicator.hidden = !selected;
    
    self.imgIcon.tintColor = selected ? [UIColor whiteColor] : color_post_category_border;
    self.lblCatName.textColor = selected ? [UIColor whiteColor] : color_post_category_border;
}

-(void)setPostCat:(OPPostCategory *)postCat {
    _postCat = postCat;
    
    self.lblCatName.text = postCat.name;
    if(postCat.iconFileName) {
        self.imgIcon.image = [[UIImage imageNamed:postCat.iconFileName]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.imgIcon.hidden = NO;
        self.constraintHideIcon.active = NO;
    } else {
        self.imgIcon.hidden = YES;
        self.constraintHideIcon.active = YES;
    }
}

@end
