//
//  OPTagView.h
//  operapet
//
//  Created by Gary KK on 14/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "HTView.h"
#import "OPTag.h"
#import "OPTagLabel.h"


@interface OPTagView : HTView

@property (strong, nonatomic) IBOutlet OPTagLabel *lblTag;

@property (nonatomic) OPTag *opTag;

@end
