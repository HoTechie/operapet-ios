//
//  MISearchFieldInternal.h
//  myislands
//
//  Created by alan tsoi on 3/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MISearchFieldInternal : NSObject

@property UIView *view;
@property (weak, nonatomic) IBOutlet UIImageView *imgMagnifyGlass;
@property (weak, nonatomic) IBOutlet UITextField *txtSearchField;
@property (copy) void (^onDone)();

-(id)initWithParentView:(UIView *)parent;

@end
