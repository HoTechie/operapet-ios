//
//  MIImageButton.h
//  myislands
//
//  Created by alan tsoi on 13/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface MIImageButton : UIView

typedef void(^EventCallback)();

@property IBInspectable UIImage *image;
@property IBInspectable NSString *caption;
@property (copy) EventCallback onClickButton;

@end
