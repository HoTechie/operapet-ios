//
//  MISearchField.m
//  myislands
//
//  Created by alan tsoi on 3/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "MISearchField.h"
#import "Constants.h"

@implementation MISearchField

-(void)awakeFromNib {
    [super awakeFromNib];
    self.internal = [[MISearchFieldInternal alloc]initWithParentView:self];
}

-(void)setText:(NSString *)text {
    self.internal.txtSearchField.text = text;
}

-(NSString *)text {
    return self.internal.txtSearchField.text;
}

-(void)setShowBorder:(BOOL)showBorder {
    _showBorder = showBorder;
    
    self.layer.borderWidth = showBorder ? 1 : 0;
    self.layer.borderColor = main_border_color.CGColor;
}

-(void)setBackgroundColor:(UIColor *)backgroundColor {
    self.internal.view.backgroundColor = backgroundColor;
}

-(void)setOnDone:(void (^)())OnDone{
    self.internal.onDone = OnDone;
}

-(void(^)())onDone {
    return self.internal.onDone;
}

@end
