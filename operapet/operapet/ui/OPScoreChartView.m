//
//  OPScoreChartView.m
//  operapet
//
//  Created by Gary KK on 19/4/2017.
//  Copyright © 2017 Hotechie. All rights reserved.
//

#import "OPScoreChartView.h"
#import "Constants.h"

#define PADDING_RATIO 0.1
#define ARC_WIDTH   20
static inline double radians (double degrees) { return degrees * M_PI/180; }

@implementation OPScoreChartView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    CGContextRef c = UIGraphicsGetCurrentContext();
    
    CGFloat padding = rect.size.width * PADDING_RATIO;
    CGFloat maxY = CGRectGetMaxY(rect);
    CGFloat midX = CGRectGetMidX(rect);
    CGFloat r  = rect.size.width/2.0 - padding;
    
    CGMutablePathRef arc = CGPathCreateMutable();
    CGPathAddArc(arc, NULL, midX, maxY, r, radians(180), radians(180 + 180 * self.progress), FALSE);
    CGPathRef strokedArc = CGPathCreateCopyByStrokingPath(arc, NULL, ARC_WIDTH, kCGLineCapButt, kCGLineJoinMiter, 10);
    CGContextAddPath(c, strokedArc);
    CGContextSetFillColorWithColor(c, color_theme_color.CGColor);
    CGContextSetLineWidth(c, 0);
    CGContextDrawPath(c, kCGPathFillStroke);
}

- (void)setProgress:(CGFloat)progress {
    _progress = progress;
    [self setNeedsDisplay];
}


@end
