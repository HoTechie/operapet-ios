//
//  OPRoundedView.m
//  operapet
//
//  Created by Gary KK on 13/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPRoundedView.h"
#import "Constants.h"

@implementation OPRoundedView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    CGContextRef c = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(c, color_theme_color.CGColor);
    CGContextSetStrokeColorWithColor(c, color_post_category_border.CGColor);
    CGContextSetLineWidth(c, 2);
    
    if(self.highlighted){
        UIBezierPath *bezierPath = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:rect.size.height/2.0 * 0.8];
        [bezierPath fill];
    } else {
        CGRect tRect = CGRectInset(rect, 2, 2);
        UIBezierPath *bezierPath = [UIBezierPath bezierPathWithRoundedRect:tRect cornerRadius:rect.size.height/2.0 * 0.8];
        CGContextAddPath(c, bezierPath.CGPath);
        CGContextStrokePath(c);
    }
}

- (void)setHighlighted:(BOOL)highlighted {
    _highlighted = highlighted;
    [self setNeedsDisplay];
}


@end
