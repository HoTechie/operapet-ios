//
//  SessionManager.h
//  myislands
//
//  Created by alan tsoi on 3/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "OPProfile.h"
#import "OPPostCategory.h"
#import "HTFacebookUser.h"

@class MemberBenefit;

typedef enum {
    MILanguageEnglish,
    MILanguageChineseTraditional,
    MILanguageChineseSimpified
}MILanguage;

typedef void(^ActionResult)(NSString *res);
typedef void(^FbActionResult)(NSString *fb_id, NSString *token);
typedef void(^FbGraphResult)(BOOL success, HTFacebookUser *user);

@interface SessionManager : NSObject
+ (instancetype)sharedInstance;

@property (nonatomic) OPProfile *currentProfile;
@property NSMutableDictionary *bookmarks;
@property NSMutableDictionary *coupons;
@property (nonatomic) MILanguage language;
@property BOOL adShown;
@property (nonatomic) NSString *device_id;

- (NSArray *)postCategories;
- (OPPostCategory *)postCategoryForId:(NSInteger)id;

- (void)endSession;
- (void)bookmark:(NSString *)path description:(NSString *)description;
- (void)unbookmark:(NSString *)path;
- (NSString *)localizabeString:(NSString *)string;
- (NSString *)languageCodeForApp;
- (NSString *)languageCodeForAPI;
- (NSString *)currentIslandName;
- (void)loadIslands;
- (NSDictionary *)languageNames;
- (NSString *)currentLanguageName;

- (BOOL)isCouponDownloaded:(MemberBenefit *)coupon;
- (void)downloadCoupon:(MemberBenefit *)coupon;
- (void)removeCoupon:(MemberBenefit *)coupon;

- (void)loginWithFacebookWithSuccess:(FbActionResult)success error:(ActionResult)error;
- (void)retrieveFacebookUser:(FbGraphResult)onResult;

- (NSString *)memberLevelString:(NSInteger)level;
- (NSString *)formattedDateString:(NSString *)input;

@end
