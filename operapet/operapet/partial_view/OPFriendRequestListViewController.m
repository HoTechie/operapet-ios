//
//  OPFriendRequestListViewController.m
//  operapet
//
//  Created by Gary KK on 26/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPFriendRequestListViewController.h"

@interface OPFriendRequestListViewController ()

@end

@implementation OPFriendRequestListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getTitle {
    return ls(@"title_friend_request");
}

- (BaseListCellFactory *)createFactory {
    OPFriendFactory *factory = [[OPFriendFactory alloc]initWithCollectionView:self.listView];
    factory.mode = OPFriendFactoryModeRequestList;
    factory.onAcceptFriendRequest = ^(OPProfile * profile){
        [[[OPMemberService alloc]init]postAcceptFriend:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
            [self updateData:NO];
        } onFailure:^(NSInteger code, NSString *str) {
            
        }] target_member_id:profile.id];
    };
    factory.onDeclineFriendRequest = ^(OPProfile * profile) {
        [[[OPMemberService alloc]init]postDeleteFriend:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
            [self updateData:NO];
        } onFailure:^(NSInteger code, NSString *str) {
            
        }] friend_relationship_id:profile.friend_relation.friend_relationship_id];
    };
    return factory;
}

- (void)configUI {
    [super configUI];
    [self updateData:NO];
}

- (void)updateData:(BOOL)forceUpdate {
    [[[OPMemberService alloc]init]getFriendRequestList:[[HTListResponseHandler alloc]initWithOnResult:^(NSArray *array) {
        self.models = array;
        [self updateUI];
    } onFailure:^(NSInteger code, NSString *str) {
        
    }]];
}

- (void)updateUI {
    [super updateUI];
    [self.factory loadData:self.models];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
