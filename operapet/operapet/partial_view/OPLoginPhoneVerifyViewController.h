//
//  OPLoginPhoneVerifyViewController.h
//  operapet
//
//  Created by Gary KK on 7/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPLoginContentViewController.h"
#import "HTTextField.h"

@interface OPLoginPhoneVerifyViewController : OPLoginContentViewController

@property (weak, nonatomic) IBOutlet HTTextField *txtPhoneNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnEULA;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnViewTerms;

@end
