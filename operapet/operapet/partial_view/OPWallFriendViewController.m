//
//  OPWallFriendViewController.m
//  operapet
//
//  Created by Gary KK on 26/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPWallFriendViewController.h"
#import "OPMemberService.h"

@interface OPWallFriendViewController ()

@end

@implementation OPWallFriendViewController

- (id)initWithProfileId:(NSInteger)profileId {
    self = [super init];
    self.profileId = profileId;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getTitle {
    return ls(@"wall_friend");
}

- (BaseListCellFactory *)createFactory {
    OPFriendFactory *factory = [[OPFriendFactory alloc]initWithCollectionView:self.listView];
    return factory;
}

- (void)configUI {
    [super configUI];
    [self updateData:NO];
}

- (void)updateData:(BOOL)forceUpdate {
    [[[OPMemberService alloc]init]getFriendList:[[HTListResponseHandler alloc]initWithOnResult:^(NSArray *array) {
        self.models = array;
        [self updateUI];
    } onFailure:^(NSInteger code, NSString *str) {
        
    }] target_member_id:self.profileId];
}

- (void)updateUI {
    [self.factory loadData:self.models];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
