//
//  OPRegistrationViewController.m
//  operapet
//
//  Created by Gary KK on 10/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPRegistrationViewController.h"
#import "OPLoginViewController.h"
#import "OPTermsViewController.h"
#import "SessionManager.h"

@interface OPRegistrationViewController ()<UITextFieldDelegate>

@end

@implementation OPRegistrationViewController

- (id)initWithFacebookId:(NSString *)facebookId facebookToken:(NSString *)facebookToken {
    self = [super init];
    self.facebookId = facebookId;
    self.facebookToken = facebookToken;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString*)getNibName {
    return NSStringFromClass([self class]);
}

- (void)configUI {
    [super configUI];
    
    [self.txtLastName setIcon:[UIImage imageNamed:@"icon_login_name"]];
    [self.txtLastName setPlaceholder:@"姓氏"];
    [self.txtFirstName setIcon:[UIImage imageNamed:@"icon_login_name"]];
    [self.txtFirstName setPlaceholder:@"名字"];
    [self.txtEmail setPlaceholder:ls(@"profile_lb_email")];
    [self.txtEmail setIcon:[UIImage imageNamed:@"icon_login_email"]];
    [self.txtPhoneNumber setPlaceholder:ls(@"profile_lb_phone")];
    [self.txtPhoneNumber setIcon:[UIImage imageNamed:@"icon_login_mobile_no"]];
    
    [self.txtLastName setReturnKeyType:UIReturnKeyNext];
    [self.txtFirstName setReturnKeyType:UIReturnKeyNext];
    [self.txtEmail setReturnKeyType:UIReturnKeyNext];
    [self.txtPhoneNumber setReturnKeyType:UIReturnKeyDone];
    
    self.txtLastName.delegate = (id<UITextFieldDelegate>)self.parentViewController;
    self.txtFirstName.delegate = (id<UITextFieldDelegate>)self.parentViewController;
    self.txtEmail.delegate = (id<UITextFieldDelegate>)self.parentViewController;
    self.txtPhoneNumber.delegate = (id<UITextFieldDelegate>)self.parentViewController;
    
    [self.btnEULA addTarget:self action:@selector(onClickEULA) forControlEvents:UIControlEventTouchUpInside];
    [self.btnRegister addTarget:self action:@selector(onClickRegister) forControlEvents:UIControlEventTouchUpInside];
    [self.btnViewTerms addTarget:self action:@selector(onClickViewTerms) forControlEvents:UIControlEventTouchUpInside];
    
    if(self.facebookId) {
        [[SessionManager sharedInstance]retrieveFacebookUser:^(BOOL success, HTFacebookUser *user) {
            if(success && user) {
                self.txtLastName.text = user.last_name;
                self.txtFirstName.text = user.first_name;
                self.txtEmail.text = user.email;
            }
        }];
    }
}

- (void)onClickViewTerms {
    [[OPNavigationController sharedInstance]pushViewController:[[OPTermsViewController alloc]init] animated:YES];
}


- (void)onClickEULA {
    self.btnEULA.selected = !self.btnEULA.selected;
}

- (void)onClickRegister {
    if(!self.btnEULA.selected) {
        [self showMessageDialog:@"必須先同意條款" buttonPositive:@"確定" positiveHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {}];
        return;
        
        
    }
    
    if(self.txtPhoneNumber.text.length && self.txtEmail.text.length && self.txtFirstName.text.length && self.txtLastName.text.length) {
        if(!self.facebookId) [self normalRegistration];
        else [self facebookRegistration];
    } else {
        [self showMessageDialog:@"請輸入所有項目" buttonPositive:@"確定" positiveHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {
            
        }];
    }
}

- (void)normalRegistration {
    [[[OPMemberService alloc]init]postCreateMember:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
        [[OPNavigationController sharedInstance]pushViewController:[OPLoginViewController loginVerifySMSViewControllerWithPhoneNumber:self.txtPhoneNumber.text] animated:YES];
    } onFailure:^(NSInteger err, NSString *str) {
        [self showMessageDialog:@"請重試" buttonPositive:@"確定" positiveHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {}];
    }] device_id:[UIDevice currentDevice].identifierForVendor.UUIDString device_type:@"1" mobile_no:self.txtPhoneNumber.text lastname:self.txtLastName.text firstname:self.txtFirstName.text email:self.txtEmail.text];
}

- (void)facebookRegistration {
    [[[OPMemberService alloc]init]postCreateFbMember:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
        [[OPNavigationController sharedInstance]pushViewController:[OPLoginViewController loginVerifySMSViewControllerWithFacebookId:self.facebookId] animated:YES];
    } onFailure:^(NSInteger code, NSString *str) {
        [self showMessageDialog:@"請重試" buttonPositive:@"確定" positiveHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {}];
    }] device_id:[UIDevice currentDevice].identifierForVendor.UUIDString device_type:@"1" mobile_no:self.txtPhoneNumber.text lastname:self.txtLastName.text firstname:self.txtFirstName.text email:self.txtEmail.text facebook_id:self.facebookId facebook_token:self.facebookToken];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(self.txtLastName == textField) {
        [self.txtFirstName becomeFirstResponder];
    } else if (self.txtFirstName == textField) {
        [self.txtEmail becomeFirstResponder];
    } else if (self.txtEmail == textField) {
        [self.txtPhoneNumber becomeFirstResponder];
    }
    
    return NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
