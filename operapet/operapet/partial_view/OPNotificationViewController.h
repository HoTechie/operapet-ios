//
//  OPNotificationViewController.h
//  operapet
//
//  Created by Gary KK on 19/4/2017.
//  Copyright © 2017 Hotechie. All rights reserved.
//

#import "BaseListViewController.h"

@interface OPNotificationViewController : BaseListViewController

@property NSArray *notifications;

@end

@interface OPNotificationFactory : BaseListCellFactory

@end
