//
//  OPInviteFriendViewController.h
//  operapet
//
//  Created by Gary KK on 26/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListViewController.h"
#import "OPInviteFriendFactory.h"
#import "OPMemberService.h"

@interface OPInviteFriendViewController : BaseListViewController

@end
