//
//  OPMenuViewController.m
//  operapet
//
//  Created by Gary KK on 6/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPMenuViewController.h"
#import "MenuCellFactory.h"
#import "Constants.h"
#import "OPLoginViewController.h"
#import "UIImageView+WebCache.h"
#import "OPMainViewController.h"

#import "OPHomeViewController.h"
#import "OPProfileViewController.h"
#import "OPWallViewController.h"
#import "OPPetListViewController.h"
#import "OPGiftListViewController.h"
#import "OPInviteFriendViewController.h"
#import "OPAboutViewController.h"
#import "OPNotificationViewController.h"

@interface OPMenuViewController ()

@end

@implementation OPMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reload {
    [self loadMenuItems];
}

- (NSString *)getNibName {
    return NSStringFromClass([self class]);
}

- (BaseListCellFactory *)createFactory {
    return [[MenuCellFactory alloc]initWithCollectionView:self.listView];
}

- (void)onClickBtnLogout {
    [[SessionManager sharedInstance]endSession];
    [[OPNavigationController sharedInstance]setViewControllers:@[[OPLoginViewController loginOptionViewController]] animated:YES];
    [[OPMainViewController sharedInstance]closeLeftMenu];
}

-(void)loadMenuItems {
    NSMutableArray *cells = [NSMutableArray array];
    CellModel *cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        OPMenuUserCell *cell = [parent dequeueReusableCellWithReuseIdentifier:@"OPMenuUserCell" forIndexPath:indexPath];
        [cell layoutIfNeeded];
        [cell.imgUserIcon sd_setImageWithURL:[NSURL URLWithString:[[OPServiceManager sharedInstance]getProfileImageURLForId:[SessionManager sharedInstance].currentProfile.image_id]]];
        [cell.imgUserIcon.layer setCornerRadius:cell.imgUserIcon.frame.size.width/2.0];
        [cell.imgUserIcon.layer setMasksToBounds:YES];
        [cell.imgUserIcon setBackgroundColor:[UIColor lightGrayColor]];
        
        cell.lblUsername.text = [SessionManager sharedInstance].currentProfile.user_name;
        cell.lblUsername.textColor = [UIColor whiteColor];
        cell.bottomBorderColor = color_menu_element_border_color;
        return cell;
    } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
        return CGSizeMake(pWidth, 90);
    } action:^(CellModel *model) {
        [[OPNavigationController sharedInstance]replaceTopViewController:[[OPHomeViewController alloc]initWithInitialTab:OPHomeTabMyWall] animated:NO];
        [[OPMainViewController sharedInstance]closeLeftMenu];
    }];
    [cells addObject:cm];
    
    cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        MenuCell *cell = [parent dequeueReusableCellWithReuseIdentifier:@"MenuCell" forIndexPath:indexPath];
        cell.lblTitle.text = ls(@"title_profile");
        cell.imgIcon.image = [UIImage imageNamed:@"icon_personal_info"];
        return cell;
    } action:^(CellModel * model) {
        [[OPNavigationController sharedInstance]replaceTopViewController:[[OPProfileViewController alloc]init] animated:NO];
        [[OPMainViewController sharedInstance]closeLeftMenu];
    }];
    [cells addObject:cm];
    
    cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        MenuCell *cell = [parent dequeueReusableCellWithReuseIdentifier:@"MenuCell" forIndexPath:indexPath];
        cell.lblTitle.text = ls(@"title_pet");
        cell.imgIcon.image = [UIImage imageNamed:@"icon_pet"];
        return cell;
    } action:^(CellModel *model) {
        OPViewController *vc = [[OPPetListViewController alloc]init];
        [[OPNavigationController sharedInstance]replaceTopViewController:vc animated:NO];
        [[OPMainViewController sharedInstance]closeLeftMenu];
    }];
    [cells addObject:cm];
    
    cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        MenuCell *cell = [parent dequeueReusableCellWithReuseIdentifier:@"MenuCell" forIndexPath:indexPath];
        cell.lblTitle.text = ls(@"title_posts");
        cell.imgIcon.image = [UIImage imageNamed:@"icon_posts"];
        return cell;
    } action:^(CellModel *model) {
        [[OPNavigationController sharedInstance]replaceTopViewController:[[OPHomeViewController alloc]init] animated:NO];
        [[OPMainViewController sharedInstance]closeLeftMenu];
    }];
    [cells addObject:cm];
    
    cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        MenuCell *cell = [parent dequeueReusableCellWithReuseIdentifier:@"MenuCell" forIndexPath:indexPath];
        cell.lblTitle.text = ls(@"title_notification");
        cell.imgIcon.image = [UIImage imageNamed:@"icon_notification"];
        return cell;
    } action:^(CellModel *model) {
        [[OPNavigationController sharedInstance]replaceTopViewController:[[OPNotificationViewController alloc]init] animated:NO];
        [[OPMainViewController sharedInstance]closeLeftMenu];
    }];
    [cells addObject:cm];
    
    cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        MenuCell *cell = [parent dequeueReusableCellWithReuseIdentifier:@"MenuCell" forIndexPath:indexPath];
        cell.lblTitle.text = ls(@"title_gift");
        cell.imgIcon.image = [UIImage imageNamed:@"icon_gift"];
        return cell;
    } action:^(CellModel *model) {
        [[OPNavigationController sharedInstance]replaceTopViewController:[[OPGiftListViewController alloc]init] animated:NO];
        [[OPMainViewController sharedInstance]closeLeftMenu];
    }];
    [cells addObject:cm];
    
    cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        MenuCell *cell = [parent dequeueReusableCellWithReuseIdentifier:@"MenuCell" forIndexPath:indexPath];
        cell.lblTitle.text = ls(@"title_about");
        cell.imgIcon.image = [UIImage imageNamed:@"icon_about"];
        return cell;
    } action:^(CellModel *model) {
        [[OPNavigationController sharedInstance]pushViewController:[[OPAboutViewController alloc]init] animated:YES];
        [[OPMainViewController sharedInstance]closeLeftMenu];
    }];
    [cells addObject:cm];
    
    cm = [CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        MenuCell *cell = [parent dequeueReusableCellWithReuseIdentifier:@"MenuCell" forIndexPath:indexPath];
        cell.lblTitle.text = ls(@"title_invite_friend");
        cell.imgIcon.image = [UIImage imageNamed:@"icon_invite"];
        return cell;
    } action:^(CellModel *model) {
        [[OPNavigationController sharedInstance]replaceTopViewController:[[OPInviteFriendViewController alloc]init] animated:NO];
        [[OPMainViewController sharedInstance]closeLeftMenu];
    }];
    [cells addObject:cm];
    
    [self.factory loadData:cells];
}

- (void)configUI {
    [super configUI];
    
    [self.btnLogout addTarget:self action:@selector(onClickBtnLogout) forControlEvents:UIControlEventTouchUpInside];
    [self.btnLogout setTitle:ls(@"title_logout") forState:UIControlStateNormal];
    self.view.backgroundColor = color_theme_color;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
