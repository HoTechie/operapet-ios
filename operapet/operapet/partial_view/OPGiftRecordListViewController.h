//
//  OPGiftRecordListViewController.h
//  operapet
//
//  Created by Gary KK on 21/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListViewController.h"
#import "OPGiftRecordFactory.h"
#import "OPProfileService.h"
#import "OPGiftService.h"

@interface OPGiftRecordListViewController : BaseListViewController

@end
