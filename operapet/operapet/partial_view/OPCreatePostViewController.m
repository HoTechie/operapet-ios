//
//  OPCreatePostViewController.m
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPCreatePostViewController.h"
#import "OPPostCategoryDropdown.h"

@interface OPCreatePostViewController ()<UITextViewDelegate, UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@end

#define MAX_IMAGE_COUNT 5

@implementation OPCreatePostViewController

- (id)init {
    self = [super init];
    
    self.images = [NSMutableArray array];
    self.postCategory = 1;
    
    return self;
}

-(id)initWithCompletion:(void (^)())completion {
    self = [self init];
    self.completion = completion;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getNibName {
    return NSStringFromClass([OPCreatePostViewController class]);
}

- (void)configUI {
    [super configUI];
    
    self.scrollContentView = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([OPCreatePostViewController class]) owner:self options:0][1];
    [self.scrollView addSubview:self.scrollContentView];
    [self constrainView:self.scrollContentView inScrollView:self.scrollView];

    [self.view layoutIfNeeded];
    
    self.lblName.text = [SessionManager sharedInstance].currentProfile.user_name;
    [self.imgProfilePic sd_setImageWithURL:urlprofilepic([SessionManager sharedInstance].currentProfile.image_id)];
    self.viewTxtBackground.layer.cornerRadius = self.viewTxtBackground.frame.size.height/2.0;
    self.viewTxtBackground.layer.masksToBounds = YES;
    
    [self.btnCamera addTarget:self action:@selector(takePicture) forControlEvents:UIControlEventTouchUpInside];
    [self.btnSend addTarget:self action:@selector(send) forControlEvents:UIControlEventTouchUpInside];
    
    self.txtTag.delegate = self;
    self.txtTag.returnKeyType = UIReturnKeyDone;
    self.txtFldContent.delegate = self;
    self.factory = [[OPCreatePostImageFactory alloc]initWithCollectionView:self.collectionImages];
    [self updateUI];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if([string containsString:@" "]) {
        if(textField.text.length && [textField.text characterAtIndex:textField.text.length - 1] != ',' ) {
            textField.text = [textField.text stringByReplacingCharactersInRange:range withString:[string stringByReplacingOccurrencesOfString:@" " withString:@","]];
        }
        return NO;
    }
    
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
    self.lblContentPlaceholder.hidden = textView.text.length;
}

- (void)updateUI {
    [super updateUI];
    [self.factory loadData:self.images];
    
    OPPostCategory *cat = [SessionManager sharedInstance].postCategories[self.postCategory];
    [self.btnCategory setTitle:cat.name forState:UIControlStateNormal];
    [self.btnCategory addTarget:self action:@selector(selectPostCat) forControlEvents:UIControlEventTouchUpInside];
}

- (void)selectPostCat {
    [OPPostCategoryDropdown showBelowSender:self.btnCategory vc:self onSelect:^(OPConstantPostCategory *cat) {
        self.postCategory = cat.id;
        [self updateUI];
    }];
}

- (void)takePicture {
    [self.view endEditing:YES];
    [self keyboardDidHide];
    
    UIImagePickerController *controller = [[UIImagePickerController alloc]init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:controller.sourceType];
    controller.allowsEditing = NO;
    [[OPNavigationController sharedInstance]presentViewController:controller animated:YES completion:nil];
}

- (void)send {
    if(!self.txtFldContent.text.length) return;
    
    [self showProgressHUD];
    [[[OPPostsService alloc]init]postCreatePost:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
        [self hideProgressHUD];
        [[OPNavigationController sharedInstance]popViewControllerAnimated:YES];
        if(self.completion) self.completion();
    } onFailure:^(NSInteger code, NSString *str) {
        [self hideProgressHUD];
    }] description:self.txtFldContent.text category_id:self.postCategory tags_list_string:self.txtTag.text images:self.images];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    if(self.images.count > MAX_IMAGE_COUNT) return;
    
    UIImage *img = [info objectForKey: UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
    [self.images addObject:img];
    [self updateUI];
}

- (NSLayoutConstraint *)constraintBottomSpace {
    return self.scrollViewBottomSpace;
}

- (void)hideKeyboard:(UITextField *)txtFld
{
    [txtFld resignFirstResponder];
    [self keyboardDidHide];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self hideKeyboard:textField];
    return NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
