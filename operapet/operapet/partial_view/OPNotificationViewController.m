//
//  OPNotificationViewController.m
//  operapet
//
//  Created by Gary KK on 19/4/2017.
//  Copyright © 2017 Hotechie. All rights reserved.
//

#import "OPNotificationViewController.h"
#import "OPProfileService.h"
#import "OPNotificationCell.h"

@interface OPNotificationViewController ()

@end

@implementation OPNotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getTitle {
    return @"通知中心";
}

- (void)updateData:(BOOL)forceUpdate {
    OPProfile *profile = [SessionManager sharedInstance].currentProfile;
    [[[OPProfileService alloc]init]getNotification:[[HTListResponseHandler alloc]initWithOnResult:^(NSArray *array) {
        self.notifications = array;
        [self updateUI];
    } onFailure:^(NSInteger code, NSString *str) {
        
    }] member_id:profile.id];
}


- (void)configUI {
    [super configUI];
    [self showMenuButton];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateData:NO];
}


- (void)updateUI {
    [super updateUI];
    [self.factory loadData:self.notifications];
}

- (BaseListCellFactory *)createFactory {
    return [[OPNotificationFactory alloc]initWithCollectionView:self.listView];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end


@implementation OPNotificationFactory

- (NSArray *)cellNibs {
    return @[NSStringFromClass([OPNotificationCell class])];
}

-(void)loadData:(NSArray *)data {
    NSMutableArray *vms = [NSMutableArray array];
    
    for(OPNotification *noti in data) {
        [vms addObject:[CellModel modelWithData:nil renderer:^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
            OPNotificationCell *cell = [parent dequeueReusableCellWithReuseIdentifier:NSStringFromClass([OPNotificationCell class]) forIndexPath:indexPath];
            [cell setImageURL:[[OPServiceManager sharedInstance]getProfileImageURLForId:noti.image_id] description:noti._description];
            return cell;
        } formater:^CGSize(CellModel *model, CGFloat pWidth, CGFloat pHeight) {
            return CGSizeMake(pWidth, 70);
        } action:^(CellModel *model) {
            
        }]];
    }
    
    [self loadViewModels:vms];
}

@end
