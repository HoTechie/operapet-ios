//
//  OPPostListViewController.h
//  operapet
//
//  Created by Gary KK on 11/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListViewController.h"
#import "OPCategoryCellFactory.h"

@interface OPPostListViewController : BaseListViewController

@property (strong, nonatomic) IBOutlet UICollectionView *categoryList;
@property OPCategoryCellFactory *categoryFactory;

@end
