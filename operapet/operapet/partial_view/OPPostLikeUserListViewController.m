//
//  OPPostLikeUserListViewController.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPostLikeUserListViewController.h"

@interface OPPostLikeUserListViewController ()

@end

@implementation OPPostLikeUserListViewController

- (id)initWithPostId:(NSInteger)postId {
    self = [super init];
    self.postId = postId;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getTitle {
    return ls(@"title_post_like_user_list");
}

- (BaseListCellFactory *)createFactory {
    return [[OPFriendFactory alloc]initWithCollectionView:self.listView];
}

- (void)configUI {
    [super configUI];
    [self updateData:NO];
}

- (void)updateData:(BOOL)forceUpdate {
    [[[OPPostsService alloc]init]getWhoLikePost:[[HTListResponseHandler alloc]initWithOnResult:^(NSArray *array) {
        self.models = array;
        [self updateUI];
    } onFailure:^(NSInteger code, NSString *str) {
        
    }] post_id:self.postId];
}

- (void)updateUI {
    [self.factory loadData:self.models];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
