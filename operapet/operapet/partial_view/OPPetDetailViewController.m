//
//  OPPetDetailViewController.m
//  operapet
//
//  Created by Gary KK on 18/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPetDetailViewController.h"

@interface OPPetDetailViewController ()

@end

@implementation OPPetDetailViewController

- (id)initWithPetId:(NSInteger)petId {
    self = [super init];
    self.petId = petId;
    return self;
}

- (id)init {
    self = [super init];
    self.petId = PET_ID_NEW;
    self.pet = [[OPPet alloc]init];
    self.pet.member_id = [SessionManager sharedInstance].currentProfile.id;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getTitle {
    return ls(@"title_pet");
}

- (BaseListCellFactory *)createFactory {
    return [[OPPetDetailFactory alloc]initWithCollectionView:self.listView];
}

- (void)configUI {
    [super configUI];
    [self updateData:NO];
}

- (void)deletePet:(OPPet *)pet {
    [[[OPPetService alloc]init]postDeletePet:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
        [self popViewController];
    } onFailure:^(NSInteger code, NSString *str) {
        [self showMessageDialog:@"請重試" buttonPositive:@"OK" positiveHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {}];
    }] pet_id:pet.id];
}

- (void)updateUI {
    [super updateUI];
    [self.navigationItem setRightBarButtonItems:[self getRightBarButtonItems]];
    [(OPPetDetailFactory *)self.factory loadPet:self.pet fromVC:self onDelete:^(OPPet *pet) {
        [self showMessageDialog:@"刪除這寵物？" buttonPositive:@"確定" buttonNegative:@"取消" positiveHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {
            [self deletePet:pet];
        } negativeHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {
           
        }];
    }];
}

- (void)updateData:(BOOL)forceUpdate {
    if(self.petId == PET_ID_NEW) {
        [self updateUI];
        return;
    }
    
    [[[OPPetService alloc]init]getPetDetail:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
        self.pet = object;
        [self updateUI];
    } onFailure:^(NSInteger code, NSString *str) {
        
    }] pet_id:self.petId];
}

- (NSLayoutConstraint *)constraintBottomSpace {
    for(NSLayoutConstraint *constraint in self.view.constraints) {
        if((constraint.firstItem == self.listView && constraint.firstAttribute == NSLayoutAttributeBottom) ||
           (constraint.secondItem == self.listView && constraint.secondAttribute == NSLayoutAttributeBottom)) {
            return  constraint;
        }
    }
    
    return nil;
}

- (NSArray *)getRightBarButtonItems {
    if(self.pet.member_id != [SessionManager sharedInstance].currentProfile.id) return @[];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithTitle:@"儲存" style:UIBarButtonItemStylePlain target:self action:@selector(save)];
    item.tintColor = color_theme_color;
    return @[item];
}

-(void)save {
    NSInteger pet_id = self.petId;
    NSInteger gender = self.pet.gender;
    NSInteger member_id = [SessionManager sharedInstance].currentProfile.id;
    NSInteger pet_type = self.pet.pet_type;
    NSString *birth_date = self.pet.birth_date;
    NSString *description = self.pet._description;
    UIImage *pet_image = ((OPPetDetailFactory *)self.factory).petImage;
    NSString *medical_record = self.pet.medical_record;
    NSString *last_body_check_date = self.pet.last_body_check_date;
    NSString *next_body_check_date = self.pet.next_body_check_date;
    NSString *last_bath_hair_cut_description = self.pet.last_bath_hair_cut_description ? self.pet.last_bath_hair_cut_description : @"";
    NSString *bmi_record_weight_1 = [@(self.pet.bmi_record_weight_1) stringValue];
    NSString *bmi_record_length_1 = [@(self.pet.bmi_record_length_1) stringValue];
    NSString *bmi_record_weight_2 = [@(self.pet.bmi_record_weight_2) stringValue];
    NSString *bmi_record_length_2 = [@(self.pet.bmi_record_length_2) stringValue];
    NSString *bmi_record_weight_3 = [@(self.pet.bmi_record_weight_3) stringValue];
    NSString *bmi_record_length_3 = [@(self.pet.bmi_record_length_3) stringValue];
    NSString *bmi_record_date_1 = self.pet.bmi_record_date_1 ? self.pet.bmi_record_date_1 : @"";
    NSString *bmi_record_date_2 = self.pet.bmi_record_date_2 ? self.pet.bmi_record_date_2 : @"";
    NSString *bmi_record_date_3 = self.pet.bmi_record_date_3 ? self.pet.bmi_record_date_3 : @"";
    NSString *pin_card_json_string = [self.pet json_pin_cards];
    
    [self showProgressHUD];
    [[[OPPetService alloc]init]posetCreatePet:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
        [self hideProgressHUD];
        [self showMessageDialog:@"儲存成功" buttonPositive:ls(@"msg_ok") positiveHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {}];
    } onFailure:^(NSInteger code, NSString *str) {
        [self hideProgressHUD];
        [self showMessageDialog:@"發生錯誤" buttonPositive:ls(@"msg_ok") positiveHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {}];
    }] pet_id:pet_id gener:gender member_id:member_id pet_type:pet_type birth_date:birth_date description:description pet_image:pet_image medical_record:medical_record last_body_check_date:last_body_check_date next_body_check_date:next_body_check_date last_bath_hair_cut_description:last_bath_hair_cut_description bmi_record_weight1:bmi_record_weight_1 bmi_record_length1:bmi_record_length_1 bmi_record_weight2:bmi_record_weight_2 bmi_record_length2:bmi_record_length_2 bmi_record_weight3:bmi_record_weight_3 bmi_record_length3:bmi_record_length_3 bmi_record_date_1:bmi_record_date_1 bmi_record_date_2:bmi_record_date_2 bmi_record_date_3:bmi_record_date_3 pin_card_json_string:pin_card_json_string];
}

- (void)backButtonPressed {
    [super backButtonPressed];
    if(self.completion) self.completion();
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
