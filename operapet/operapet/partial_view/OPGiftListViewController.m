//
//  OPGiftListViewController.m
//  operapet
//
//  Created by Gary KK on 21/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPGiftListViewController.h"
#import "OPProfileService.h"

@interface OPGiftListViewController ()

@end

@implementation OPGiftListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getTitle {
    return ls(@"title_gift_list");
}

- (BaseListCellFactory *)createFactory {
    return [[OPGiftFactory alloc]initWithCollectionView:self.listView];
}

- (void)configUI {
    [super configUI];
    [self showMenuButton];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateData:NO];
}

- (void)updateData:(BOOL)forceUpdate {
    [[[OPProfileService alloc]init]getProfile:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
        OPProfile *updatedProfile = object;
        [SessionManager sharedInstance].currentProfile = updatedProfile;
        
        [[[OPGiftService alloc]init]getGifts:[[HTListResponseHandler alloc]initWithOnResult:^(NSArray *array) {
            self.gifts = array;
            [self updateUI];
        } onFailure:^(NSInteger code, NSString *str) {
            
        }]];
        
    } onFailure:^(NSInteger code, NSString *str) {
        [self hideProgressHUD];
    }] member_id:[SessionManager sharedInstance].currentProfile.id];
}

- (void)updateUI {
    [self.factory loadData:self.gifts];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
