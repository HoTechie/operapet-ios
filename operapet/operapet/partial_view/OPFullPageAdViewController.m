//
//  OPFullPageAdViewController.m
//  operapet
//
//  Created by Gary KK on 19/4/2017.
//  Copyright © 2017 Hotechie. All rights reserved.
//

#import "OPFullPageAdViewController.h"
#import "OPAdService.h"

@interface OPFullPageAdViewController ()

@end

@implementation OPFullPageAdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getNibName {
    return NSStringFromClass([OPFullPageAdViewController class]);
}

- (void)configUI {
    [super configUI];
    
    self.view.backgroundColor = [UIColor blackColor];
    [self.btnClose addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [self.btnAd addTarget:self action:@selector(onTapAd) forControlEvents:UIControlEventTouchUpInside];
    [self updateData:NO];
}

- (void)onTapAd {
    if(self.advert && self.advert.link && self.advert.link.length) {
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:self.advert.link]];
    }
}

- (void)close {
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

- (void)updateData:(BOOL)forceUpdate {
    [[[OPAdService alloc]init]getAdvListByType:[[HTListResponseHandler alloc]initWithOnResult:^(NSArray *array) {
        OPAdvert *ad = array.count ? array.firstObject : nil;
        if(ad) {
            self.advert = ad;
            [self.imgAdvert sd_setImageWithURL:[NSURL URLWithString:[[OPServiceManager sharedInstance]getAdvImageURLForId:ad.image_id]]];
        } else {
            [self close];
        }
    } onFailure:^(NSInteger code, NSString *str) {
        
    }] type_id:ADV_TYPE_POP_UP];
}

+ (id)showInViewController:(UIViewController *)vc {
    OPFullPageAdViewController *adVC = [[OPFullPageAdViewController alloc]init];
    
    [vc addChildViewController:adVC];
    [vc.view addSubview:adVC.view];
    [OPFullPageAdViewController constraintView:adVC.view inContainer:vc.view];
    
    return adVC;
}

+(void)constraintView:(UIView *)view inContainer:(UIView *)container {
    [view setTranslatesAutoresizingMaskIntoConstraints:NO];
    [container addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:container attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [container addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:container attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [container addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:container attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [container addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:container attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
