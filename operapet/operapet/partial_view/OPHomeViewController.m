//
//  OPHomeViewController.m
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPHomeViewController.h"

#import "OPWallViewController.h"
#import "OPPostListViewController.h"
#import "OPSearchTagViewController.h"
#import "OPFriendSearchViewController.h"
#import "OPCreatePostViewController.h"
#import "OPMainViewController.h"
#import "OPFullPageAdViewController.h"


@interface OPHomeViewController ()

@property OPPostListViewController *postListVC;
@property OPFriendSearchViewController *friendSearchVC;
@property OPSearchTagViewController *searchTagVC;
@property OPWallViewController *wallVC;

@end

@implementation OPHomeViewController

- (id)initWithInitialTab:(OPHomeTab)initialTab {
    self = [self init];
    self.initialTab = initialTab;
    return self;
}

- (id)init {
    self = [super initWithViewControllers:[self viewControllers]];
    self.initialTab = OPHomeTabNewsFeed;
    return self;
}

- (NSArray *)viewControllers {
    self.postListVC = [[OPPostListViewController alloc]init];
    self.friendSearchVC = [[OPFriendSearchViewController alloc]init];
    self.searchTagVC = [[OPSearchTagViewController alloc]init];
    self.wallVC = [[OPWallViewController alloc]init];
    
    return @[self.postListVC, self.friendSearchVC, self.searchTagVC, self.wallVC];
}

- (void)buttonNewPostPressed {
    __weak OPHomeViewController *_self = self;
    [[OPNavigationController sharedInstance]pushViewController:[[OPCreatePostViewController alloc]initWithCompletion:^{
        [_self.postListVC updateData:YES];
    }] animated:YES];
}

- (void)configUI {
    [super configUI];
    [self showMenuButton];
    
    self.currentTab = (int)self.initialTab;
    
    if(![SessionManager sharedInstance].adShown) {
        [OPFullPageAdViewController showInViewController:[OPMainViewController sharedInstance]];
        [SessionManager sharedInstance].adShown = YES;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
