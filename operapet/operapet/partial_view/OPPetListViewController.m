//
//  OPPetListViewController.m
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPetListViewController.h"

@interface OPPetListViewController ()

@end

@implementation OPPetListViewController

- (id)initWithProfileId:(NSInteger)profileId {
    self = [super init];
    self.profileId = profileId;
    return self;
}

- (id)init {
    self = [super init];
    self.profileId = MY_RPOFILE_ID;
    return self;
}

- (NSString *)getTitle {
    return ls(@"title_pet_list");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateData:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BaseListCellFactory *)createFactory {
    __weak OPPetListViewController *_self = self;
    OPPetFactory *factory = [[OPPetFactory alloc]initWithCollectionView:self.listView];
    factory.completion = ^void(){
        [_self updateData:YES];
    };
    return factory;
}

- (void)configUI {
    [super configUI];
    
    if(self.profileId == MY_RPOFILE_ID) {
        [self showMenuButton];
    }
    
    [self updateData:NO];
}

- (void)updateData:(BOOL)forceUpdate {
    NSInteger profileId = self.profileId == MY_RPOFILE_ID ? [SessionManager sharedInstance].currentProfile.id : self.profileId;
    
    
    [[[OPProfileService alloc]init]getAllPet:[[HTListResponseHandler alloc]initWithOnResult:^(NSArray *array) {
        self.models = array;
        [self updateUI];
    } onFailure:^(NSInteger code, NSString *str) {
        
    }] member_id:profileId];
}

- (void)updateUI {
    [super updateUI];
    [self.factory loadData:self.models];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
