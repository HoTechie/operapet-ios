//
//  OPLoginOptionViewController.m
//  operapet
//
//  Created by Gary KK on 6/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPLoginOptionViewController.h"
#import "OPLoginViewController.h"
#import "OPHomeViewController.h"

#import "SessionManager.h"
#import "OPMemberService.h"
#import "OPRegistrationViewController.h"


@interface OPLoginOptionViewController ()

@end

#define DEBUG_SKIP_LOGIN      NO

@implementation OPLoginOptionViewController

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getNibName {
    return NSStringFromClass([self class]);
}

- (void)configUI {
    [super configUI];
    [self.btnLoginMobile addTarget:self action:@selector(onClickBtnLoginMobile) forControlEvents:UIControlEventTouchUpInside];
    [self.btnRegister setTitle:ls(@"login_member_register") forState:UIControlStateNormal];
    [self.btnRegister addTarget:self action:@selector(onClickBtnRegister) forControlEvents:UIControlEventTouchUpInside];
    [self.btnLoginFacebook addTarget:self action:@selector(onClickBtnLoginFacebook) forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(DEBUG_SKIP_LOGIN || [SessionManager sharedInstance].currentProfile) {
        [[OPNavigationController sharedInstance]pushViewController:[[OPHomeViewController alloc]init] animated:NO];
    }
}

-(void)onClickBtnLoginFacebook {
    [[SessionManager sharedInstance]loginWithFacebookWithSuccess:^(NSString *fb_id, NSString* token) {
        
        [[[OPMemberService alloc]init]postIsFbExist:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
            [[OPNavigationController sharedInstance]pushViewController:[OPLoginViewController loginVerifySMSViewControllerWithFacebookId:fb_id]  animated:YES];
        } onFailure:^(NSInteger code, NSString *str) {
            if(code == 404) {
                NSLog(@"[FbLogin] failed with 404");
                [[OPNavigationController sharedInstance]pushViewController:[OPLoginViewController registrationViewControllerWithFacebookId:fb_id token:token]  animated:YES];
            } else {
                NSLog(@"[FbLogin] failed with code %@",[@(code)stringValue]);
            }
        }] facebook_id:fb_id];
        
        
    } error:^(NSString *res) {
       
    }];
}

-(void)onClickBtnLoginMobile {
    [[OPNavigationController sharedInstance]pushViewController:[OPLoginViewController loginPhoneVerifyViewController]  animated:YES];
}

-(void)onClickBtnRegister {
    [[OPNavigationController sharedInstance]pushViewController:[OPLoginViewController registrationViewController]  animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
