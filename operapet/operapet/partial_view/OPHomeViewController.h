//
//  OPHomeViewController.h
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPTabBarViewController.h"

typedef enum {
    OPHomeTabNewsFeed,
    OPHomeTabFriends,
    OPHomeTabTags,
    OPHomeTabMyWall
} OPHomeTab;
@interface OPHomeViewController : OPTabBarViewController

-(id)initWithInitialTab:(OPHomeTab)initialTab;

@property OPHomeTab initialTab;



@end
