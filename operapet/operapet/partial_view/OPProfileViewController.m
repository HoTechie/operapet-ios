//
//  OPProfileViewController.m
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPProfileViewController.h"
#import "OPActionHistoryListViewController.h"
#import "OPGiftRecordListViewController.h"
#import "OPProfileService.h"

@interface OPProfileViewController ()

@end

@implementation OPProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getNibName {
    return NSStringFromClass([OPProfileViewController class]);
}

- (NSString *)getTitle {
    return ls(@"title_profile");
}

- (BaseListCellFactory *)createFactory {
    return [[OPProfileFactory alloc]initWithCollectionView:self.listView];
}

- (void)configUI {
    [super configUI];
    
    [self.btnRedeemRecords addTarget:self action:@selector(showRedeemRecords) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPointsRecords addTarget:self action:@selector(showPointsRecords) forControlEvents:UIControlEventTouchUpInside];
    [self showMenuButton];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateData:NO];
}

- (void)updateData:(BOOL)forceUpdate {
    [[[OPProfileService alloc]init]getProfile:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
        [SessionManager sharedInstance].currentProfile = object;
        self.profile = object;
        [self updateUI];
    } onFailure:^(NSInteger code, NSString *str) {
        
    }]];
}

- (void)updateUI {
    OPProfileFactory *factory = (id)self.factory;
    if(self.profile) [factory loadProfile:self.profile fromVC:self];
}

- (NSArray *)getRightBarButtonItems {
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithTitle:@"儲存" style:UIBarButtonItemStylePlain target:self action:@selector(save)];
    item.tintColor = color_theme_color;
    return @[item];
}

-(void)save {
    OPProfileFactory *factory = (OPProfileFactory *)self.factory;
    OPProfile *profile = factory.profile;
    
    UIImage *profile_image = factory.profileImage;
    NSString *device_id = @"ID";
    NSString *device_type = @"1";
    NSString *mobile_no = profile.mobile;
    NSString *lastname = profile.lastname;
    NSString *firstname = profile.firstname;
    NSString *email = profile.email;
    
    [self showProgressHUD];
    [[[OPMemberService alloc]init]postUpdateMember:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
        [self hideProgressHUD];
        OPProfile *updatedProfile = object;
        [SessionManager sharedInstance].currentProfile = updatedProfile;
    } onFailure:^(NSInteger code, NSString *str) {
        [self hideProgressHUD];
        [self showMessageDialog:@"發生錯誤" buttonPositive:ls(@"msg_ok") positiveHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {
            
        }];
    }] profile_image:profile_image device_id:device_id device_type:device_type mobile_no:mobile_no lastname:lastname firstname:firstname email:email];
}

-(void)showRedeemRecords {
    [[OPNavigationController sharedInstance]pushViewController:[[OPGiftRecordListViewController alloc]init] animated:YES];
}

-(void)showPointsRecords {
    [[OPNavigationController sharedInstance]pushViewController:[[OPActionHistoryListViewController alloc]init] animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
