//
//  OPPostByTagListViewController.m
//  operapet
//
//  Created by Gary KK on 14/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPostByTagListViewController.h"
#import "OPPostsService.h"

@interface OPPostByTagListViewController ()

@end

@implementation OPPostByTagListViewController

-(id)initWithOPTag:(OPTag *)optag {
    self = [super init];
    self.optag = optag;
    return self;
}

- (BaseListCellFactory *)createFactory {
    return [[OPPostListCellFactory alloc]initWithCollectionView:self.listView];
}

- (NSString *)getTitle {
    return [NSString stringWithFormat:@"#%@",self.optag._description];
}

-(void)configUI {
    [super configUI];
    [self updateData:NO];
}

-(void)updateData:(BOOL)forceUpdate {
    [[[OPPostsService alloc]init]getPostByTagId:[[HTListResponseHandler alloc]initWithOnResult:^(NSArray *array) {
        self.models = array;
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            [self updateUI];
        }];
    } onFailure:^(NSInteger code, NSString *str) {
        
    }] post_tag_id:self.optag.id];
}

- (void)updateUI {
    [super updateUI];
    [self.factory loadData:self.models];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
