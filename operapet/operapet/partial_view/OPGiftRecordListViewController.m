//
//  OPGiftRecordListViewController.m
//  operapet
//
//  Created by Gary KK on 21/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPGiftRecordListViewController.h"

@interface OPGiftRecordListViewController ()

@end

@implementation OPGiftRecordListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getTitle {
    return ls(@"profile_gift_record");
}

- (BaseListCellFactory *)createFactory {
    OPGiftRecordFactory *factory = [[OPGiftRecordFactory alloc]initWithCollectionView:self.listView];
    factory.onCollectGift = ^(OPGift *gift){
        [self collectGift:gift];
    };
    
    return factory;
}

- (void)collectGift:(OPGift *)gift {
    [self showMessageDialog:ls(@"gift_collect_q") buttonPositive:ls(@"msg_yes") buttonNegative:ls(@"msg_no") positiveHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {
        [[[OPGiftService alloc]init]postCollectGift:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
            [self showMessageDialog:ls(@"gift_collect_success") buttonPositive:ls(@"msg_ok") positiveHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {}];
            [self updateData:YES];
        } onFailure:^(NSInteger code, NSString *str) {
            
        }] redemption_id:gift.id];
    } negativeHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {
        
    }];
}

- (void)configUI {
    [super configUI];
    [self updateData:NO];
}

- (void)updateData:(BOOL)forceUpdate {
    [[[OPProfileService alloc]init]getGiftHistory:[[HTListResponseHandler alloc]initWithOnResult:^(NSArray *array) {
        self.models = array;
        [self updateUI];
    } onFailure:^(NSInteger code, NSString *str) {
        
    }]];
}

- (void)updateUI {
    [self.factory loadData:self.models];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
