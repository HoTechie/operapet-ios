//
//  OPSearchTagViewController.h
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListViewController.h"
#import "OPCircleButton.h"

@interface OPSearchTagViewController : BaseListViewController

@property (strong, nonatomic) IBOutlet OPCircleButton *btnClose;
@property (strong, nonatomic) IBOutlet UITextField *txtSearch;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionSearchResults;
@property (strong, nonatomic) IBOutlet UIImageView *imgNoResult;

@property NSString *searchKey;

@end
