//
//  OPTermsViewController.m
//  operapet
//
//  Created by Gary KK on 23/3/2017.
//  Copyright © 2017 Hotechie. All rights reserved.
//

#import "OPTermsViewController.h"

@interface OPTermsViewController ()

@end

@implementation OPTermsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *) getNibName {
    return NSStringFromClass([OPTermsViewController class]);
}

- (NSString *)getTitle {
    return @"使用條款";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
