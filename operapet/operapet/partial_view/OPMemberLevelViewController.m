//
//  OPMemberLevelViewController.m
//  operapet
//
//  Created by Gary KK on 18/4/2017.
//  Copyright © 2017 Hotechie. All rights reserved.
//

#import "OPMemberLevelViewController.h"

#define SCORE_PER_LEVEL 10000

@interface OPMemberLevelViewController ()

@end

@implementation OPMemberLevelViewController

- (id)initWithProfile:(OPProfile *)profile {
    self = [super init];
    self.profile = profile;
    return self;
}

- (NSString*)getNibName {
    return NSStringFromClass([OPMemberLevelViewController class]);
}

- (void)updateUI {
    [super updateUI];
    
    self.lblLevel.text = self.profile.memberLevelString;
    self.lblScore.text = [@(self.profile.remain_credit) stringValue];
    self.chart.progress = ((float)(self.profile.credit_total)) / SCORE_PER_LEVEL;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateUI];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getTitle {
    return @"會員級別";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
