//
//  OPLoginOptionViewController.h
//  operapet
//
//  Created by Gary KK on 6/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPLoginContentViewController.h"

@interface OPLoginOptionViewController : OPLoginContentViewController

@property (weak, nonatomic) IBOutlet UIButton *btnLoginFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnLoginMobile;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;


@end
