//
//  OPPetPinCardViewController.m
//  operapet
//
//  Created by Gary KK on 21/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPetPinCardViewController.h"

@interface OPPetPinCardViewController ()<UITextFieldDelegate>

@end

@implementation OPPetPinCardViewController

-(id)initWithCallback:(void (^)(OPPinCard *))callback {
    self = [super init];
    self.onSavePincard = callback;
    self.pincard = [[OPPinCard alloc]init];
    return self;
}

-(id)initWithPincard:(id)pincard callback:(void (^)(OPPinCard *))callback {
    self = [super init];
    self.onSavePincard = callback;
    self.pincard = pincard;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getNibName {
    return NSStringFromClass([OPPetPinCardViewController class]);
}

- (void)configUI {
    [super configUI];
    
    self.scrollContentView = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([OPPetPinCardViewController class]) owner:self options:0][1];
    [self.scrollView addSubview:self.scrollContentView];
    [self constrainView:self.scrollContentView inScrollView:self.scrollView];
    [self.view layoutIfNeeded];
    
    [self.btnSubmit addTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchUpInside];
    [self configureTextfield:self.txtName numberOnly:NO];
    [self configureTextfield:self.txtLastPinYear numberOnly:YES];
    [self configureTextfield:self.txtLastPinMonth numberOnly:YES];
    [self configureTextfield:self.txtLastPinDay numberOnly:YES];
    [self configureTextfield:self.txtNextPinYear numberOnly:YES];
    [self configureTextfield:self.txtNextPinMonth numberOnly:YES];
    [self configureTextfield:self.txtNextPinDay numberOnly:YES];
    
    [self updateUI];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *target = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    NSInteger charLimit = INT_MAX;
    if(textField == self.txtLastPinYear || textField == self.txtNextPinYear) {
        charLimit = 4;
    } else if(textField == self.txtLastPinDay || textField == self.txtLastPinMonth || textField == self.txtNextPinMonth || textField == self.txtNextPinDay) {
        charLimit = 2;
    }
    
    return target.length <= charLimit;
}

- (void)configureTextfield:(UITextField *)txtField numberOnly:(BOOL)numberOnly {
    [txtField setDelegate:self];
    [txtField setReturnKeyType:UIReturnKeyDone];
    if(numberOnly) [txtField setKeyboardType:UIKeyboardTypeNumberPad];
}

- (void)updateUI {
    self.txtName.text = self.pincard.name;
    self.txtFldRemarks.text = self.pincard._description;
    
    NSDateComponents *comps;
    if(self.pincard.recent_pin_record_date.length) {
        comps = [self.pincard dateCompsFromString:self.pincard.recent_pin_record_date];
        self.txtLastPinYear.text = [@(comps.year) stringValue];
        self.txtLastPinMonth.text = [@(comps.month) stringValue];
        self.txtLastPinDay.text = [@(comps.day) stringValue];
    }
    
    if(self.pincard.next_pin_record_date.length) {
        comps = [self.pincard dateCompsFromString:self.pincard.next_pin_record_date];
        self.txtNextPinYear.text = [@(comps.year) stringValue];
        self.txtNextPinMonth.text = [@(comps.month) stringValue];
        self.txtNextPinDay.text = [@(comps.day) stringValue];
    }
}

- (void)saveToPincard {
    self.pincard.name = self.txtName.text;
    self.pincard._description = self.txtFldRemarks.text;
    
    if(self.txtLastPinYear.text.length && self.txtLastPinMonth.text.length && self.txtLastPinDay.text.length) {
        NSDateComponents *comps = [[NSDateComponents alloc]init];
        [comps setYear:self.txtLastPinYear.text.integerValue];
        [comps setMonth:self.txtLastPinMonth.text.integerValue];
        [comps setDay:self.txtLastPinDay.text.integerValue];
        self.pincard.recent_pin_record_date = [self.pincard stringFromDateComps:comps];
    } else {
        self.pincard.recent_pin_record_date = [self.pincard stringFromDate:[NSDate date]];
    }
    
    if(self.txtNextPinYear.text.length && self.txtNextPinMonth.text.length && self.txtNextPinDay.text.length) {
        NSDateComponents *comps = [[NSDateComponents alloc]init];
        [comps setYear:self.txtNextPinYear.text.integerValue];
        [comps setMonth:self.txtNextPinMonth.text.integerValue];
        [comps setDay:self.txtNextPinDay.text.integerValue];
        self.pincard.next_pin_record_date = [self.pincard stringFromDateComps:comps];
    } else {
        self.pincard.next_pin_record_date = [self.pincard stringFromDate:[NSDate date]];
    }
    
    if(self.onSavePincard) self.onSavePincard(self.pincard);
}

- (void)submit {
    if(!self.txtName.text.length) {
        [self showMessageDialog:@"請確認輸入的資料正確" buttonPositive:ls(@"msg_ok")  positiveHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {}];
        return;
    }
    
    [self saveToPincard];
    [[OPNavigationController sharedInstance]popViewControllerAnimated:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self hideKeyboard:textField];
    return NO;
}

-(NSLayoutConstraint *)constraintBottomSpace {
    return _constraintBottomSpace;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
