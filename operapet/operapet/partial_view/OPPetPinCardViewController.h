//
//  OPPetPinCardViewController.h
//  operapet
//
//  Created by Gary KK on 21/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPViewController.h"
#import "OPPinCard.h"


@interface OPPetPinCardViewController : OPViewController

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;

@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UITextField *txtLastPinYear;
@property (strong, nonatomic) IBOutlet UITextField *txtLastPinMonth;
@property (strong, nonatomic) IBOutlet UITextField *txtLastPinDay;
@property (strong, nonatomic) IBOutlet UITextField *txtNextPinYear;
@property (strong, nonatomic) IBOutlet UITextField *txtNextPinMonth;
@property (strong, nonatomic) IBOutlet UITextField *txtNextPinDay;
@property (strong, nonatomic) IBOutlet UITextView *txtFldRemarks;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintBottomSpace;

@property UIView *scrollContentView;
@property BOOL isKeyboardVisible;

@property OPPinCard *pincard;
@property void(^onSavePincard)(OPPinCard * pincard);

-(id)initWithPincard:pincard callback:(void(^)(OPPinCard *))callback;
-(id)initWithCallback:(void(^)(OPPinCard *))callback;

@end
