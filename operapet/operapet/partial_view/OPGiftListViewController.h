//
//  OPGiftListViewController.h
//  operapet
//
//  Created by Gary KK on 21/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListViewController.h"
#import "OPGiftFactory.h"
#import "OPGiftService.h"

@interface OPGiftListViewController : BaseListViewController

@property NSArray *gifts;

@end
