//
//  OPPostDetailViewController.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListViewController.h"
#import "OPPostDetailFactory.h"
#import "OPPostsService.h"

@interface OPPostDetailViewController : BaseListViewController

@property (strong, nonatomic) IBOutlet UITextView *txtFldComment;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong, nonatomic) IBOutlet UILabel *lblPlaceholder;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintBottomSpace;

-(id)initWithPostId:(NSInteger)postId;

@property NSInteger postId;
@property OPPost *post;

@end
