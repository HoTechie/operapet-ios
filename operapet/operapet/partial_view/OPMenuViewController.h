//
//  OPMenuViewController.h
//  operapet
//
//  Created by Gary KK on 6/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListViewController.h"

@interface OPMenuViewController : BaseListViewController

@property (strong, nonatomic) IBOutlet UIButton *btnLogout;

-(void)reload;

@end
