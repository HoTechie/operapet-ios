//
//  OPFriendSearchViewController.h
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListViewController.h"
#import "OPMemberService.h"
#import "OPCircleButton.h"
#import "OPFriendFactory.h"

@interface OPFriendSearchViewController : BaseListViewController

@property (strong, nonatomic) IBOutlet OPCircleButton *btnClose;
@property (strong, nonatomic) IBOutlet UITextField *txtSearch;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionSearchResults;

@property NSString *searchKey;

@property NSArray *searchResults;
@property NSArray *friends;
@property NSArray *requests;


@end
