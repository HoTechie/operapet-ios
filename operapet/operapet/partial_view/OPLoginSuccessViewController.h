//
//  OPLoginSuccessViewController.h
//  operapet
//
//  Created by Gary KK on 11/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPLoginContentViewController.h"
#import "HTRoundedButton.h"

@interface OPLoginSuccessViewController : OPLoginContentViewController

@property (strong, nonatomic) IBOutlet UILabel *lblMessage;
@property (strong, nonatomic) IBOutlet HTRoundedButton *btnConfirm;


@end
