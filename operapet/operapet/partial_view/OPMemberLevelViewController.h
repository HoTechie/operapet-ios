//
//  OPMemberLevelViewController.h
//  operapet
//
//  Created by Gary KK on 18/4/2017.
//  Copyright © 2017 Hotechie. All rights reserved.
//

#import "OPViewController.h"
#import "OPProfile.h"

#import "OPScoreChartView.h"

@interface OPMemberLevelViewController : OPViewController

- (id)initWithProfile:(OPProfile *)profile;

@property OPProfile *profile;
@property (strong, nonatomic) IBOutlet OPScoreChartView *chart;
@property (strong, nonatomic) IBOutlet UILabel *lblLevel;
@property (strong, nonatomic) IBOutlet UILabel *lblScore;

@end
