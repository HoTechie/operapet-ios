//
//  OPGiftDetailViewController.h
//  operapet
//
//  Created by Gary KK on 21/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPViewController.h"
#import "iCarousel.h"
#import "OPGift.h"
#import "OPGiftService.h"

@interface OPGiftDetailViewController : OPViewController

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet iCarousel *carousel;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lbldescription;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UILabel *lblPoints;
@property (strong, nonatomic) IBOutlet UIButton *btnRedeem;
@property (strong, nonatomic) IBOutlet UIView *viewCircle;

@property NSInteger giftId;
@property OPGift *gift;

@property UIView * scrollContentView;

- (id)initWithGiftId:(NSInteger)giftId;

@end
