//
//  OPSearchTagViewController.m
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPSearchTagViewController.h"
#import "OPPostsService.h"
#import "OPSearchTagFactory.h"

@interface OPSearchTagViewController ()<UITextFieldDelegate>

@end

@implementation OPSearchTagViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getNibName {
    return NSStringFromClass([OPSearchTagViewController class]);
}

- (NSString *)getTitle {
    return ls(@"title_search_tag");
}

- (UICollectionView *)listView {
    return self.collectionSearchResults;
}

- (BaseListCellFactory *)createFactory {
    return [[OPSearchTagFactory alloc]initWithCollectionView:self.listView];
}

- (void)configUI {
    [super configUI];
    
    [self.btnClose addTarget:self action:@selector(emptySearchBox) forControlEvents:UIControlEventTouchUpInside];
    [self.txtSearch setDelegate:self];
    [self.txtSearch setReturnKeyType:UIReturnKeyDone];
}


- (void)emptySearchBox {
    self.txtSearch.text = @"";
}

- (void)updateData:(BOOL)forceUpdate {
    [[[OPPostsService alloc]init]postGetSearchTagResult:[[HTListResponseHandler alloc]initWithOnResult:^(NSArray *array) {
        self.models = array;
        [self updateUI];
    } onFailure:^(NSInteger code, NSString *str) {
        
    }] search_key:self.searchKey];
}

- (void)updateUI {
    self.imgNoResult.hidden = self.models.count;
    [self.factory loadData:self.models];
    [super updateUI];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    self.searchKey = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self updateData:NO];
    return YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.txtSearch resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
