//
//  OPInviteFriendViewController.m
//  operapet
//
//  Created by Gary KK on 26/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPInviteFriendViewController.h"

@interface OPInviteFriendViewController ()

@end

@implementation OPInviteFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BaseListCellFactory *)createFactory {
    OPInviteFriendFactory *factory = [[OPInviteFriendFactory alloc]initWithCollectionView:self.listView];
    factory.onSubmission = ^(NSArray *resuts){
        NSString *csv = @"";
        for(NSString *number in resuts) {
            if(csv.length && number.length) csv = [csv stringByAppendingString:@","];
            csv = [csv stringByAppendingString:number];
        }
        
        if(csv.length) {
            [self showProgressHUD];
            
            [[[OPMemberService alloc]init]postReferalFriendSendSms:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
                [self hideProgressHUD];
                [self showMessageDialog:ls(@"invite_friend_success") buttonPositive:nil positiveHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {}];
            } onFailure:^(NSInteger code, NSString *str) {
                [self hideProgressHUD];
            }] phone_list_csv:csv];
         }
    };
    return factory;
}

- (NSString *)getTitle {
    return ls(@"title_invite_friend_page");
}

- (void)configUI {
    [super configUI];
    
    [self showMenuButton];
    [self.factory loadData:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
