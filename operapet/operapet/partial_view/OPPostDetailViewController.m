//
//  OPPostDetailViewController.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPostDetailViewController.h"
#import "MBProgressHUD.h"

@interface OPPostDetailViewController ()<UITextViewDelegate>

@end

@implementation OPPostDetailViewController

- (id)initWithPostId:(NSInteger)postId {
    self = [super init];
    self.postId = postId;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (void)configUI {
    [super configUI];
    [self updateData:NO];
    
    self.txtFldComment.delegate = self;
    [self.btnSubmit addTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchUpInside];
}

- (void)textViewDidChange:(UITextView *)textView {
    self.lblPlaceholder.hidden = textView.text.length;
}

- (void)submit {
    if(!self.txtFldComment.text.length) return;
    self.btnSubmit.enabled = NO;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[[OPPostsService alloc]init]postCreateComment:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
        [self updateData:YES];
        self.btnSubmit.enabled = YES;
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } onFailure:^(NSInteger code, NSString *str) {
        self.btnSubmit.enabled = YES;
    }] description:self.txtFldComment.text post_id:self.postId];
}

- (NSString *)nibName {
    return NSStringFromClass([OPPostDetailViewController class]);
}

- (BaseListCellFactory *)createFactory {
    OPPostDetailFactory *factory = [[OPPostDetailFactory alloc]initWithCollectionView:self.listView];
    factory.onLike = self.onLike;
    factory.onShare = self.onShare;
    factory.onLikeComment = self.onLikeComment;
    return factory;
}

- (void)updateData:(BOOL)forceUpdate {
    [[[OPPostsService alloc]init]getPostDetail:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
        self.post = object;
        [self updateUI];
    } onFailure:^(NSInteger code, NSString *str) {
        
    }] post_id:self.postId];
}

- (void)updateUI {
    [super updateUI];
    
    [((OPPostDetailFactory *)self.factory)loadPost:self.post];
    
    if(self.txtFldComment.text.length) {
        self.txtFldComment.text = @"";
        [self.listView scrollRectToVisible:CGRectMake(0,self.listView.contentSize.height - 1,1,1) animated:YES];
    }
    
    [self.view endEditing:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
