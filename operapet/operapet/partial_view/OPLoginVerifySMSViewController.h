//
//  OPLoginVerifySMSViewController.h
//  operapet
//
//  Created by Gary KK on 11/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPLoginContentViewController.h"
#import "HTTextField.h"
#import "HTRoundedButton.h"

@interface OPLoginVerifySMSViewController : OPLoginContentViewController

-(id)initWithPhoneNumber:(NSString *)phoneNumber;
-(id)initWithFacebookId:(NSString *)facebookId;

@property NSString *phoneNumber;
@property NSString *facebookId;

@property (strong, nonatomic) IBOutlet HTTextField *txtSMS;
@property (strong, nonatomic) IBOutlet HTRoundedButton *btnConfirm;

@end
