//
//  OPPetListViewController.h
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListViewController.h"

#import "OPPetFactory.h"
#import "OPProfileService.h"



@interface OPPetListViewController : BaseListViewController

-(id)initWithProfileId:(NSInteger)profileId;

@property NSInteger profileId;
@property OPProfile *profile;


@end
