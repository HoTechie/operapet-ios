//
//  OPActionHistoryListViewController.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPActionHistoryListViewController.h"

@interface OPActionHistoryListViewController ()

@end

@implementation OPActionHistoryListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BaseListCellFactory *)createFactory {
    return [[OPActionHistoryFactory alloc]initWithCollectionView:self.listView];
}

- (void)configUI {
    [super configUI];
    [self updateData:NO];
}

-(NSString *)getTitle {
    return ls(@"title_action_history");
}

-(void)updateData:(BOOL)forceUpdate {
    [[[OPMemberService alloc]init]getActionHistory:[[HTListResponseHandler alloc]initWithOnResult:^(NSArray *array) {
        self.models = array;
        [self updateUI];
    } onFailure:^(NSInteger code, NSString *str) {
        
    }]];
}

-(void)updateUI {
    [self.factory loadData:self.models];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
