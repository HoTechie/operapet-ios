//
//  OPActionHistoryListViewController.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListViewController.h"
#import "OPMemberService.h"
#import "OPActionHistoryFactory.h"

@interface OPActionHistoryListViewController : BaseListViewController

@end
