//
//  OPProfileViewController.h
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListViewController.h"
#import "OPProfileFactory.h"
#import "OPMemberService.h"

@interface OPProfileViewController : BaseListViewController

@property (strong, nonatomic) IBOutlet UIButton *btnRedeemRecords;
@property (strong, nonatomic) IBOutlet UIButton *btnPointsRecords;
@property UIImage *profileImage;
@property OPProfile *profile;

@end
