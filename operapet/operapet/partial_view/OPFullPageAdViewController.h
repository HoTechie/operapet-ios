//
//  OPFullPageAdViewController.h
//  operapet
//
//  Created by Gary KK on 19/4/2017.
//  Copyright © 2017 Hotechie. All rights reserved.
//

#import "OPViewController.h"

@class OPAdvert;

@interface OPFullPageAdViewController : OPViewController

@property (strong, nonatomic) IBOutlet UIImageView *imgAdvert;
@property (strong, nonatomic) IBOutlet UIButton *btnClose;
@property (strong, nonatomic) IBOutlet UIButton *btnAd;
@property OPAdvert *advert;

+(id)showInViewController:(UIViewController *)vc;

@end
