//
//  OPFriendSearchViewController.m
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPFriendSearchViewController.h"

@interface OPFriendSearchViewController ()<UITextFieldDelegate>

@end

@implementation OPFriendSearchViewController

- (id)init {
    self = [super init];
    self.searchKey = @"";
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getNibName {
    return NSStringFromClass([self class]);
}

- (NSString *)getTitle {
    return ls(@"title_search_friend");
}

- (UICollectionView *)listView {
    return self.collectionSearchResults;
}

- (BaseListCellFactory *)createFactory {
    OPFriendFactory *factory = [[OPFriendFactory alloc]initWithCollectionView:self.listView];
    factory.onAddFriend = ^(OPProfile *profile){
        [self showProgressHUD];
        [[[OPMemberService alloc]init]postRequestFriend:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
            [self hideProgressHUD];
            [self updateData:NO];
        } onFailure:^(NSInteger code, NSString *str) {
            [self hideProgressHUD];
        }] target_member_id:profile.id];
    };
    factory.mode = OPFriendFactoryModeMyFriendList;
    return factory;
}

- (void)configUI {
    [super configUI];
    
    self.txtSearch.delegate = self;
    [self.btnClose addTarget:self action:@selector(emptyTxtSearch) forControlEvents:UIControlEventTouchUpInside];
    [self.txtSearch addTarget:self action:@selector(onTxtSearchEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    [self.txtSearch setReturnKeyType:UIReturnKeyDone];
}

- (void)emptyTxtSearch {
    self.txtSearch.text = @"";
    self.searchKey = @"";
    [self updateUI];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self hideKeyboard:textField];
    return NO;
}

- (void)onTxtSearchEditingChanged:(UITextField *)txtField {
    self.searchKey = txtField.text;
    [self updateData:NO];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateData:NO];
}

- (void)updateData:(BOOL)forceUpdate {
    if(self.searchKey.length) {
        [[[OPMemberService alloc]init]postGetSearchFriendResult:[[HTListResponseHandler alloc]initWithOnResult:^(NSArray *array) {
            self.searchResults = array;
            [self updateUI];
        } onFailure:^(NSInteger code, NSString *str) {
            
        }] search_key:self.searchKey];
    } else {
        [[[OPMemberService alloc]init]getFriendList:[[HTListResponseHandler alloc]initWithOnResult:^(NSArray *array) {
            self.friends = array;
            
            [[[OPMemberService alloc]init]getFriendRequestList:[[HTListResponseHandler alloc]initWithOnResult:^(NSArray *array) {
                self.requests = array;
                [self updateUI];
            } onFailure:^(NSInteger code, NSString *str) {
                
            }]];
            
            
        } onFailure:^(NSInteger code, NSString *str) {
            
        }] target_member_id:[SessionManager sharedInstance].currentProfile.id];
    }
}

- (void)updateUI {
    @synchronized (self) {
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            OPFriendFactory *factory = (OPFriendFactory *)self.factory;
            factory.mode = self.searchKey.length ? OPFriendFactoryModeSearchResultList : OPFriendFactoryModeMyFriendList;
            
            if(factory.mode == OPFriendFactoryModeMyFriendList) {
                [factory loadProfiles:self.friends requests:self.requests];
            } else if(factory.mode == OPFriendFactoryModeSearchResultList) {
                [factory loadProfiles:self.searchResults];
            }
        }];
        
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
