//
//  OPPostByTagListViewController.h
//  operapet
//
//  Created by Gary KK on 14/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListViewController.h"

#import "OPPostListCellFactory.h"
#import "OPTag.h"

@interface OPPostByTagListViewController : BaseListViewController

-(id)initWithOPTag:(OPTag *)optag;

@property OPTag *optag;

@end
