//
//  OPWallViewController.m
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPWallViewController.h"
#import "OPMemberService.h"

@interface OPWallViewController ()

@end

@implementation OPWallViewController

-(id)initWithProfileId:(NSInteger)profileId {
    self = [super init];
    self.profileId = profileId;
    return self;
}

- (id)init {
    self = [super init];
    self.profileId = [SessionManager sharedInstance].currentProfile.id;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getTitle {
    return ls(@"title_my_wall");
}

- (BaseListCellFactory *)createFactory {
    OPWallFactory *factory = [[OPWallFactory alloc]initWithCollectionView:self.listView];
    
    factory.onShare = self.onShare;
    factory.onLike = self.onLike;
    factory.onComment = self.onComment;
    
    factory.onAddFriend = ^(){
        [self showProgressHUD];
        [[[OPMemberService alloc]init]postRequestFriend:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
            [self hideProgressHUD];
            [self updateData:NO];
        } onFailure:^(NSInteger code, NSString *str) {
            [self hideProgressHUD];
        }] target_member_id:self.profile.id];
    };
    factory.onAcceptFriend = ^(){
        [self showMessageDialog:ls(@"wall_confirm_friend_q") buttonPositive:ls(@"wall_confirm_friend_q_yes") buttonNegative:ls(@"wall_confirm_friend_q_no") positiveHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {
            [self showProgressHUD];
            [[[OPMemberService alloc]init]postAcceptFriend:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
                [self hideProgressHUD];
                [self updateData:NO];
            } onFailure:^(NSInteger code, NSString *str) {
                [self hideProgressHUD];
            }] target_member_id:self.profile.id];
        } negativeHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {
            [self showProgressHUD];
            [[[OPMemberService alloc]init]postDeleteFriend:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
                [self hideProgressHUD];
                [self updateData:NO];
            } onFailure:^(NSInteger code, NSString *str) {
                [self hideProgressHUD];
            }] friend_relationship_id:self.profile.friend_relation.friend_relationship_id];
        }];
    };
    factory.onDeleteFriend = ^(){
        [self showMessageDialog:ls(@"wall_unfriend_q") buttonPositive:ls(@"wall_unfriend_q_yes") buttonNegative:ls(@"msg_no") positiveHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {
            [self showProgressHUD];
            [[[OPMemberService alloc]init]postDeleteFriend:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
                [self hideProgressHUD];
                [self updateData:NO];
            } onFailure:^(NSInteger code, NSString *str) {
                [self hideProgressHUD];
            }] friend_relationship_id:self.profile.friend_relation.friend_relationship_id];
        } negativeHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {
            
        }];
    };
    
    return factory;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateData:NO];
}

- (void)configUI {
    [super configUI];
}

- (void)updateData:(BOOL)forceUpdate {
    [self showProgressHUD];
    [[[OPProfileService alloc]init]getProfile:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
        [self hideProgressHUD];
        self.profile = object;
//        [self updateUI];
        
        [[[OPPostsService alloc]init]getPostsByMember:[[HTListResponseHandler alloc]initWithOnResult:^(NSArray *array) {
            self.models = array;
            [self updateUI];
        } onFailure:^(NSInteger code, NSString *str) {
            
        }] member_id:self.profileId];
    } onFailure:^(NSInteger code, NSString *str) {
        [self hideProgressHUD];
    }] member_id:self.profileId];
}

- (void)updateUI {
    [super updateUI];
    
    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        @synchronized (self) {
            ((OPWallFactory *)self.factory).profile = self.profile;
            [self.factory loadData:self.models];
            NSLog(@"[OPWallVC] Loading models with count: %@",@(self.models.count));
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
