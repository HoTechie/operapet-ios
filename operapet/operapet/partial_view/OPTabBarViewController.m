//
//  OPTabBarViewController.m
//  operapet
//
//  Created by Gary KK on 11/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPTabBarViewController.h"

@interface OPTabBarViewController ()

@end

@implementation OPTabBarViewController

-(id)initWithViewControllers:(NSArray *)viewControllers {
    self = [super init];
    
    for(id vc in viewControllers) {
        [self addChildViewController:vc];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (OPViewController *)currentViewController {
    if(self.currentTab < self.childViewControllers.count) {
        return self.childViewControllers[self.currentTab];
    }
    
    return nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getNibName {
    return @"OPTabBarViewController";
}

- (void)configUI {
    [super configUI];
    
    self.tabBarButtons = [NSArray array];
    [self configTabBarButton:self.btnNewsFeed];
    [self configTabBarButton:self.btnFriends];
    [self configTabBarButton:self.btnTags];
    [self configTabBarButton:self.btnMyWall];
    
    [self.btnNewPost addTarget:self action:@selector(buttonNewPostPressed) forControlEvents:UIControlEventTouchUpInside];
    [self updateUI];
}

- (void)buttonNewPostPressed {
    
}

- (void)configTabBarButton:(UIButton *)button {
    NSMutableArray *tabBarButtons = [NSMutableArray arrayWithArray:self.tabBarButtons];
    [button addTarget:self action:@selector(onClickBtnTab:) forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:[button.currentBackgroundImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [tabBarButtons addObject:button];
    self.tabBarButtons = tabBarButtons;
}

- (void)setCurrentTab:(OPTabBarTab)currentTab {
    _currentTab = currentTab;
    [self updateUI];
}

- (void)onClickBtnTab:(UIButton *)button {
    self.currentTab = (int)[self.tabBarButtons indexOfObject:button];
}

- (void)updateUI {
    [super updateUI];
    
    for(UIButton *btn in self.tabBarButtons) {
        btn.tintColor = color_tab_icon_color;
    }
    
    UIButton *currentTabBtn = self.tabBarButtons[self.currentTab];
    currentTabBtn.tintColor = color_tab_icon_color_highlight;
    
    for(UIView *view in self.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    OPViewController *currentViewController = [self currentViewController];
    if(currentViewController) {
        self.navigationItem.title = [currentViewController getTitle];
        [self.contentView addSubview:currentViewController.view];
        [self constrainView:currentViewController.view equalView:self.contentView];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
