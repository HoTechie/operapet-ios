//
//  OPRegistrationViewController.h
//  operapet
//
//  Created by Gary KK on 10/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPLoginContentViewController.h"
#import "HTTextField.h"

@interface OPRegistrationViewController : OPLoginContentViewController

@property (strong, nonatomic) IBOutlet HTTextField *txtLastName;
@property (strong, nonatomic) IBOutlet HTTextField *txtFirstName;
@property (strong, nonatomic) IBOutlet HTTextField *txtEmail;
@property (strong, nonatomic) IBOutlet HTTextField *txtPhoneNumber;
@property (strong, nonatomic) IBOutlet UIButton *btnEULA;
@property (strong, nonatomic) IBOutlet UIButton *btnRegister;
@property (strong, nonatomic) IBOutlet UIButton *btnViewTerms;


@property NSString *facebookId;
@property NSString *facebookToken;

- (id)initWithFacebookId:(NSString *)facebookId facebookToken:(NSString *)facebookToken;

@end
