//
//  OPPetDetailViewController.h
//  operapet
//
//  Created by Gary KK on 18/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListViewController.h"
#import "OPPetDetailFactory.h"
#import "OPPetService.h"

@interface OPPetDetailViewController : BaseListViewController

-(id)initWithPetId:(NSInteger)petId;


@property NSInteger petId;
@property OPPet *pet;
@property BOOL isKeyboardVisible;
@property void(^completion)();

@end
