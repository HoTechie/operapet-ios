//
//  OPLoginVerifySMSViewController.m
//  operapet
//
//  Created by Gary KK on 11/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPLoginVerifySMSViewController.h"
#import "OPLoginViewController.h"

@interface OPLoginVerifySMSViewController ()

@end

@implementation OPLoginVerifySMSViewController

- (id)initWithPhoneNumber:(NSString *)phoneNumber {
    self = [super init];
    self.phoneNumber = phoneNumber;
    return self;
}

- (id)initWithFacebookId:(NSString *)facebookId {
    self = [super init];
    self.facebookId = facebookId;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configUI {
    [super configUI];
    self.txtSMS.placeholder = ls(@"輸入 SMS 驗證碼確認戶口");
    [self.txtSMS setIcon:[UIImage imageNamed:@"icon_sms_code"]];
    self.txtSMS.returnKeyType = UIReturnKeyDone;
    self.txtSMS.delegate = (id<UITextFieldDelegate>)self.parentViewController;
    
    [self.btnConfirm setTitle:ls(@"確認") forState:UIControlStateNormal];
    [self.btnConfirm addTarget:self action:@selector(onClickConfirm) forControlEvents:UIControlEventTouchUpInside];
    
    if(self.facebookId) {
        [self showProgressHUD];
        
        [[[OPMemberService alloc]init]postFbLoginSendSmsApi:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
            [self hideProgressHUD];
            NSLog(@"[FbLogin] SMS Sent");
        } onFailure:^(NSInteger code, NSString *str) {
            [self hideProgressHUD];
            NSLog(@"[FbLogin] SMS Send failed with code %@", [@(code) stringValue]);
        }] facebook_id:self.facebookId];
    }
}

- (void)onClickConfirm {
    if(self.txtSMS.text.length){
        [self showProgressHUD];
        
        if(self.phoneNumber) [self normalLogin];
        if(self.facebookId) [self facebookLogin];
        
    } else {
        // show error for not inputing verification code
    }
}

- (void)normalLogin {
    [[[OPMemberService alloc]init]postNormalLogin:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
        [self hideProgressHUD];
        [SessionManager sharedInstance].currentProfile = object;
        [[OPNavigationController sharedInstance]pushViewController:[OPLoginViewController loginSuccessViewController] animated:YES];
    } onFailure:^(NSInteger err, NSString *str) {
        [self hideProgressHUD];
        [self handleFailure];
    }] mobile_number:self.phoneNumber verify_code:self.txtSMS.text];
}

- (void)facebookLogin {
    [[[OPMemberService alloc]init]postFbLogin:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
        [self hideProgressHUD];
        [SessionManager sharedInstance].currentProfile = object;
        [[OPNavigationController sharedInstance]pushViewController:[OPLoginViewController loginSuccessViewController] animated:YES];
    } onFailure:^(NSInteger code, NSString *str) {
        [self hideProgressHUD];
        [self handleFailure];
    }] facebook_id:self.facebookId verify_code:self.txtSMS.text];
}

- (void)handleFailure {
    [self showMessageDialog:@"驗證錯誤" buttonPositive:@"確定" positiveHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {
        
    }];
}

- (NSString *)getNibName {
    return NSStringFromClass([self class]);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
