//
//  OPLoginSuccessViewController.m
//  operapet
//
//  Created by Gary KK on 11/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPLoginSuccessViewController.h"
#import "OPHomeViewController.h"

@interface OPLoginSuccessViewController ()

@end

@implementation OPLoginSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getNibName {
    return NSStringFromClass([self class]);
}

- (void)configUI {
    [super configUI];
    
    self.lblMessage.text = @"驗證成功，\n請按「確認」進入應用程式!";
    
    [self.btnConfirm setTitle:ls(@"確認") forState:UIControlStateNormal];
    [self.btnConfirm addTarget:self action:@selector(onClickConfirm) forControlEvents:UIControlEventTouchUpInside];
}

- (void)onClickConfirm {
    [[OPNavigationController sharedInstance]pushViewController:[[OPHomeViewController alloc]init] animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
