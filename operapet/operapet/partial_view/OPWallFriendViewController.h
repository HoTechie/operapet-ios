//
//  OPWallFriendViewController.h
//  operapet
//
//  Created by Gary KK on 26/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListViewController.h"
#import "OPMemberService.h"
#import "OPFriendFactory.h"

@interface OPWallFriendViewController : BaseListViewController

- (id)initWithProfileId:(NSInteger)profileId;

@property NSInteger profileId;

@end
