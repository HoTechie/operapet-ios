//
//  OPPostListViewController.m
//  operapet
//
//  Created by Gary KK on 11/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPostListViewController.h"
#import "OPPostListCellFactory.h"
#import "OPPostsService.h"

@interface OPPostListViewController ()

@end

@implementation OPPostListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getNibName {
    return NSStringFromClass([self class]);
}

- (NSString *)getTitle {
    return ls(@"title_posts");
}

- (BaseListCellFactory *)createFactory {
    OPPostListCellFactory *factory = [[OPPostListCellFactory alloc]initWithCollectionView:self.listView];
    factory.onShare = self.onShare;
    factory.onLike = self.onLike;
    factory.onComment = self.onComment;
    return factory;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateData:NO];
}

- (void)updateUI {
    NSMutableArray *visiblePosts = [NSMutableArray array];
    
    for(OPPost *post in self.models) {
        if(self.categoryFactory.currentCatId == 0 || self.categoryFactory.currentCatId == post.category_id) {
            [visiblePosts addObject:post];
        }
    }
    
    [self.factory loadData:visiblePosts];
}

- (void)updateData:(BOOL)forceUpdate {
    [[[OPPostsService alloc]init]getPosts:[[HTListResponseHandler alloc]initWithOnResult:^(NSArray *array) {
        self.models = array;
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            [self updateUI];
        }];
    } onFailure:^(NSInteger code, NSString *msg) {
        
    }]];
}

- (void)configUI {
    [super configUI];
    
    self.categoryFactory = [[OPCategoryCellFactory alloc]initWithCollectionView:self.categoryList];
    [self.categoryFactory loadData:[SessionManager sharedInstance].postCategories];
    
    __weak OPPostListViewController *_self = self;
    self.categoryFactory.onChangeCategory = ^(){
        [_self updateUI];
    };
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
