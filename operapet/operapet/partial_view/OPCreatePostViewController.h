//
//  OPCreatePostViewController.h
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPViewController.h"
#import "OPCreatePostImageFactory.h"
#import "OPPostsService.h"

@interface OPCreatePostViewController : OPViewController

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *viewTxtBackground;
@property (strong, nonatomic) IBOutlet UIButton *btnCamera;
@property (strong, nonatomic) IBOutlet UIButton *btnSend;
@property (strong, nonatomic) IBOutlet UITextField *txtTag;
@property (strong, nonatomic) IBOutlet UITextView *txtFldContent;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionImages;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *scrollViewBottomSpace;

@property (strong, nonatomic) IBOutlet UIImageView *imgProfilePic;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblContentPlaceholder;
@property (strong, nonatomic) IBOutlet UIButton *btnCategory;

-(id) initWithCompletion:(void(^)())completion;

@property UIView* scrollContentView;
@property BOOL isKeyboardVisible;
@property NSMutableArray *images;
@property OPCreatePostImageFactory *factory;
@property NSInteger postCategory;
@property void(^completion)();

@end
