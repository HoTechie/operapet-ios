//
//  OPGiftDetailViewController.m
//  operapet
//
//  Created by Gary KK on 21/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPGiftDetailViewController.h"

@interface OPGiftDetailViewController ()<iCarouselDelegate, iCarouselDataSource>

@end

@implementation OPGiftDetailViewController

- (id)initWithGiftId:(NSInteger)giftId {
    self = [super init];
    self.giftId = giftId;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getNibName {
    return NSStringFromClass([OPGiftDetailViewController class]);
}

- (void)configUI {
    [super configUI];
    
    self.scrollContentView = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([OPGiftDetailViewController class]) owner:self options:0][1];
    [self.scrollView addSubview:self.scrollContentView];
    [self constrainView:self.scrollContentView inScrollView:self.scrollView];
    [self.view layoutIfNeeded];
    
    [self.btnRedeem addTarget:self action:@selector(redeem) forControlEvents:UIControlEventTouchUpInside];
    self.carousel.delegate = self;
    self.carousel.dataSource = self;
    self.carousel.pagingEnabled = YES;
    self.carousel.bounces = NO;
    self.carousel.clipsToBounds = YES;
    
    [self updateData:NO];
}

- (void)redeem {
    [self showProgressHUD];
    [[[OPGiftService alloc]init]postRedeemGift:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
        [self showMessageDialog:@"禮物換領成功" buttonPositive:@"確定" positiveHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {
            [self popViewController];
        }];
        [self hideProgressHUD];
    } onFailure:^(NSInteger code, NSString *str) {
        [self hideProgressHUD];
    }] gift_id:self.giftId];
}

- (void)updateData:(BOOL)forceUpdate {
    [[[OPGiftService alloc]init]getGifts:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
        self.gift = object;
        [self updateUI];
    } onFailure:^(NSInteger code, NSString *str) {
        
    }] gift_id:self.giftId];
}

- (void)updateUI {
    self.lblTitle.text = self.gift.name;
    self.lblDate.text = [self.gift formattedDateFromString:self.gift.create_time];
    self.lbldescription.text = self.gift._description;
    self.lblPoints.text = [@(self.gift.credit) stringValue];
    self.pageControl.numberOfPages = self.gift.images.count;
    [self.carousel reloadData];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.viewCircle.layer.cornerRadius = self.viewCircle.frame.size.height / 2.0;
    self.viewCircle.layer.masksToBounds = YES;
}

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel {
    return self.gift.images.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(nullable UIView *)view {
    UIImageView *imgView = (UIImageView *)view;
    
    if(!imgView) {
        imgView = [[UIImageView alloc]init];
        [imgView setContentMode:UIViewContentModeScaleAspectFill];
        [imgView setClipsToBounds:YES];
    }
    
    [imgView setFrame:carousel.bounds];
    
    
    NSInteger imageId = [self.gift.images[index] integerValue];
    [imgView sd_setImageWithURL:urlgiftimg(imageId)];
    
    return imgView;
}

-(void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel {
    self.pageControl.currentPage = carousel.currentItemIndex;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
