//
//  OPTabBarViewController.h
//  operapet
//
//  Created by Gary KK on 11/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPViewController.h"

typedef enum {
    OPTabBarTabNewsFeed,
    OPTabBarTabFriends,
    OPTabBarTabTags,
    OPTabBarTabMyWall
} OPTabBarTab;

@interface OPTabBarViewController : OPViewController

@property (strong, nonatomic) IBOutlet UIButton *btnNewsFeed;
@property (strong, nonatomic) IBOutlet UIButton *btnFriends;
@property (strong, nonatomic) IBOutlet UIButton *btnNewPost;
@property (strong, nonatomic) IBOutlet UIButton *btnTags;
@property (strong, nonatomic) IBOutlet UIButton *btnMyWall;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property NSArray *tabBarButtons;
@property (nonatomic) OPTabBarTab currentTab;

-(id)initWithViewControllers:(NSArray *)viewControllers;
-(void)buttonNewPostPressed;

@end
