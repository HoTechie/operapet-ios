//
//  OPWallViewController.h
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "BaseListViewController.h"
#import "OPWallFactory.h"
#import "OPProfileService.h"
#import "OPPostsService.h"

@interface OPWallViewController : BaseListViewController

@property OPProfile *profile;
@property NSInteger profileId;

-(id)initWithProfileId:(NSInteger)profileId;

@end
