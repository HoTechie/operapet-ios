//
//  OPPostsService.m
//  operapet
//
//  Created by Gary KK on 13/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPostsService.h"

@implementation OPPostsService

-(void)getPosts:(HTListResponseHandler *)handler {
    NSString *path = [self pathing:@"allpostapi", nil];
    HTArrayParser *parser = [HTArrayParser model:[OPPost class]];
    [[OPServiceManager sharedInstance]get:path headers:[OPServiceManager sharedInstance].requestHeaders parser:parser handler:handler];
}

-(void)getPostsByCategory:(HTListResponseHandler *)handler category_id:(NSInteger)category_id {
    NSString *path = [self pathing:@"CategoryPostApi/{category_id}", int(category_id), nil];
    HTArrayParser *parser = [HTArrayParser model:[OPPost class]];
    [[OPServiceManager sharedInstance]get:path parser:parser handler:handler];
}

-(void)getPostsByMember:(HTListResponseHandler *)handler member_id:(NSInteger)member_id {
    NSString *path = [self pathing:@"allpostapi/{member_id}", int(member_id), nil];
    HTArrayParser *parser = [HTArrayParser model:[OPPost class]];
    [[OPServiceManager sharedInstance]get:path parser:parser handler:handler];
}

/* Tag Related */
-(void)getPostByTagId:(HTListResponseHandler *)handler post_tag_id:(NSInteger)post_tag_id {
    NSString *path = [self pathing:@"GetPostByTagIdApi/{post_tag_id}", int(post_tag_id), nil];
    HTArrayParser *parser = [HTArrayParser model:[OPPost class]];
    [[OPServiceManager sharedInstance]get:path parser:parser handler:handler];
}

-(void)postGetSearchTagResult:(HTListResponseHandler *)handler search_key:(NSString *)search_key {
    NSString *path = [self pathing:@"GetSearchTagResultApi", nil];
    HTArrayParser *parser = [HTArrayParser model:[OPTag class]];
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:[OPServiceManager sharedInstance].requestHeaders
                                     parts:@{@"search_key": search_key}
                                 fileparts:@[]
                                    parser:parser
                                   handler:handler];
}

/* Post Detail */

-(void)getPostDetail:(HTModelResponseHandler *)handler post_id:(NSInteger)post_id {
    NSString *path = [self pathing:@"PostDetailApi/{post_id}", int(post_id), nil];
    HTModelParser *parser = [HTModelParser model:[OPPost class]];
    [[OPServiceManager sharedInstance]get:path parser:parser handler:handler];
}

/* Post Actions */

-(void)getWhoLikePost:(HTListResponseHandler *)handler post_id:(NSInteger)post_id {
    NSString *path = [self pathing:@"GetWhoLikePostapi/{post_id}", int(post_id), nil];
    HTArrayParser *parser = [HTArrayParser model:[OPProfile class]];
    [[OPServiceManager sharedInstance]get:path parser:parser handler:handler];
}

-(void)postCreateComment:(HTModelResponseHandler *)handler description:(NSString *)description post_id:(NSInteger)post_id {
    NSString *path = [self pathing:@"CreateCommentApi", nil];
    HTModelParser *parser = [HTModelParser model:[OPComment class]];
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:[OPServiceManager sharedInstance].requestHeaders
                                     parts:@{@"description": description,
                                             @"post_id": int(post_id)}
                                 fileparts:@[]
                                    parser:parser
                                   handler:handler];
}

-(void)postCreateCommentLike:(HTModelResponseHandler *)handler comment_id:(NSInteger)comment_id {
    NSString *path = [self pathing:@"CreateCommentLikeApi", nil];
    HTModelParser *parser = [HTModelParser model:[HTBaseModel class]];
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:[OPServiceManager sharedInstance].requestHeaders
                                     parts:@{@"comment_id": int(comment_id)}
                                 fileparts:@[]
                                    parser:parser
                                   handler:handler];
}

-(void)postCreateLike:(HTModelResponseHandler *)handler post_id:(NSInteger)post_id {
    NSString *path = [self pathing:@"CreateLikeApi", nil];
    HTModelParser *parser = [HTModelParser model:[HTBaseModel class]];
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:[OPServiceManager sharedInstance].requestHeaders
                                     parts:@{@"post_id": int(post_id)}
                                 fileparts:@[]
                                    parser:parser
                                   handler:handler];
}

-(void)postCreateShare:(HTModelResponseHandler *)handler post_id:(NSInteger)post_id {
    NSString *path = [self pathing:@"CreateShareApi", nil];
    HTModelParser *parser = [HTModelParser model:[HTBaseModel class]];
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:[OPServiceManager sharedInstance].requestHeaders
                                     parts:@{@"post_id": int(post_id)}
                                 fileparts:@[]
                                    parser:parser
                                   handler:handler];
}

/* Create Post */
#define MAX_POST_IMAGE_COUNT    5
-(void)postCreatePost:(HTModelResponseHandler *)handler description:(NSString *)description category_id:(NSInteger)category_id tags_list_string:(NSString *)tags_list_string images:(NSArray *)images {
    NSString *path = [self pathing:@"CreatePostApi", nil];
    HTModelParser *parser = [HTModelParser model:[OPPost class]];
    
    NSMutableArray *multiPartImages = [NSMutableArray array];
    for(NSInteger i = 0; i < images.count && i < MAX_POST_IMAGE_COUNT; i++) {
        UIImage *image = images[i];
        NSString *imgName = [NSString stringWithFormat:@"img%@",[@(i+1)stringValue]];
        
        [multiPartImages addObject:[HTMultiPartFileData fileDataWithData:UIImageJPEGRepresentation(image,0.8)
                                                                    name:imgName fileName:[NSString stringWithFormat:@"%@.jpg",imgName]]];
    }
    
    NSMutableDictionary *parts = [NSMutableDictionary dictionary];
    [parts setObject:description forKey:@"description"];
    [parts setObject:int(category_id) forKey:@"category_id"];
    if(tags_list_string && tags_list_string.length) {
        [parts setObject:tags_list_string forKey:@"tags_list_string"];
    }
    
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:[OPServiceManager sharedInstance].requestHeaders
                                     parts:parts
                                 fileparts:multiPartImages
                                    parser:parser
                                   handler:handler];
}

@end
