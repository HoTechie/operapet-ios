//
//  HTModelResponseHandler.h
//  myislands
//
//  Created by alan tsoi on 30/8/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTBaseResponseHandler.h"

@interface HTModelResponseHandler : HTBaseResponseHandler

@property (copy) ModelCallback onResult;

+(instancetype) handlerWithOnResult:(ModelCallback)onResult onFailure:(ErrorCallback)onFailure;
-(id)initWithOnResult:(ModelCallback)onResult onFailure:(ErrorCallback)onFailure;

@end
