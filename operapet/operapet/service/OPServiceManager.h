//
//  OPServiceManager.h
//  myislands
//
//  Created by alan tsoi on 30/8/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTBaseResponseHandler.h"
#import "HTServiceParser.h"
#import "UIImageView+WebCache.h"

#define urlprofilepic(x)    [NSURL URLWithString:[[OPServiceManager sharedInstance]getProfileImageURLForId:x]]
#define urlpostimg(x)       [NSURL URLWithString:[[OPServiceManager sharedInstance]getPostImageURLForId:x]]
#define urlpetimg(x)        [NSURL URLWithString:[[OPServiceManager sharedInstance]getPetImageURLForId:x]]
#define urlgiftimg(x)       [NSURL URLWithString:[[OPServiceManager sharedInstance]getGiftImageURLForId:x]]


@interface HTMultiPartFileData : NSObject

@property NSString *name;
@property NSData *data;
@property NSString *fileName;

+(id)fileDataWithData:(NSData *)data name:(NSString *)name fileName:(NSString *)fileName;

@end

@interface OPServiceManager : NSObject

+ (instancetype)sharedInstance;


@property NSString *baseURL;
@property BOOL isProduction;
@property BOOL adShown;

@property (nonatomic) NSString *langCode;

-(void)get:(NSString *)path
    parser:(HTServiceParser *)parser
   handler:(HTBaseResponseHandler *)handler;

-(void)get:(NSString *)path
   headers:(NSDictionary *)headers
    parser:(HTServiceParser *)parser
   handler:(HTBaseResponseHandler *)handler;

-(void)post:(NSString *)path
      paras:(NSDictionary *)paras
     parser:(HTServiceParser *)parser
    handler:(HTBaseResponseHandler *)handler;

-(void)post:(NSString *)path
    headers:(NSDictionary *)headers
      paras:(NSDictionary *)paras
     parser:(HTServiceParser *)parser
    handler:(HTBaseResponseHandler *)handler;

-(void)post:(NSString *)path
    headers:(NSDictionary *)headers
      parts:(NSDictionary *)parts
  fileparts:(NSArray *)fileparts
     parser:(HTServiceParser *)parser
    handler:(HTBaseResponseHandler *)handler;

-(NSString *)getPostImageURLForId:(NSInteger)id;
-(NSString *)getProfileImageURLForId:(NSInteger)id;
-(NSString *)getPetImageURLForId:(NSInteger)id;
-(NSString *)getGiftImageURLForId:(NSInteger)id;
-(NSString *)getAdvImageURLForId:(NSInteger)id;

-(NSDictionary *)requestHeaders;

@end
