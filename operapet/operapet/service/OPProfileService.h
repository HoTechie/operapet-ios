//
//  OPProfileService.h
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "HTBaseService.h"
#import "OPProfile.h"
#import "OPGift.h"
#import "OPNotification.h"

#define MY_RPOFILE_ID       -1

@interface OPProfileService : HTBaseService

-(void)getProfile:(HTModelResponseHandler *)handler;
-(void)getProfile:(HTModelResponseHandler *)handler member_id:(NSInteger)member_id;
-(void)getAllPet:(HTListResponseHandler *)handler member_id:(NSInteger)member_id;
-(void)getGiftHistory:(HTListResponseHandler *)handler;
-(void)getNotification:(HTListResponseHandler *)handler member_id:(NSInteger)member_id;
-(void)postDeviceId:(HTModelResponseHandler *)handler device_id:(NSString *)device_id;

@end
