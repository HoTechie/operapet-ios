//
//  OPPostsService.h
//  operapet
//
//  Created by Gary KK on 13/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "HTBaseService.h"
#import "OPPost.h"
#import "OPTag.h"

@interface OPPostsService : HTBaseService

-(void)getPosts:(HTListResponseHandler *)handler;
-(void)getPostsByCategory:(HTListResponseHandler *)handler category_id:(NSInteger)category_id;
-(void)getPostsByMember:(HTListResponseHandler *)handler member_id:(NSInteger)member_id;

-(void)postGetSearchTagResult:(HTListResponseHandler *)handler search_key:(NSString *)search_key;
-(void)getPostByTagId:(HTListResponseHandler *)handler post_tag_id:(NSInteger)post_tag_id;

-(void)getPostDetail:(HTModelResponseHandler *)handler post_id:(NSInteger)post_id;
-(void)getWhoLikePost:(HTListResponseHandler *)handler post_id:(NSInteger)post_id;
-(void)postCreateComment:(HTModelResponseHandler *)handler description:(NSString *)description post_id:(NSInteger)post_id;
-(void)postCreateCommentLike:(HTModelResponseHandler *)handler comment_id:(NSInteger)comment_id;
-(void)postCreateLike:(HTModelResponseHandler *)handler post_id:(NSInteger)post_id;
-(void)postCreateShare:(HTModelResponseHandler *)handler post_id:(NSInteger)post_id;

-(void)postCreatePost:(HTModelResponseHandler *)handler description:(NSString *)description category_id:(NSInteger)category_id
     tags_list_string:(NSString *)tags_list_string images:(NSArray *)images;

@end
