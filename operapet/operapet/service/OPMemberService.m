//
//  OPMemberService.m
//  operapet
//
//  Created by Gary KK on 14/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPMemberService.h"

@implementation OPMemberService

-(void)postNormalLogin:(HTModelResponseHandler *)handler mobile_number:(NSString *)mobile_number verify_code:(NSString *)verify_code {
    NSString *path = [self pathing:@"normalLoginApi", nil];
    HTModelParser *parser = [HTModelParser model:[OPProfile class]];
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:@{}
                                     parts:@{@"mobile_number":mobile_number,
                                             @"verify_code":verify_code}
                                 fileparts:@[]
                                    parser:parser
                                   handler:handler];
}

-(void)postNormalLoginSendSms:(HTModelResponseHandler *)handler mobile_number:(NSString *)mobile_number {
    NSString *path = [self pathing:@"NormalLoginSendSmsApi", nil];
    HTModelParser *parser = [HTModelParser model:[OPProfile class]];
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:@{}
                                     parts:@{@"mobile_number":mobile_number}
                                 fileparts:@[]
                                    parser:parser
                                   handler:handler];
}

-(void)postCreateMember:(HTModelResponseHandler *)handler device_id:(NSString *)device_id device_type:(NSString *)device_type mobile_no:(NSString *)mobile_no lastname:(NSString *)lastname firstname:(NSString *)firstname email:(NSString *)email
{
    NSString *path = [self pathing:@"CreateMemberApi", nil];
    HTModelParser *parser = [HTModelParser model:[OPProfile class]];
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:@{}
                                     parts:@{@"device_id":device_id,
                                             @"device_type":device_type,
                                             @"mobile_no":mobile_no,
                                             @"lastname": lastname,
                                             @"firstname": firstname,
                                             @"email": email}
                                 fileparts:@[]
                                    parser:parser
                                   handler:handler];
}

-(void)postCreateFbMember:(HTModelResponseHandler *)handler device_id:(NSString *)device_id device_type:(NSString *)device_type mobile_no:(NSString *)mobile_no lastname:(NSString *)lastname firstname:(NSString *)firstname email:(NSString *)email facebook_id:(NSString *)facebook_id facebook_token:(NSString *)facebook_token {
    NSString *path = [self pathing:@"CreateFbMemberApi", nil];
    HTModelParser *parser = [HTModelParser model:[OPProfile class]];
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:@{}
                                     parts:@{@"device_id":device_id,
                                             @"device_type":device_type,
                                             @"mobile_no":mobile_no,
                                             @"facebook_id":facebook_id,
                                             @"facebook_token":facebook_token,
                                             @"lastname": lastname,
                                             @"firstname": firstname,
                                             @"email": email}
                                 fileparts:@[]
                                    parser:parser
                                   handler:handler];
}

-(void)postRequestFriend:(HTModelResponseHandler *)handler target_member_id:(NSInteger)target_member_id {
    NSString *path = [self pathing:@"RequestFriendApi", nil];
    HTModelParser *parser = [HTModelParser model:[HTBaseModel class]];
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:[OPServiceManager sharedInstance].requestHeaders
                                     parts:@{@"target_member_id":int(target_member_id)}
                                 fileparts:@[]
                                    parser:parser
                                   handler:handler];
}

-(void)postAcceptFriend:(HTModelResponseHandler *)handler target_member_id:(NSInteger)target_member_id {
    NSString *path = [self pathing:@"AcceptFriendApi", nil];
    HTModelParser *parser = [HTModelParser model:[HTBaseModel class]];
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:[OPServiceManager sharedInstance].requestHeaders
                                     parts:@{@"target_member_id":int(target_member_id)}
                                 fileparts:@[]
                                    parser:parser
                                   handler:handler];
}

-(void)postDeleteFriend:(HTModelResponseHandler *)handler friend_relationship_id:(NSInteger)friend_relationship_id {
    NSString *path = [self pathing:@"DeleteFriendApi", nil];
    HTModelParser *parser = [HTModelParser model:[HTBaseModel class]];
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:[OPServiceManager sharedInstance].requestHeaders
                                     parts:@{@"friend_relationship_id":int(friend_relationship_id)}
                                 fileparts:@[]
                                    parser:parser
                                   handler:handler];
}

-(void)getFriendList:(HTListResponseHandler *)handler target_member_id:(NSInteger)target_member_id {
    NSString *path = [self pathing:@"GetFriendListApi/{target_member_id}", int(target_member_id), nil];
    HTArrayParser *parser = [HTArrayParser model:[OPProfile class]];
    
    [[OPServiceManager sharedInstance]get:path headers:[OPServiceManager sharedInstance].requestHeaders parser:parser handler:handler];
}

-(void)postUpdateMember:(HTModelResponseHandler *)handler
          profile_image:(UIImage *)profile_image
              device_id:(NSString *)device_id
            device_type:(NSString *)device_type
              mobile_no:(NSString *)mobile_no
               lastname:(NSString *)lastname
              firstname:(NSString *)firstname
                  email:(NSString *)email
{
    NSString *path = [self pathing:@"updatememberapi", nil];
    HTModelParser *parser = [HTModelParser model:[OPProfile class]];
    
    NSMutableArray *multiPartImages = [NSMutableArray array];
    if(profile_image) {
        [multiPartImages addObject:[HTMultiPartFileData fileDataWithData:UIImageJPEGRepresentation(profile_image,0.4)
                                                                    name:@"profile_image" fileName:@"profile_image.jpg"]];
    }
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:[OPServiceManager sharedInstance].requestHeaders
                                     parts:@{@"device_id":device_id,
                                             @"device_type":device_type,
                                             @"mobile_no":mobile_no,
                                             @"lastname":lastname,
                                             @"firstname":firstname,
                                             @"email":email}
                                 fileparts:multiPartImages
                                    parser:parser
                                   handler:handler];
}

-(void)getFriendRequestList:(HTListResponseHandler *)handler {
    NSString *path = [self pathing:@"GetFriendRequestListApi", nil];
    HTArrayParser *parser = [HTArrayParser model:[OPProfile class]];
    
    [[OPServiceManager sharedInstance]get:path headers:[OPServiceManager sharedInstance].requestHeaders parser:parser handler:handler];
}

-(void)postGetSearchFriendResult:(HTListResponseHandler *)handler search_key:(NSString *)search_key {
    NSString *path = [self pathing:@"GetSearchFriendResultApi", nil];
    HTArrayParser *parser = [HTArrayParser model:[OPProfile class]];
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:[OPServiceManager sharedInstance].requestHeaders
                                     parts:@{@"search_key":search_key}
                                 fileparts:@[]
                                    parser:parser
                                   handler:handler];
}

- (void)postReferalFriendSendSms:(HTModelResponseHandler *)handler phone_list_csv:(NSString *)phone_list_csv {
    NSString *path = [self pathing:@"ReferalFriendSendSmsApi", nil];
    HTModelParser *parser = [HTModelParser model:[HTBaseModel class]];
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:[OPServiceManager sharedInstance].requestHeaders
                                     parts:@{@"phone_list_csv":phone_list_csv}
                                 fileparts:@[]
                                    parser:parser
                                   handler:handler];
}

- (void)getActionHistory:(HTListResponseHandler *)handler {
    NSString *path = [self pathing:@"ActionHistoryApi", nil];
    HTArrayParser *parser = [HTArrayParser model:[OPActionHistory class]];
    
    [[OPServiceManager sharedInstance]get:path headers:[OPServiceManager sharedInstance].requestHeaders parser:parser handler:handler];
}

-(void)postIsFbExist:(HTModelResponseHandler *)handler facebook_id:(NSString *)facebook_id {
    NSString *path = [self pathing:@"IsFbExistApi", nil];
    HTModelParser *parser = [HTModelParser model:[HTBaseModel class]];
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:@{}
                                     parts:@{@"facebook_id":facebook_id}
                                 fileparts:@[]
                                    parser:parser
                                   handler:handler];
}

-(void)postFbLoginSendSmsApi:(HTModelResponseHandler *)handler facebook_id:(NSString *)facebook_id {
    NSString *path = [self pathing:@"FbLoginSendSmsApi", nil];
    HTModelParser *parser = [HTModelParser model:[OPProfile class]];
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:@{}
                                     parts:@{@"facebook_id":facebook_id}
                                 fileparts:@[]
                                    parser:parser
                                   handler:handler];
}

-(void)postFbLogin:(HTModelResponseHandler *)handler facebook_id:(NSString *)facebook_id verify_code:(NSString *)verify_code{
    NSString *path = [self pathing:@"FbLoginApi", nil];
    HTModelParser *parser = [HTModelParser model:[OPProfile class]];
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:@{}
                                     parts:@{@"facebook_id":facebook_id,
                                             @"verify_code":verify_code}
                                 fileparts:@[]
                                    parser:parser
                                   handler:handler];
}


@end
