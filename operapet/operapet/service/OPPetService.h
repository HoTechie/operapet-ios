//
//  OPPetService.h
//  operapet
//
//  Created by Gary KK on 18/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "HTBaseService.h"

#define PET_ID_NEW  0

@interface OPPetService : HTBaseService

-(void)getPetDetail:(HTModelResponseHandler *)handler pet_id:(NSInteger)pet_id;
-(void)postDeletePet:(HTModelResponseHandler *)handler pet_id:(NSInteger)pet_id;
-(void)posetCreatePet:(HTModelResponseHandler *)handler pet_id:(NSInteger)pet_id gener:(NSInteger)gender
            member_id:(NSInteger)member_id pet_type:(NSInteger)pet_type
           birth_date:(NSString *)birth_date description:(NSString *)description pet_image:(UIImage *)pet_image medical_record:(NSString *)medical_record
 last_body_check_date:(NSString *)last_body_check_date next_body_check_date:(NSString *)next_body_check_date last_bath_hair_cut_description:(NSString *)last_bath_hair_cut_description
   bmi_record_weight1:(NSString *)bmi_record_weight1 bmi_record_length1:(NSString *)bmi_record_length1
   bmi_record_weight2:(NSString *)bmi_record_weight2 bmi_record_length2:(NSString *)bmi_record_length2
   bmi_record_weight3:(NSString *)bmi_record_weight3 bmi_record_length3:(NSString *)bmi_record_length3
    bmi_record_date_1:(NSString *)bmi_record_date_1
    bmi_record_date_2:(NSString *)bmi_record_date_2
    bmi_record_date_3:(NSString *)bmi_record_date_3
 pin_card_json_string:(NSString *)pin_card_json_string;

@end
