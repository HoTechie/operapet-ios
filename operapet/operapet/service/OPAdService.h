//
//  OPAdService.h
//  operapet
//
//  Created by Gary KK on 19/4/2017.
//  Copyright © 2017 Hotechie. All rights reserved.
//

#import "HTBaseService.h"
#import "OPAdvert.h"

#define ADV_TYPE_POP_UP         1
#define ADV_TYPE_POST_LIST      2
#define ADV_TYPE_GIFT           3


@interface OPAdService : HTBaseService

-(void)getAdvListByType:(HTListResponseHandler *)handler type_id:(NSInteger)type_id;

@end
