//
//  OPPetService.m
//  operapet
//
//  Created by Gary KK on 18/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPetService.h"

@implementation OPPetService

-(void)getPetDetail:(HTModelResponseHandler *)handler pet_id:(NSInteger)pet_id {
    NSString *path = [self pathing:@"petdetailapi/{pet_id}", int(pet_id), nil];
    HTModelParser *parser = [HTModelParser model:[OPPet class]];
    [[OPServiceManager sharedInstance]get:path parser:parser handler:handler];
}

-(void)postDeletePet:(HTModelResponseHandler *)handler pet_id:(NSInteger)pet_id {
    NSString *path = [self pathing:@"DeletePetApi", nil];
    HTModelParser *parser = [HTModelParser model:[HTBaseModel class]];
    [[OPServiceManager sharedInstance]post:path headers:[OPServiceManager sharedInstance].requestHeaders parts:@{@"pet_id":int(pet_id)} fileparts:@[] parser:parser handler:handler];
}

-(void)posetCreatePet:(HTModelResponseHandler *)handler pet_id:(NSInteger)pet_id gener:(NSInteger)gender member_id:(NSInteger)member_id pet_type:(NSInteger)pet_type birth_date:(NSString *)birth_date description:(NSString *)description pet_image:(UIImage *)pet_image medical_record:(NSString *)medical_record last_body_check_date:(NSString *)last_body_check_date next_body_check_date:(NSString *)next_body_check_date last_bath_hair_cut_description:(NSString *)last_bath_hair_cut_description bmi_record_weight1:(NSString *)bmi_record_weight1 bmi_record_length1:(NSString *)bmi_record_length1 bmi_record_weight2:(NSString *)bmi_record_weight2 bmi_record_length2:(NSString *)bmi_record_length2 bmi_record_weight3:(NSString *)bmi_record_weight3 bmi_record_length3:(NSString *)bmi_record_length3 bmi_record_date_1:(NSString *)bmi_record_date_1 bmi_record_date_2:(NSString *)bmi_record_date_2 bmi_record_date_3:(NSString *)bmi_record_date_3 pin_card_json_string:(NSString *)pin_card_json_string {
    
    NSString *path = [self pathing:@"CreatePetApi", nil];
    HTModelParser *parser = [HTModelParser model:[OPPet class]];
    
    NSMutableArray *multiPartImages = [NSMutableArray array];
    if(pet_image) {
        [multiPartImages addObject:[HTMultiPartFileData fileDataWithData:UIImageJPEGRepresentation(pet_image,0.4)
                                                                    name:@"pet_image" fileName:@"pet_image.jpg"]];
    }
    
    NSMutableDictionary *parts = [NSMutableDictionary dictionary];
    parts[@"pet_id"] = int(pet_id);
    parts[@"gender"] = int(gender);
    parts[@"member_id"] = int(member_id);
    parts[@"pet_type"] = int(pet_type);
    if(birth_date) parts[@"birth_date"] = birth_date;
    if(description) parts[@"description"] = description;
    if(medical_record) parts[@"medical_record"] = medical_record;
    if(last_body_check_date) parts[@"last_body_check_date"] = last_body_check_date;
    if(next_body_check_date) parts[@"next_body_check_date"] = next_body_check_date;
    if(last_bath_hair_cut_description) parts[@"last_bath_hair_cut_description"] = last_bath_hair_cut_description;
    if(bmi_record_weight1) parts[@"bmi_record_weight_1"] = bmi_record_weight1;
    if(bmi_record_length1) parts[@"bmi_record_length_1"] = bmi_record_length1;
    if(bmi_record_weight2) parts[@"bmi_record_weight_2"] = bmi_record_weight2;
    if(bmi_record_length2) parts[@"bmi_record_length_2"] = bmi_record_length2;
    if(bmi_record_weight3) parts[@"bmi_record_weight_3"] = bmi_record_weight3;
    if(bmi_record_length3) parts[@"bmi_record_length_3"] = bmi_record_length3;
    if(bmi_record_date_1) parts[@"bmi_record_date_1"] = bmi_record_date_1;
    if(bmi_record_date_2) parts[@"bmi_record_date_2"] = bmi_record_date_2;
    if(bmi_record_date_3) parts[@"bmi_record_date_3"] = bmi_record_date_3;
    if(pin_card_json_string) parts[@"pin_card_json_string"] = pin_card_json_string;
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:[OPServiceManager sharedInstance].requestHeaders
                                     parts:parts
                                 fileparts:multiPartImages
                                    parser:parser
                                   handler:handler];
}

@end
