//
//  HTListResponseHandler.h
//  myislands
//
//  Created by alan tsoi on 30/8/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTBaseResponseHandler.h"

@interface HTListResponseHandler : HTBaseResponseHandler

@property (copy) ArrayCallback onResult;

+(instancetype)handlerWithOnResult:(ArrayCallback)onResult onFailure:(ErrorCallback)onFailure;
-(id)initWithOnResult:(ArrayCallback)onResult onFailure:(ErrorCallback)onFailure;

@end
