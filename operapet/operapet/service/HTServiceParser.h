//
//  HTServiceParser.h
//  myislands
//
//  Created by alan tsoi on 31/8/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTServiceParser : NSObject
-(id)parse:(NSString *)input;
-(id)initWithModel:(Class) model;
@property Class model;
@end

@interface HTModelParser : HTServiceParser
+(id)model:(Class)model;
@end

@interface HTArrayParser : HTServiceParser
+(id)model:(Class)model;
@end
