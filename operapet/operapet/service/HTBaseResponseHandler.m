//
//  MIResponseHandler.m
//  myislands
//
//  Created by alan tsoi on 30/8/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "HTBaseResponseHandler.h"

@implementation HTBaseResponseHandler

-(id)initWithOnFailure:(ErrorCallback)onFailure {
    self = [super init];
    self.onFailure = onFailure;
    return self;
}

-(void)onResponse:(id)res { }

-(void)onRequestFailed:(NSInteger)code msg:(NSString *)msg {
    NSLog(@"[Request] Request failed with code %@, messge: %@", [@(code) stringValue], msg);
    if(self.onFailure) self.onFailure(code, msg);
}

@end
