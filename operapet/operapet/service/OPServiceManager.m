//
//  OPServiceManager.m
//  myislands
//
//  Created by alan tsoi on 30/8/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "OPServiceManager.h"
#import "Constants.h"
#import "AFNetworking.h"

#import "SessionManager.h"

@implementation HTMultiPartFileData

+(id)fileDataWithData:(NSData *)data name:(NSString *)name fileName:(NSString *)fileName {
    HTMultiPartFileData *part = [[self alloc]init];
    part.name = name;
    part.data = data;
    part.fileName = fileName;
    return part;
}

@end

@interface OPServiceManager()
@property AFHTTPSessionManager *manager;
@end

@implementation OPServiceManager

+ (instancetype)sharedInstance {
    static id sharedInstance = NULL;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(id)init {
    self = [super init];
    
    self.isProduction = IS_PRODUCTION;
    self.baseURL = IS_PRODUCTION ? API_PRODUCTION : API_SANDBOX;
    self.manager = [AFHTTPSessionManager manager];
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    return self;
}

-(NSString *)postRequestURL:(NSString *)path {
    if(path.length) {
        BOOL hasTrailingSlash = [self.baseURL characterAtIndex:self.baseURL.length - 1] == '/';
        NSString *api_path = hasTrailingSlash ? @"" : @"/";
        
        NSString *targetUrlString = [NSString stringWithFormat:@"%@%@%@", self.baseURL, api_path, path];
        return targetUrlString;
    }
    
    return @"";
}

-(NSString *)requestURL:(NSString *)path {
    if(path.length) {
        BOOL hasTrailingSlash = [self.baseURL characterAtIndex:self.baseURL.length - 1] == '/';
        NSString *api_path = hasTrailingSlash ? @"" : @"/";
        
        NSString *targetUrlString = [NSString stringWithFormat:@"%@%@%@", self.baseURL, api_path, path];
        return targetUrlString;
    }
    
    return @"";
}

-(NSString *)webviewURL:(NSString *)path {
    if(path.length) {
        BOOL hasTrailingSlash = [self.baseURL characterAtIndex:self.baseURL.length - 1] == '/';
        NSString *slash = hasTrailingSlash ? @"" : @"/";
        return [NSString stringWithFormat:@"%@%@%@?webview=1", self.baseURL, slash, path];
    }
    
    return @"";
}

-(void)handleTask:(NSURLSessionDataTask*) task responseObject:(id)responseObject withParser:(HTServiceParser *)parser respHandler:(HTBaseResponseHandler *)handler {
    if(responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        id res = [parser parse:responseStr];
        
        if(res) [handler onResponse:res];
        else [handler onResponse:nil];
    } else [handler onRequestFailed:0 msg:@""];
}

/* GET */

-(void)get:(NSString *)path parser:(HTServiceParser *)parser handler:(HTBaseResponseHandler *)handler {
    [self get:path headers:[self requestHeaders] parser:parser handler:handler];
}

-(void)get:(NSString *)path headers:(NSDictionary *)headers parser:(HTServiceParser *)parser handler:(HTBaseResponseHandler *)handler {
    NSString *urlStr = [self requestURL:path];
    
    NSError *err;
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer]requestWithMethod:@"GET" URLString:urlStr parameters:0 error:&err];
    NSMutableDictionary *mergedHeaders = [NSMutableDictionary dictionaryWithDictionary:request.allHTTPHeaderFields];
    [mergedHeaders addEntriesFromDictionary:headers];
    [request setAllHTTPHeaderFields:mergedHeaders];
    
    NSURLSessionDataTask *task = [self.manager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if(!error && responseObject) {
            [self handleTask:task responseObject:responseObject withParser:parser respHandler:handler];
        } else {
            NSData *data = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
            NSString *err = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            
            NSHTTPURLResponse *res = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
            NSInteger statusCode = 0;
            if(res) statusCode = res.statusCode;
            
            [handler onRequestFailed:statusCode msg:err];
        }
    }];
    [task resume];
}

/* POST */

-(void)post:(NSString *)path paras:(NSDictionary *)paras parser:(HTServiceParser *)parser handler:(HTBaseResponseHandler *)handler {
    NSString *urlStr = [self postRequestURL:path];
    
    [self.manager POST:urlStr parameters:paras progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        id res = [parser parse:responseStr];
        
        if(res) [handler onResponse:res];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *data = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSString *err = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        
        NSHTTPURLResponse *res = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
        NSInteger statusCode = 0;
        if(res) statusCode = res.statusCode;
        
        [handler onRequestFailed:statusCode msg:err];
    }];
}

-(void)post:(NSString *)path headers:(NSDictionary *)headers paras:(NSDictionary *)paras parser:(HTServiceParser *)parser handler:(HTBaseResponseHandler *)handler {
    NSString *urlStr = [self requestURL:path];
    
    NSError *err;
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer]requestWithMethod:@"POST" URLString:urlStr parameters:paras error:&err];
    NSMutableDictionary *mergedHeaders = [NSMutableDictionary dictionaryWithDictionary:request.allHTTPHeaderFields];
    [mergedHeaders addEntriesFromDictionary:headers];
    [request setAllHTTPHeaderFields:mergedHeaders];
    
    NSURLSessionDataTask *task = [self.manager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if(!error && responseObject) {
            [self handleTask:task responseObject:responseObject withParser:parser respHandler:handler];
        } else {
            NSData *data = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
            NSString *err = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            NSHTTPURLResponse *res = (id)response;
            NSInteger statusCode = 0;
            if(res) statusCode = res.statusCode;
            [handler onRequestFailed:statusCode msg:err];
        }
    }];
    [task resume];
}

-(void)post:(NSString *)path
    headers:(NSDictionary *)headers
      parts:(NSDictionary *)parts
  fileparts:(NSArray *)fileparts
     parser:(HTServiceParser *)parser
    handler:(HTBaseResponseHandler *)handler {
    NSString *urlStr = [self requestURL:path];
    
    NSError *err;
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer]multipartFormRequestWithMethod:@"POST" URLString:urlStr parameters:@{} constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        for(NSString *name in parts) {
            NSString *valueString = parts[name];
            NSData *data = [valueString dataUsingEncoding:NSUTF8StringEncoding];
            [formData appendPartWithFormData:data name:name];
        }
        
        for(HTMultiPartFileData *partFileData in fileparts) {
            [formData appendPartWithFileData:partFileData.data name:partFileData.name fileName:partFileData.fileName mimeType:@"image/jpeg"];
        }
        
    } error:&err];
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:request.allHTTPHeaderFields];
    [dictionary addEntriesFromDictionary:headers];
    [request setAllHTTPHeaderFields:dictionary];
    
    NSURLSessionDataTask *task = [self.manager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if(!error && responseObject) {
            [self handleTask:task responseObject:responseObject withParser:parser respHandler:handler];
        } else {
            NSData *data = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
            NSString *err = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            NSHTTPURLResponse *res = (id)response;
            NSInteger statusCode = 0;
            if(res) statusCode = res.statusCode;
            [handler onRequestFailed:statusCode msg:err];
        }
    }];
    [task resume];
}

/* MISC */

- (NSString *)getPostImageURLForId:(NSInteger)id {
    return [self requestURL:[NSString stringWithFormat:@"GetPostImageApi/%@",[@(id)stringValue]]];
}

- (NSString *)getProfileImageURLForId:(NSInteger)id {
    return [self requestURL:[NSString stringWithFormat:@"GetProfileImageApi/%@",[@(id)stringValue]]];
}

- (NSString *)getPetImageURLForId:(NSInteger)id {
    return [self requestURL:[NSString stringWithFormat:@"GetPetImageApi/%@",[@(id) stringValue]]];
}

- (NSString *)getGiftImageURLForId:(NSInteger)id {
    return [self requestURL:[NSString stringWithFormat:@"GetGiftImageApi/%@",[@(id) stringValue]]];
}

- (NSString *)getAdvImageURLForId:(NSInteger)id {
    return [self requestURL:[NSString stringWithFormat:@"GetAdvImageApi/%@",[@(id) stringValue]]];
}

-(NSDictionary *)requestHeaders {
    NSMutableDictionary *headers = [NSMutableDictionary dictionary];
    OPProfile *profile = [SessionManager sharedInstance].currentProfile;
    if(profile) {
        headers[@"memberid"] = [@(profile.id) stringValue];
        headers[@"token"] = profile.token;
    }
    
    return headers;
}

@end
