//
//  OPPet.h
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "HTBaseModel.h"
#import "OPPinCard.h"

typedef enum {
    OPPetGenderMale = 1,
    OPPetGenderFemale = 2
} OPPetGender;

@protocol OPPinCard
@end

@interface OPPet : HTBaseModel

@property NSInteger id;
@property NSInteger member_id;
@property NSInteger gender;
@property NSInteger image_id;
@property NSInteger pet_type;
@property NSString *birth_date;
@property NSString *_description;
@property NSString *medical_record;
@property NSString *last_body_check_date;
@property NSString *next_body_check_date;
@property NSString *last_bath_hair_cut_description;
@property float bmi_record_weight_1;
@property float bmi_record_length_1;
@property float bmi_record_weight_2;
@property float bmi_record_length_2;
@property float bmi_record_weight_3;
@property float bmi_record_length_3;
@property NSString *bmi_record_date_1;
@property NSString *bmi_record_date_2;
@property NSString *bmi_record_date_3;
@property NSArray <OPPinCard> *pin_card_list;

-(NSString *)json_pin_cards;

-(void)setBirthDateWithDate:(NSDate *)date;
-(NSString *)formattedBirthdate;

-(NSDate *)dateFromString:(NSString *)string;
-(NSString *)stringFromDate:(NSDate *)date;

- (NSDateComponents *)dateCompsFromString:(NSString *)string;
- (NSString *)stringFromDateComps:(NSDateComponents *)comps;
- (void)updateBMIRecordWithWeight:(float)weight length:(float)length insert:(BOOL)insert;

@end
