//
//  OPGift.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPGift.h"

@implementation OPGift

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"_description": @"description",
                                                                  }];
}

-(BOOL)active {
    return self.status == OPGiftStatusRedeemed;
}


-(NSString *)statusString {
    if(self.status == OPGiftStatusRedeemed) return @"處理中";
    if(self.status == OPGiftStatusCancelled) return @"已取消";
    if(self.status == OPGiftStatusCollected) return @"已換領";
    return @"";
}

@end
