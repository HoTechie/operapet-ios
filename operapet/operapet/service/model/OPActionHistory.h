//
//  OPActionHistory.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPBaseModel.h"

@interface OPActionHistory : OPBaseModel

@property NSInteger id;
@property NSInteger type;
@property NSInteger credit;
@property NSString *_description;
@property NSString *create_time;

@end
