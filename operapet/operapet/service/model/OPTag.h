//
//  OPTag.h
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "HTBaseModel.h"

@interface OPTag : HTBaseModel
@property NSInteger id;
@property NSString *_description;
@property NSInteger image_id;
@end
