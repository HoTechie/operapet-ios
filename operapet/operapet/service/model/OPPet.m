//
//  OPPet.m
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPet.h"

@implementation OPPet

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"_description": @"description",
                                                                  }];
}

-(NSString *)json_pin_cards {
    NSString *json_arr_content = @"";
    for(OPPinCard *pinCard in self.pin_card_list) {
        if(json_arr_content.length) json_arr_content = [json_arr_content stringByAppendingString:@","];
        json_arr_content = [json_arr_content stringByAppendingString:[pinCard toJSONString]];
    }
    
    return [NSString stringWithFormat:@"[%@]",json_arr_content];
}

- (void)setBirthDateWithDate:(NSDate *)date {
    self.birth_date = [self stringFromDate:date];
}

- (NSString *)formattedBirthdate {
    NSDate *date = [self dateFromString:self.birth_date];
    
    NSDateFormatter *outFormatter = [[NSDateFormatter alloc]init];
    [outFormatter setDateFormat:@"dd / MM / yyyy"];
    return [outFormatter stringFromDate:date];
}

- (NSDate *)dateFromString:(NSString *)string {
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    return [formatter dateFromString:string];
}

- (NSString *)stringFromDate:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    return [formatter stringFromDate:date];
}

- (NSDateComponents *)dateCompsFromString:(NSString *)string {
    NSDate *date = [self dateFromString:string];
    NSCalendar *cal = [NSCalendar currentCalendar];
    return date ? [cal components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date] : nil;
}

- (NSString *)stringFromDateComps:(NSDateComponents *)comps {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDate *date = [cal dateFromComponents:comps];
    return [self stringFromDate:date];
}

-(void)updateBMIRecordWithWeight:(float)weight length:(float)length insert:(BOOL)insert {
    if(insert){
        self.bmi_record_weight_3 = self.bmi_record_weight_2;
        self.bmi_record_length_3 = self.bmi_record_length_2;
        self.bmi_record_date_3 = self.bmi_record_date_2;
        
        self.bmi_record_weight_2 = self.bmi_record_weight_1;
        self.bmi_record_length_2 = self.bmi_record_length_1;
        self.bmi_record_date_2 = self.bmi_record_date_1;
    }
    
    self.bmi_record_date_1 = [self stringFromDate:[NSDate date]];
    self.bmi_record_length_1 = length;
    self.bmi_record_weight_1 = weight;
}

@end
