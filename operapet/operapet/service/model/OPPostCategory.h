//
//  OPPostCategory.h
//  operapet
//
//  Created by Gary KK on 13/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "HTBaseModel.h"
#import <UIKit/UIKit.h>
@interface OPPostCategory : HTBaseModel

+(id)category;

@property NSInteger id;
@property UIColor *color;
@property UIColor *colorDarker;
@property NSString *iconFileName;
@property NSString *name;

@end
