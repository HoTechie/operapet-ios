//
//  OPNotification.m
//  operapet
//
//  Created by Gary KK on 19/4/2017.
//  Copyright © 2017 Hotechie. All rights reserved.
//

#import "OPNotification.h"

@implementation OPNotification

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"_description": @"description"
                                                                  }];
}

@end
