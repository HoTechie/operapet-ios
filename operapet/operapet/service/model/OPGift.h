//
//  OPGift.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPBaseModel.h"

typedef enum {
    OPGiftStatusDefault,
    OPGiftStatusCancelled = -1,
    OPGiftStatusRedeemed = 1,
    OPGiftStatusCollected = 2
} OPGiftStatus;

@interface OPGift : OPBaseModel

@property NSInteger id;
@property NSString *name;
@property NSString *_description;
@property NSInteger credit;
@property NSString *create_time;
@property NSArray *images;
@property NSInteger status;

-(BOOL) active;
-(NSString *)statusString;
@end
