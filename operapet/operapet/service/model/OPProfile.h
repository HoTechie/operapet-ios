//
//  OPProfile.h
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "HTBaseModel.h"
#import "OPFriendRelation.h"
#import "OPPet.h"

@protocol OPPet
@end

@interface OPProfile : HTBaseModel

@property NSInteger id;
@property NSString *user_name;
@property NSInteger image_id;
@property NSString *mobile;
@property NSString *email;
@property NSInteger member_level;
@property NSInteger credit_total;
@property NSInteger remain_credit;
@property NSInteger no_of_share;
@property NSInteger no_of_post;

@property NSString *lastname;
@property NSString *firstname;

@property NSArray <OPPet> *pets;
@property OPFriendRelation *friend_relation;

@property NSString *like_date;
@property NSString *token;

-(BOOL)isFriend;
-(BOOL)isPendingFriend;
-(BOOL)isWaitingMyResponse;
-(NSString *)memberLevelString;

@end
