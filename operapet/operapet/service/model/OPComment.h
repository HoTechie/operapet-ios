//
//  OPComment.h
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "HTBaseModel.h"
#import "OPProfile.h"

@interface OPComment : HTBaseModel

@property NSInteger id;
@property NSInteger no_of_like;
@property BOOL is_like;
@property NSString *_description;
@property NSString *create_time;
@property OPProfile *profile;

@end
