//
//  OPPinCard.h
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPBaseModel.h"

@interface OPPinCard : OPBaseModel

@property NSInteger id;
@property NSInteger pet_id;
@property NSString *recent_pin_record_date;
@property NSString *next_pin_record_date;
@property NSString *_description;
@property NSString *name;

@end
