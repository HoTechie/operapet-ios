//
//  OPBaseModel.h
//  operapet
//
//  Created by Gary KK on 21/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "HTBaseModel.h"

@interface OPBaseModel : HTBaseModel

-(NSDate *)dateFromString:(NSString *)string;
-(NSString *)stringFromDate:(NSDate *)date;

- (NSDateComponents *)dateCompsFromString:(NSString *)string;
- (NSString *)stringFromDateComps:(NSDateComponents *)comps;
- (NSString *)formattedDateFromString:(NSString *)string;

@end
