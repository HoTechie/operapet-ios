//
//  OPPost.h
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "HTBaseModel.h"
#import "OPProfile.h"
#import "OPComment.h"

@protocol OPTag
@end

@protocol OPComment
@end

@interface OPPost : HTBaseModel

@property NSInteger id;
@property NSInteger no_of_like;
@property NSInteger no_of_comment;
@property NSString* _description;
@property BOOL is_like;
@property NSString *create_time;
@property NSInteger category_id;
@property OPProfile *profile;
@property NSArray <OPTag> *tags;
@property NSArray *images;
@property NSArray <OPComment> *cms;

@end
