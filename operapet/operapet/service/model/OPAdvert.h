//
//  OPAdvert.h
//  operapet
//
//  Created by Gary KK on 19/4/2017.
//  Copyright © 2017 Hotechie. All rights reserved.
//

#import "OPBaseModel.h"

@interface OPAdvert : OPBaseModel

@property NSInteger image_id;
@property NSInteger type;
@property NSString *link;

@end
