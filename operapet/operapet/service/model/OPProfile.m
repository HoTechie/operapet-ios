//
//  OPProfile.m
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPProfile.h"
#import "SessionManager.h"
#import "Constants.h"

@implementation OPProfile

-(BOOL) isFriend {
    return self.friend_relation.status == OPFriendRelationConfirmed;
}

-(BOOL) isPendingFriend {
    return self.friend_relation.status == OPFriendRelationPending;
}

-(BOOL) isWaitingMyResponse {
    return self.friend_relation.status == OPFriendRelationPending && self.friend_relation.destination_id == [SessionManager sharedInstance].currentProfile.id;
}

-(NSString *)memberLevelString {
    NSString *key = [NSString stringWithFormat:@"member_level_%@", self.member_level >= 3 ? @(3) : @(self.member_level)];
    return ls(key);
}

@end
