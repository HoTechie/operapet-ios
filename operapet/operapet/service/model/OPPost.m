//
//  OPPost.m
//  operapet
//
//  Created by Gary KK on 12/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPPost.h"

@implementation OPPost

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"_description": @"description",
                                                                  }];
}

@end
