//
//  HTContainerModel.h
//  myislands
//
//  Created by alan tsoi on 31/8/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "HTBaseModel.h"

@interface HTContainerModel : HTBaseModel

@property NSInteger total;
@property NSInteger per_page;
@property NSInteger last_page;
@property NSString *next_page_url;
@property NSString *prev_page_url;
@property NSInteger from;
@property NSInteger to;
@property id<Optional> data ;

@end
