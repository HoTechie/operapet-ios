//
//  OPBaseModel.m
//  operapet
//
//  Created by Gary KK on 21/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPBaseModel.h"
#import "SessionManager.h"

@implementation OPBaseModel

- (NSDate *)dateFromString:(NSString *)string {
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    return [formatter dateFromString:string];
}

- (NSString *)stringFromDate:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    return [formatter stringFromDate:date];
}

- (NSDateComponents *)dateCompsFromString:(NSString *)string {
    NSDate *date = [self dateFromString:string];
    NSCalendar *cal = [NSCalendar currentCalendar];
    return [cal components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date];
}

- (NSString *)stringFromDateComps:(NSDateComponents *)comps {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDate *date = [cal dateFromComponents:comps];
    return [self stringFromDate:date];
}

- (NSString *)formattedDateFromString:(NSString *)string {
    return [[SessionManager sharedInstance]formattedDateString:string];
}

@end
