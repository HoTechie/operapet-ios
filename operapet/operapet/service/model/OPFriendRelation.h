//
//  OPFriendRelation.h
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "HTBaseModel.h"

typedef enum {
    OPFriendRelationNone,
    OPFriendRelationPending,
    OPFriendRelationConfirmed
}OPFriendRelationStatus;

@interface OPFriendRelation : HTBaseModel

@property NSInteger friend_relationship_id;
@property NSInteger source_id;
@property NSInteger destination_id;
@property NSInteger status;

@end
