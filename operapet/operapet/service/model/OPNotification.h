//
//  OPNotification.h
//  operapet
//
//  Created by Gary KK on 19/4/2017.
//  Copyright © 2017 Hotechie. All rights reserved.
//

#import "OPBaseModel.h"

@interface OPNotification : OPBaseModel

@property NSInteger push_notification_id;
@property NSInteger member_id;
@property NSInteger post_id;
@property NSInteger image_id;
@property NSString *_description;
@property NSInteger status;
@property NSString *crt_date;

@end
