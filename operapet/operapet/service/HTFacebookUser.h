//
//  HTFacebookUser.h
//  operapet
//
//  Created by Gary KK on 23/3/2017.
//  Copyright © 2017 Hotechie. All rights reserved.
//

#import "HTBaseModel.h"

@interface HTFbPictureData : HTBaseModel
@property NSString *url;
@end

@interface HTFbPicture : HTBaseModel
@property HTFbPictureData *data;
@end

@interface HTFacebookUser : HTBaseModel

@property NSString *email;
@property NSString *id;
@property NSString *last_name;
@property NSString *first_name;
@property NSString *birthday;
@property HTFbPicture *picture;

@end
