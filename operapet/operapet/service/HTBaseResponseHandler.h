//  MIResponseHandler.h
//  myislands
//
//  Created by alan tsoi on 30/8/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTBaseResponseHandler : NSObject

typedef void (^ArrayCallback)(NSArray *array);
typedef void (^ModelCallback)(id object);
typedef void (^VoidCallback)();
typedef void (^ErrorCallback)(NSInteger code, NSString *str);

@property (copy) ErrorCallback onFailure;

-(id)initWithOnFailure:(ErrorCallback)onFailure;

-(void)onResponse:(id)res;
-(void)onRequestFailed:(NSInteger)code msg:(NSString *)msg;

@end



