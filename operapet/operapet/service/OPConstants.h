//
//  OPConstants.h
//  operapet
//
//  Created by Gary KK on 20/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OPConstant : NSObject
@property NSInteger id;
@property (nonatomic) NSString *name;
@end

@interface OPConstantPostCategory : OPConstant
+(id)postCategoryWithId:(NSInteger)id;
@end

@interface OPConstantPetCategory : OPConstant
@property NSArray *types;
+(id)petCategoryWithName:(NSString *)name types:(NSArray *)types;
@end

@interface OPConstantPetType : OPConstant
+(id)petTypeWithId:(NSInteger)id name:(NSString *)name;
@end

@interface OPConstants : NSObject

+ (instancetype)sharedInstance;

@property NSMutableArray *petCategories;
@property NSMutableArray *postCategories;

-(OPConstantPetCategory *)petCategoryForPetTypeId:(NSInteger)pet_type;
- (OPConstantPetType *)petTypeForPetTypeId:(NSInteger)pet_type;

@end
