//
//  HTBaseService.h
//  myislands
//
//  Created by alan tsoi on 30/8/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SessionManager.h"
#import "OPServiceManager.h"
#import "HTListResponseHandler.h"
#import "HTModelResponseHandler.h"

#define int(x) [NSString stringWithFormat:@"%ld", (long)x]

typedef enum {
    Model, List
} RequestType;

@interface HTBaseService : NSObject

-(NSString *)pathing:(NSString *)path,... NS_REQUIRES_NIL_TERMINATION;


@end
