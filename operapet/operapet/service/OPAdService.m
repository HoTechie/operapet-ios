//
//  OPAdService.m
//  operapet
//
//  Created by Gary KK on 19/4/2017.
//  Copyright © 2017 Hotechie. All rights reserved.
//

#import "OPAdService.h"


@implementation OPAdService

- (void)getAdvListByType:(HTListResponseHandler *)handler type_id:(NSInteger)type_id {
    NSString *path = [self pathing:@"/GetAdvListByTypeApi/{type_id}", int(type_id), nil];
    HTArrayParser *parser = [HTArrayParser model:[OPAdvert class]];
    [[OPServiceManager sharedInstance]get:path parser:parser handler:handler];
}

@end
