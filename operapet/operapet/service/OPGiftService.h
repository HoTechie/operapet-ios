//
//  OPGiftService.h
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "HTBaseService.h"
#import "OPGift.h"

@interface OPGiftService : HTBaseService

-(void)getGifts:(HTListResponseHandler *)handler;
-(void)getGifts:(HTModelResponseHandler *)handler gift_id:(NSInteger)gift_id;
-(void)postRedeemGift:(HTModelResponseHandler *)handler gift_id:(NSInteger)gift_id;
-(void)postCollectGift:(HTModelResponseHandler *)handler redemption_id:(NSInteger)redemption_id;

@end
