//
//  HTModelResponseHandler.m
//  myislands
//
//  Created by alan tsoi on 30/8/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "HTModelResponseHandler.h"

@implementation HTModelResponseHandler

+(instancetype)handlerWithOnResult:(ModelCallback)onResult onFailure:(ErrorCallback)onFailure {
    return [[self alloc]initWithOnResult:onResult onFailure:onFailure];
}

-(id)initWithOnResult:(ModelCallback)onResult onFailure:(ErrorCallback)onFailure {
    self = [super initWithOnFailure:onFailure];
    self.onResult = onResult;
    return self;
}

-(void)onResponse:(id)res {
    if(self.onResult) self.onResult(res);
}

@end
