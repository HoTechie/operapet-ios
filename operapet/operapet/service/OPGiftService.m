//
//  OPGiftService.m
//  operapet
//
//  Created by Gary KK on 25/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPGiftService.h"

@implementation OPGiftService

-(void)getGifts:(HTListResponseHandler *)handler {
    NSString *path = [self pathing:@"giftapi", nil];
    HTArrayParser *parser = [HTArrayParser model:[OPGift class]];
    [[OPServiceManager sharedInstance]get:path parser:parser handler:handler];
}

-(void)getGifts:(HTModelResponseHandler *)handler gift_id:(NSInteger)gift_id {
    NSString *path = [self pathing:@"giftapi/{gift_id}", int(gift_id), nil];
    HTModelParser *parser = [HTModelParser model:[OPGift class]];
    [[OPServiceManager sharedInstance]get:path parser:parser handler:handler];
}

-(void)postRedeemGift:(HTModelResponseHandler *)handler gift_id:(NSInteger)gift_id {
    NSString *path = [self pathing:@"RedeemGiftApi", nil];
    HTModelParser *parser = [HTModelParser model:[OPGift class]];
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:[OPServiceManager sharedInstance].requestHeaders
                                     parts:@{@"gift_id": int(gift_id)}
                                 fileparts:@[]
                                    parser:parser
                                   handler:handler];
}

-(void)postCollectGift:(HTModelResponseHandler *)handler redemption_id:(NSInteger)redemption_id {
    NSString *path = [self pathing:@"CollectGiftApi", nil];
    HTModelParser *parser = [HTModelParser model:[OPGift class]];
    
    [[OPServiceManager sharedInstance]post:path
                                   headers:[OPServiceManager sharedInstance].requestHeaders
                                     parts:@{@"redemption_id": int(redemption_id)}
                                 fileparts:@[]
                                    parser:parser
                                   handler:handler];
}

@end
