//
//  HTServiceParser.m
//  myislands
//
//  Created by alan tsoi on 31/8/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "HTServiceParser.h"
#import "HTContainerModel.h"

@implementation HTServiceParser

-(id)initWithModel:(Class)model {
    self = [super init];
    self.model = model;
    return self;
}

-(id)parse:(NSString *)input {
    JSONModelError *err;
    HTContainerModel *container = [[HTContainerModel alloc]initWithString:input error:&err];
    if(!err) return container;
    
    return nil;
}

@end

@implementation HTArrayParser

+(id)model:(Class)model {
    return [[self alloc]initWithModel:model];
}

-(id)parse:(id)input {
    HTContainerModel *container = [super parse:input];
    NSArray *dataArray = container ? (NSArray *)container.data : [NSArray array];
    
    NSMutableArray *result = [NSMutableArray array];
    if(dataArray) {
        for(NSDictionary *dict in dataArray) {
            NSError *err;
            HTBaseModel *model = [[self.model alloc]initWithDictionary:dict error:&err];
            if(!err) [result addObject:model];
        }
    }
    
    return result;
}


@end

@implementation HTModelParser

+(id)model:(Class)model {
    return [[self alloc]initWithModel:model];
}

-(id)parse:(id)input {
    HTContainerModel *container = [super parse:input];
    id data = container ? container.data : nil;
    
    if(data && [data isKindOfClass:[NSDictionary class]]) {
        NSError *err;
        HTBaseModel *model = [[self.model alloc]initWithDictionary:data error:&err];
        if(!err) return model;
    }
    
    return nil;
}

-(id)parseAsRoot:(NSString *)input {
    NSError *err;
    HTBaseModel *model = [[self.model alloc]initWithString:input error:&err];
    if(!err) return model;
    
    return nil;
}

@end
