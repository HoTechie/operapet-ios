//
//  OPConstants.m
//  operapet
//
//  Created by Gary KK on 20/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPConstants.h"

@implementation OPConstants

+ (instancetype)sharedInstance {
    static id sharedInstance = NULL;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    self = [super init];
    [self loadAllConstants];
    return self;
}

- (void)loadAllConstants {
    self.petCategories = [NSMutableArray array];
    
    NSMutableArray *cats = [NSMutableArray array];
    [cats addObject:[OPConstantPetType petTypeWithId:1 name:@"家貓"]];
    [cats addObject:[OPConstantPetType petTypeWithId:2 name:@"暹羅貓"]];
    [cats addObject:[OPConstantPetType petTypeWithId:3 name:@"金吉拉"]];
    [cats addObject:[OPConstantPetType petTypeWithId:4 name:@"緬因貓"]];
    [cats addObject:[OPConstantPetType petTypeWithId:5 name:@"伯曼貓"]];
    [cats addObject:[OPConstantPetType petTypeWithId:6 name:@"布偶貓"]];
    [cats addObject:[OPConstantPetType petTypeWithId:7 name:@"波斯貓"]];
    [cats addObject:[OPConstantPetType petTypeWithId:8 name:@"美國短毛貓"]];
    [cats addObject:[OPConstantPetType petTypeWithId:9 name:@"英國短毛貓"]];
    [cats addObject:[OPConstantPetType petTypeWithId:10 name:@"挪威森林貓"]];
    [cats addObject:[OPConstantPetType petTypeWithId:11 name:@"異國短毛貓"]];
    [cats addObject:[OPConstantPetType petTypeWithId:12 name:@"俄羅斯藍貓"]];
    [cats addObject:[OPConstantPetType petTypeWithId:13 name:@"喜瑪拉雅貓"]];
    [cats addObject:[OPConstantPetType petTypeWithId:14 name:@"蘇格蘭折耳貓"]];
    [cats addObject:[OPConstantPetType petTypeWithId:15 name:@"土耳其安哥拉貓"]];
    [cats addObject:[OPConstantPetType petTypeWithId:16 name:@"斯芬克斯貓（加拿大無毛貓）"]];
    [cats addObject:[OPConstantPetType petTypeWithId:17 name:@"其他"]];
    [self.petCategories addObject:[OPConstantPetCategory petCategoryWithName:@"貓" types:cats]];
    
    NSMutableArray *dogs = [NSMutableArray array];
    [dogs addObject:[OPConstantPetType petTypeWithId:18 name:@"松鼠"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:19 name:@"銀狐"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:20 name:@"沙皮"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:21 name:@"柴犬"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:22 name:@"西施"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:23 name:@"比熊"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:24 name:@"八哥"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:25 name:@"哥基"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:26 name:@"唐狗"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:27 name:@"鬆獅"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:28 name:@"薩摩耶"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:29 name:@"芝娃娃"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:30 name:@"史納莎"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:31 name:@"聖班納"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:32 name:@"北京狗"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:33 name:@"魔天使"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:34 name:@"臘腸狗"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:35 name:@"蝴蝶犬"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:36 name:@"玩具貴婦"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:37 name:@"英國曲架"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:38 name:@"拉布拉多"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:39 name:@"愛斯基摩"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:40 name:@"約瑟爹利"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:41 name:@"邊界牧羊犬"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:42 name:@"金毛尋回犬"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:43 name:@"古代牧羊犬"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:44 name:@"德國牧羊犬"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:45 name:@"小型牧羊犬"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:46 name:@"哈士奇（雪橇）"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:47 name:@"西高地白爹利"]];
    [dogs addObject:[OPConstantPetType petTypeWithId:48 name:@"其他"]];
    [self.petCategories addObject:[OPConstantPetCategory petCategoryWithName:@"狗" types:dogs]];
    
    self.postCategories = [NSMutableArray array];
    [self.postCategories addObject:[OPConstantPostCategory postCategoryWithId:1]];
    [self.postCategories addObject:[OPConstantPostCategory postCategoryWithId:7]];
}

- (OPConstantPetCategory *)petCategoryForPetTypeId:(NSInteger)pet_type {
    for(OPConstantPetCategory *cat in self.petCategories) {
        for(OPConstantPetType *type in cat.types) {
            if(type.id == pet_type) {
                return cat;
            }
        }
    }
    
    return nil;
}

- (OPConstantPetType *)petTypeForPetTypeId:(NSInteger)pet_type {
    for(OPConstantPetCategory *cat in self.petCategories) {
        for(OPConstantPetType *type in cat.types) {
            if(type.id == pet_type) {
                return type;
            }
        }
    }
    
    return nil;
}

@end

@implementation OPConstant

@end

@implementation OPConstantPostCategory

+(id)postCategoryWithId:(NSInteger)id {
    OPConstantPostCategory *cat = [[OPConstantPostCategory alloc]init];
    cat.id = id;
    return cat;
}

- (NSString *)name {
    NSString *key = [NSString stringWithFormat:@"post_category_name_%@",@(self.id)];
    return NSLocalizedString(key,nil);
}

@end

@implementation OPConstantPetCategory

+(id)petCategoryWithName:(NSString *)name types:(NSArray *)types {
    OPConstantPetCategory *cat =  [[OPConstantPetCategory alloc]init];
    cat.name = name;
    cat.types = types;
    return cat;
}

@end

@implementation OPConstantPetType

+(id)petTypeWithId:(NSInteger)id name:(NSString *)name {
    OPConstantPetType *type = [[OPConstantPetType alloc]init];
    type.id = id;
    type.name = name;
    return type;
}



@end

