//
//  OPProfileService.m
//  operapet
//
//  Created by Gary KK on 15/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPProfileService.h"

@implementation OPProfileService

- (void)getProfile:(HTModelResponseHandler *)handler {
    NSString *path = [self pathing:@"profileapi", nil];
    HTModelParser *parser = [HTModelParser model:[OPProfile class]];
    [[OPServiceManager sharedInstance]get:path parser:parser handler:handler];
}

- (void)getProfile:(HTModelResponseHandler *)handler member_id:(NSInteger)member_id {
    NSString *path = [self pathing:@"profileapi/{member_id}", int(member_id), nil];
    HTModelParser *parser = [HTModelParser model:[OPProfile class]];
    [[OPServiceManager sharedInstance]get:path parser:parser handler:handler];
}

- (void)getAllPet:(HTListResponseHandler *)handler member_id:(NSInteger)member_id {
    NSString *path = [self pathing:@"allpetapi/{member_id}", int(member_id), nil];
    HTArrayParser *parser = [HTArrayParser model:[OPPet class]];
    [[OPServiceManager sharedInstance]get:path parser:parser handler:handler];
}

- (void)getGiftHistory:(HTListResponseHandler *)handler {
    NSString *path = [self pathing:@"gifthistoryapi", nil];
    HTArrayParser *parser = [HTArrayParser model:[OPGift class]];
    [[OPServiceManager sharedInstance]get:path parser:parser handler:handler];
}

- (void)getNotification:(HTListResponseHandler *)handler member_id:(NSInteger)member_id{
    NSString *path = [self pathing:@"GetNotificationListApi/{member_id}", int(member_id), nil];
    HTArrayParser *parser = [HTArrayParser model:[OPNotification class]];
    [[OPServiceManager sharedInstance]get:path parser:parser handler:handler];
}

- (void)postDeviceId:(HTModelResponseHandler *)handler device_id:(NSString *)device_id {
    NSString *path = [self pathing:@"UpdateDeviceTokenApi", nil];
    HTModelParser *parser = [HTModelParser model:[HTBaseModel class]];
    [[OPServiceManager sharedInstance]post:path headers:[OPServiceManager sharedInstance].requestHeaders
                                     parts:@{@"device_id":device_id, @"device_type": int(1)}
                                 fileparts:@[] parser:parser handler:handler];
}

@end
