//
//  OPMemberService.h
//  operapet
//
//  Created by Gary KK on 14/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "HTBaseService.h"
#import "OPProfile.h"
#import "OPActionHistory.h"

@interface OPMemberService : HTBaseService

-(void)postNormalLogin:(HTModelResponseHandler *)handler mobile_number:(NSString *)mobile_number verify_code:(NSString *)verify_code;
-(void)postNormalLoginSendSms:(HTModelResponseHandler *)handler mobile_number:(NSString *)mobile_number;
-(void)postCreateMember:(HTModelResponseHandler *)handler device_id:(NSString *)device_id device_type:(NSString *)device_type
              mobile_no:(NSString *)mobile_no lastname:(NSString *)lastname firstname:(NSString *)firstname email:(NSString *)email;
-(void)postCreateFbMember:(HTModelResponseHandler *)handler device_id:(NSString *)device_id device_type:(NSString *)device_type
                mobile_no:(NSString *)mobile_no lastname:(NSString *)lastname firstname:(NSString *)firstname email:(NSString *)email
              facebook_id:(NSString *)facebook_id facebook_token:(NSString *)facebook_token;

-(void)postRequestFriend:(HTModelResponseHandler *)handler target_member_id:(NSInteger)target_member_id;
-(void)postAcceptFriend:(HTModelResponseHandler *)handler target_member_id:(NSInteger)target_member_id;
-(void)postDeleteFriend:(HTModelResponseHandler *)handler friend_relationship_id:(NSInteger)friend_relationship_id;
-(void)getFriendList:(HTListResponseHandler *)handler target_member_id:(NSInteger)target_member_id;

-(void)postUpdateMember:(HTModelResponseHandler *)handler
          profile_image:(UIImage *)profile_image
              device_id:(NSString *)device_id
            device_type:(NSString *)device_type
              mobile_no:(NSString *)mobile_no
               lastname:(NSString *)lastname
              firstname:(NSString *)firstname
                  email:(NSString *)email;

-(void)getFriendRequestList:(HTListResponseHandler *)handler;
-(void)postGetSearchFriendResult:(HTListResponseHandler *)handler search_key:(NSString *)search_key;

-(void)postReferalFriendSendSms:(HTModelResponseHandler *)handler phone_list_csv:(NSString *)phone_list_csv;
-(void)getActionHistory:(HTListResponseHandler *)handler;

-(void)postIsFbExist:(HTModelResponseHandler *)handler facebook_id:(NSString *)facebook_id;
-(void)postFbLoginSendSmsApi:(HTModelResponseHandler *)handler facebook_id:(NSString *)facebook_id;
-(void)postFbLogin:(HTModelResponseHandler *)handler facebook_id:(NSString *)facebook_id verify_code:(NSString *)verify_code;

@end
