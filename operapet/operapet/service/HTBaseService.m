//
//  HTBaseService.m
//  myislands
//
//  Created by alan tsoi on 30/8/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "HTBaseService.h"
#import "JSONModelLib.h"

@implementation HTBaseService

-(NSString *)pathing:(NSString *)path, ... {
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\{.*?\\}" options:0 error:nil];
    NSArray *matches = [regex matchesInString:path options:0 range:NSMakeRange(0, path.length)];
    NSMutableArray *paras = [NSMutableArray array];
    
    va_list args;
    
    id arg = nil;
    va_start(args, path);
    while ((arg = va_arg(args,id))) {
        [paras addObject:arg];
    }
    va_end(args);
    
    NSInteger it = matches.count;
    for (NSTextCheckingResult *match in [matches reverseObjectEnumerator]) {
        --it;
        NSString *para = (it >= 0 && it < paras.count) ? paras[it] : @"";
        NSString * encoded = [para stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
        NSRange range = [match range];
        path = [path stringByReplacingCharactersInRange:range withString:encoded];
    }
    
    return path;
}



@end
