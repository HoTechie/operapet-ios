//
//  OPLoginViewController.m
//  operapet
//
//  Created by Gary KK on 6/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPLoginViewController.h"
#import "OPMainViewController.h"
#import "OPLoginOptionViewController.h"
#import "OPLoginPhoneVerifyViewController.h"
#import "OPRegistrationViewController.h"
#import "OPLoginVerifySMSViewController.h"
#import "OPLoginSuccessViewController.h"


@interface OPLoginViewController ()
@property OPViewController *childViewController;
@end

@implementation OPLoginViewController

-(id)initWithChildViewController:(OPViewController *)childViewController {
    self = [super init];
    self.childViewController = childViewController;
    [self addChildViewController:self.childViewController];
    return self;
}

- (void)showBackButton {
    self.btnBack.hidden = NO;
    [self.btnBack addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (BOOL)navigationBarHidden {
    return YES;
}

- (void)configUI {
    [super configUI];
    
    [self.scrollView addSubview:self.childViewController.view];
    [self constrainView:self.childViewController.view inScrollView:self.scrollView];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.scrollViewHeightConstraint.constant = [self.childViewController.view systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getNibName {
    return @"OPLoginViewController";
}

+(OPViewController *)loginOptionViewController {
    return [[self alloc]initWithChildViewController:[[OPLoginOptionViewController alloc]init]];
}

+(OPViewController *)loginPhoneVerifyViewController {
    return [[self alloc]initWithChildViewController:[[OPLoginPhoneVerifyViewController alloc]init]];
}

+(OPViewController *)registrationViewController {
    return [[self alloc]initWithChildViewController:[[OPRegistrationViewController alloc]init]];
}

+(OPViewController *)registrationViewControllerWithFacebookId:(NSString *)facebook_id token:(NSString *)token {
    return [[self alloc]initWithChildViewController:[[OPRegistrationViewController alloc]initWithFacebookId:facebook_id facebookToken:token]];
}

+(OPViewController *)loginVerifySMSViewControllerWithPhoneNumber:(NSString *)phoneNumber {
    return [[self alloc]initWithChildViewController:[[OPLoginVerifySMSViewController alloc]initWithPhoneNumber:phoneNumber]];
}

+(OPViewController *)loginVerifySMSViewControllerWithFacebookId:(NSString *)facebook_id {
    return [[self alloc]initWithChildViewController:[[OPLoginVerifySMSViewController alloc]initWithFacebookId:facebook_id]];
}

+(OPViewController *)loginSuccessViewController {
    return [[self alloc]initWithChildViewController:[[OPLoginSuccessViewController alloc]init]];
}

- (NSLayoutConstraint *)constraintBottomSpace {
    return self.scrollViewBottomSpace;
}

- (void)hideKeyboard:(UITextField *)txtFld
{
    [txtFld resignFirstResponder];
    [self keyboardDidHide];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self hideKeyboard:textField];
    
    if([self.childViewController respondsToSelector:@selector(textFieldShouldReturn:)]) {
        return [(id <UITextFieldDelegate>)self.childViewController textFieldShouldReturn:textField];
    }
    
    return NO;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
