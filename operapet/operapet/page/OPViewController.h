//
//  OPViewController.h
//  operapet
//
//  Created by Gary KK on 6/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//



#import "BaseViewController.h"
#import "OPNavigationController.h"

@class OPPost;
@class OPComment;

@interface OPViewController : BaseViewController

- (void)showMenuButton;

@property BOOL isKeyboardVisible;
-(NSLayoutConstraint *)constraintBottomSpace;

- (void)keyboardOnScreen:(NSNotification*) notification;
- (void)keyboardDidHide;
- (void)hideKeyboard:(UITextField *)txtFld;
- (BOOL)navigationBarHidden;

@property void(^onLike)(OPPost *post);
@property void(^onComment)(OPPost *post);
@property void(^onShare)(OPPost *post);
@property void(^onLikeComment)(OPComment *comment);

@end
