//
//  OPMainViewController.m
//  operapet
//
//  Created by Gary KK on 6/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPMainViewController.h"

@interface OPMainViewController ()

@end

@implementation OPMainViewController

+ (instancetype)sharedInstance {
    static id sharedInstance = NULL;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    self = [super init];
    
    
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.rootViewCoverColorForLeftView = [UIColor colorWithWhite:0.0 alpha:0.5];
    
    [self setLeftViewEnabledWithWidth:250.f
                    presentationStyle:LGSideMenuPresentationStyleSlideBelow
                 alwaysVisibleOptions:LGSideMenuAlwaysVisibleOnNone];
    
    self.leftViewStatusBarStyle = UIStatusBarStyleDefault;
    self.leftViewStatusBarVisibleOptions = LGSideMenuStatusBarVisibleOnNone;
    self.leftViewAnimationSpeed = 0.0;
    self.leftViewSwipeGestureEnabled = NO;
    
    self.leftViewBackgroundColor = [UIColor redColor];
    
    /* ----- */
    
//    [self setRightViewEnabledWithWidth:100.f
//                     presentationStyle:LGSideMenuPresentationStyleSlideBelow
//                  alwaysVisibleOptions:LGSideMenuAlwaysVisibleOnNone];
//    
//    self.rightViewStatusBarStyle = UIStatusBarStyleDefault;
//    self.rightViewStatusBarVisibleOptions = LGSideMenuStatusBarVisibleOnNone;
    
    self.menuViewController = [[OPMenuViewController alloc]init];
    
    [self addChildViewController:self.menuViewController];
    [self.leftView addSubview:self.menuViewController.view];
    [self constrainView:self.menuViewController.view equalView:self.leftView];
}

-(void)constrainView:(UIView *) view equalView:(UIView *)view2{
    [view setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *con1 = [NSLayoutConstraint constraintWithItem:view2 attribute:NSLayoutAttributeLeading relatedBy:0 toItem:view attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
    NSLayoutConstraint *con2 = [NSLayoutConstraint constraintWithItem:view2 attribute:NSLayoutAttributeTrailing relatedBy:0 toItem:view attribute:NSLayoutAttributeTrailing multiplier:1 constant:0];
    NSLayoutConstraint *con3 = [NSLayoutConstraint constraintWithItem:view2 attribute:NSLayoutAttributeTop relatedBy:0 toItem:view attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    NSLayoutConstraint *con4 = [NSLayoutConstraint constraintWithItem:view2 attribute:NSLayoutAttributeBottom relatedBy:0 toItem:view attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    NSArray *constraints = @[con1,con2,con3,con4];
    [view2 addConstraints:constraints];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)openLeftMenu {
    [self.menuViewController reload];
    [self showLeftViewAnimated:NO completionHandler:nil];
}

- (void)closeLeftMenu {
    [self hideLeftViewAnimated:NO completionHandler:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
