//
//  OPLoginViewController.h
//  operapet
//
//  Created by Gary KK on 6/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPViewController.h"
#import "OPMemberService.h"

@interface OPLoginViewController : OPViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imgLoginBackground;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *scrollViewHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *scrollViewBottomSpace;


+(OPViewController *)loginOptionViewController;
+(OPViewController *)loginPhoneVerifyViewController;
+(OPViewController *)registrationViewController;
+(OPViewController *)registrationViewControllerWithFacebookId:(NSString *)facebook_id token:(NSString *)token;
+(OPViewController *)loginVerifySMSViewControllerWithPhoneNumber:(NSString *)phoneNumber;
+(OPViewController *)loginVerifySMSViewControllerWithFacebookId:(NSString *)facebook_id;
+(OPViewController *)loginSuccessViewController;

@property BOOL isKeyboardVisible;
@property CGSize savedScrollContentSize;
@property CGSize savedLogoHeightRatio;


@end
