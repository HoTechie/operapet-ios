//
//  OPMainViewController.h
//  operapet
//
//  Created by Gary KK on 6/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <LGSideMenuController/LGSideMenuController.h>
#import "OPMenuViewController.h"

@interface OPMainViewController : LGSideMenuController

+ (instancetype)sharedInstance;

@property OPMenuViewController *menuViewController;

- (void)openLeftMenu;
- (void)closeLeftMenu;

@end
