//
//  OPViewController.m
//  operapet
//
//  Created by Gary KK on 6/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import "OPViewController.h"
#import "OPMainViewController.h"

#import "OPPostDetailViewController.h"
#import "OPPostsService.h"

@interface OPViewController ()

@end

@implementation OPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configUI {
    [super configUI];
    
    self.navigationController.navigationBar.barTintColor  = color_main_bg_color;
    [self.view setBackgroundColor:color_main_bg_color];
    
    __weak OPViewController *_self = self;
    self.onLike = ^(OPPost *post){
        [[[OPPostsService alloc]init]postCreateLike:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
            post.is_like = !post.is_like;
            if(post.is_like) post.no_of_like++;
            else post.no_of_like--;
            [_self updateUI];
        } onFailure:^(NSInteger code, NSString *str) {
            
        }] post_id:post.id];
    };
    self.onShare = ^(OPPost *post) {
        [[[OPPostsService alloc]init]postCreateShare:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
            [_self showMessageDialog:ls(@"post_share_success") buttonPositive:ls(@"msg_ok") positiveHandler:^(MessageDialogAlertController *ac, UIAlertAction *action) {}];
        } onFailure:^(NSInteger code, NSString *str) {
            
        }] post_id:post.id];
    };
    self.onComment = ^(OPPost *post){
        [[OPNavigationController sharedInstance]pushViewController:[[OPPostDetailViewController alloc]initWithPostId:post.id] animated:YES];
    };
    self.onLikeComment = ^(OPComment *comment){
        [[[OPPostsService alloc]init]postCreateCommentLike:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
            comment.is_like = !comment.is_like;
            if(comment.is_like) comment.no_of_like++;
            else comment.no_of_like--;
            
            [_self updateUI];
        } onFailure:^(NSInteger code, NSString *str) {
            
        }] comment_id:comment.id];
    };
}

- (void)showBackButton {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *btnImg = [[UIImage imageNamed:@"btn_back"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    CGRect lframe = CGRectMake(0, 0, 50, 50);
    button.contentMode = UIViewContentModeScaleAspectFit;
    button.frame = lframe;
    [button setTintColor:[UIColor lightGrayColor]];
    [button setBackgroundImage:btnImg forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
}

- (void)showMenuButton {
    [self showLeftNavBarButtonWithImageNamed:@"btn_menu" withSize:CGSizeMake(55, 55)];
}

- (void)leftNavBarButtonPressed {
    [[OPMainViewController sharedInstance]openLeftMenu];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:[self navigationBarHidden] animated:animated];
    
    NSNotificationCenter* center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(keyboardDidHide) name:UIKeyboardDidHideNotification object:nil];
    [center addObserver:self selector:@selector(keyboardOnScreen:) name:UIKeyboardDidChangeFrameNotification object:nil];
}

- (BOOL)navigationBarHidden {
    return NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (NSLayoutConstraint *)constraintBottomSpace {
    return nil;
}

- (void)keyboardOnScreen:(NSNotification*) notification{
    NSDictionary * userInfo = notification.userInfo;
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGFloat keyboardHeight = MIN(keyboardSize.width, keyboardSize.height);
    
    if(self.constraintBottomSpace) {
        [UIView animateWithDuration:0.5
                         animations:^{
                             self.constraintBottomSpace.constant =  keyboardHeight;
                             [self.view layoutIfNeeded];
                         }];
    }
    
    self.isKeyboardVisible = true;
}

- (void)keyboardDidHide {
    if(self.isKeyboardVisible) {
        [UIView animateWithDuration:0.5
                         animations:^{
                             self.constraintBottomSpace.constant = 0;
                             [self.view layoutIfNeeded];
                         }];
    }
    
    self.isKeyboardVisible = false;
}

- (void)hideKeyboard:(UITextField *)txtFld
{
    [txtFld resignFirstResponder];
    [self keyboardDidHide];
}

- (void)popViewController {
    [[OPNavigationController sharedInstance]popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
