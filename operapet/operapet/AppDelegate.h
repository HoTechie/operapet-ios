//
//  AppDelegate.h
//  operapet
//
//  Created by Gary KK on 6/10/2016.
//  Copyright © 2016 Hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

