//
//  SessionManager.m
//  myislands
//
//  Created by alan tsoi on 3/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "SessionManager.h"
#import "Constants.h"

#import "BaseNavigationController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "UIImageView+WebCache.h"

#import "OPProfileService.h"

#define SESSION_PROFILE @"SESSION_PROFILE"
#define COUPONS_DICT @"COUPONS_DICT"

@interface SessionManager()
@property FBSDKLoginManager *fbLoginManager;
@end

@implementation SessionManager

+ (instancetype)sharedInstance {
    static id sharedInstance = NULL;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    self = [super init];
    NSData *encodedProfile = [[NSUserDefaults standardUserDefaults]objectForKey:SESSION_PROFILE];
    if(encodedProfile) self.currentProfile = [NSKeyedUnarchiver unarchiveObjectWithData:encodedProfile];
    
    [[SDImageCache sharedImageCache]setShouldDecompressImages:NO];
    [[SDWebImageDownloader sharedDownloader]setShouldDecompressImages:NO];
    
    return self;
}

- (void)endSession {
    self.currentProfile = nil;
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:SESSION_PROFILE];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

- (void)setCurrentProfile:(OPProfile *)currentProfile {
    currentProfile.token = _currentProfile.token;
    _currentProfile = currentProfile;
    NSData *encodedProfile = [NSKeyedArchiver archivedDataWithRootObject:currentProfile];
    [[NSUserDefaults standardUserDefaults]setObject:encodedProfile forKey:SESSION_PROFILE];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self trySubmitDeviceId];
}

- (void)setDevice_id:(NSString *)device_id {
    _device_id = device_id;
    [self trySubmitDeviceId];
}

- (void)trySubmitDeviceId {
    if(self.currentProfile && self.device_id) {
        [[[OPProfileService alloc]init]postDeviceId:[[HTModelResponseHandler alloc]initWithOnResult:^(id object) {
            
        } onFailure:^(NSInteger code, NSString *str) {
            
        }] device_id:self.device_id];
    }
}

- (MILanguage)defaultLanguage {
    NSString *preferredLangId = [[[NSBundle mainBundle]preferredLocalizations]objectAtIndex:0];
    
    if([preferredLangId isEqualToString:@"en"]) {
        return MILanguageEnglish;
    } else if([preferredLangId isEqualToString:@"zh-Hant"]) {
        return MILanguageChineseTraditional;
    } else if([preferredLangId isEqualToString:@"zh-Hans"]) {
        return MILanguageChineseSimpified;
    }
    
    return MILanguageEnglish;
}

- (NSString *)languageCodeForApp {
    if(self.language == MILanguageEnglish) {
        return @"Base";
    } else if(self.language == MILanguageChineseTraditional) {
        return @"zh-Hant";
    } else if(self.language == MILanguageChineseSimpified) {
        return @"zh-Hans";
    }
    
    return @"Base";
}

- (NSString *)languageCodeForAPI {
    NSString *langId = [self languageCodeForApp];
    
    if([langId isEqualToString:@"Base"]) {
        return @"en";
    } else if([langId isEqualToString:@"zh-Hant"]) {
        return @"zh_HK";
    } else if([langId isEqualToString:@"zh-Hans"]) {
        return @"zh_CN";
    }
    
    return @"en";
}

- (void)setLanguage:(MILanguage)language {
    _language = language;
    [[NSUserDefaults standardUserDefaults]setObject:[@(language) stringValue] forKey:@"PREFERRED_LANG"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

- (NSString *)localizabeString:(NSString *)string{
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"Localizable" ofType:@"strings" inDirectory:nil forLocalization:[self languageCodeForApp]];
    NSBundle *dataBundle = [[NSBundle alloc] initWithPath:[bundlePath stringByDeletingLastPathComponent]];
    return NSLocalizedStringFromTableInBundle(string, @"Localizable", dataBundle, nil);
}

- (NSString *)currentLanguageName {
    return [self languageNames][@(self.language)];
}

- (NSDictionary *)languageNames {
    return @{@(MILanguageEnglish):ls(@"lang_en_display"),
             @(MILanguageChineseTraditional):ls(@"lang_zh_hk_display"),
             @(MILanguageChineseSimpified):ls(@"lang_zh_cn_display")};
}

- (void)loginWithFacebookWithSuccess:(FbActionResult)onSuccess error:(ActionResult)onError {
    self.fbLoginManager = self.fbLoginManager ? self.fbLoginManager : [[FBSDKLoginManager alloc] init];
    self.fbLoginManager.loginBehavior = FBSDKLoginBehaviorWeb;
    
    [self.fbLoginManager logOut];
    [self.fbLoginManager
     logInWithReadPermissions: @[@"public_profile", @"email"]
     fromViewController:[BaseNavigationController sharedInstance].topViewController
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             if(onError) onError(@"");
         } else if (result.isCancelled) {
             NSLog(@"[FbLogin] Cancelled");
         } else {
             NSLog(@"[FbLogin] OAuth token got with FB ID: %@", [FBSDKAccessToken currentAccessToken].userID);
             if(onSuccess) onSuccess([FBSDKAccessToken currentAccessToken].userID, [[FBSDKAccessToken currentAccessToken]tokenString]);
         }
     }];
}

- (void)retrieveFacebookUser:(FbGraphResult)onResult {
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"id,first_name,last_name,email,picture.type(large)" forKey:@"fields"];
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                  id result, NSError *error) {
         
         if(!error) {
             HTFacebookUser *user = [[HTFacebookUser alloc]initWithDictionary:result error:nil];
             if(onResult) onResult(YES, user);
         } else {
             if(onResult) onResult(NO, nil);
         }
     }];
}

- (NSArray *)postCategories {
    NSMutableArray *cats = [NSMutableArray array];
    OPPostCategory *category;
    
    category = [OPPostCategory category];
    category.id = 0;
    category.color = [UIColor blackColor];
    category.colorDarker = [UIColor blackColor];
    category.iconFileName = nil;
    category.name = ls(@"post_category_name_0");
    [cats addObject:category];
    
    category = [OPPostCategory category];
    category.id = 1;
    category.color = color_theme_color;
    category.colorDarker = color_theme_color_darker;
    category.iconFileName = @"icon_cat_1";
    category.name = ls(@"post_category_name_1");
    [cats addObject:category];
    
    category = [OPPostCategory category];
    category.id = 2;
    category.color = color_theme_color_2;
    category.colorDarker = color_theme_color_2_darker;
    category.iconFileName = @"icon_cat_2";
    category.name = ls(@"post_category_name_2");
    [cats addObject:category];
    
    category = [OPPostCategory category];
    category.id = 3;
    category.color = color_theme_color_3;
    category.colorDarker = color_theme_color_3_darker;
    category.iconFileName = @"icon_cat_3";
    category.name = ls(@"post_category_name_3");
    [cats addObject:category];
    
    category = [OPPostCategory category];
    category.id = 4;
    category.color = color_theme_color_4;
    category.colorDarker = color_theme_color_4_darker;
    category.iconFileName = @"icon_cat_4";
    category.name = ls(@"post_category_name_4");
    [cats addObject:category];
    
    category = [OPPostCategory category];
    category.id = 5;
    category.color = color_theme_color_5;
    category.colorDarker = color_theme_color_5_darker;
    category.iconFileName = @"icon_cat_5";
    category.name = ls(@"post_category_name_5");
    [cats addObject:category];
    
    category = [OPPostCategory category];
    category.id = 6;
    category.color = color_theme_color_6;
    category.colorDarker = color_theme_color_6_darker;
    category.iconFileName = @"icon_cat_6";
    category.name = ls(@"post_category_name_6");
    [cats addObject:category];
    
    category = [OPPostCategory category];
    category.id = 7;
    category.color = color_theme_color_7;
    category.colorDarker = color_theme_color_7_darker;
    category.iconFileName = @"icon_cat_7";
    category.name = ls(@"post_category_name_7");
    [cats addObject:category];
    
    category = [OPPostCategory category];
    category.id = 8;
    category.color = color_theme_color_8;
    category.colorDarker = color_theme_color_8_darker;
    category.iconFileName = @"icon_cat_8";
    category.name = ls(@"post_category_name_8");
    [cats addObject:category];
    
    category = [OPPostCategory category];
    category.id = 9;
    category.color = color_theme_color_9;
    category.colorDarker = color_theme_color_9_darker;
    category.iconFileName = @"icon_cat_9";
    category.name = ls(@"post_category_name_9");
    [cats addObject:category];
    
    return cats;
}

- (OPPostCategory *)postCategoryForId:(NSInteger)id {
    NSArray *cats = [self postCategories];
    if(id < cats.count) return cats[id];
    return cats[0];
}

- (NSString *)memberLevelString:(NSInteger)level {
    NSString *level_str = [NSString stringWithFormat:@"member_level_%@", [@(level) stringValue]];
    return level <= 3 ? ls(level_str) : @"";
}

- (NSString *)formattedDateString:(NSString *)input {
    NSDateFormatter *inFormat = [[NSDateFormatter alloc]init];
    [inFormat setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *date = [inFormat dateFromString:input];
    NSDateFormatter *outFormat = [[NSDateFormatter alloc]init];
    [outFormat setDateFormat:@"yyyy年M月d日"];
    NSString *outDate = [outFormat stringFromDate:date];
    return outDate;
}



@end
