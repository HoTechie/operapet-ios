//
//  BaseViewController.h
//  megaman
//
//  Created by Duncan Wong on 12/7/2016.
//  Copyright © 2016 LiSeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageDialogAlertController.h"
#import "ListDialogAlertController.h"
#import "BaseNavigationController.h"

#import "SessionManager.h"
#import "Constants.h"



@interface BaseViewController : UIViewController

- (NSString*)getNibName;
- (NSString*)getTitle;
- (NSArray *)getRightBarButtonItems;

- (void)configUI;
- (void)updateUI;
- (void)updateData:(BOOL) forceUpdate;

- (void)showBackButton;
- (void)backButtonPressed;
- (NSString *)backButtonImageName;
- (CGSize)backButtonSize;

- (void)showLeftNavBarButtonWithImageNamed:(NSString *)imgName withSize:(CGSize)size;
- (void)leftNavBarButtonPressed;

- (void)showProgressHUD;
- (void)hideProgressHUD;
- (void)popViewController;

- (void)stylizeTextField:(UITextField *)txtFld;

- (void)showMessageDialog:(NSString*)message
           buttonPositive:(NSString*)btnPositive
           buttonNegative:(NSString*)btnNevigative
          positiveHandler:(MessageDialogPositiveHandler) positiveHandler
          negativeHandler:(MessageDialogNegativeHandler) negativeHandler;

- (void)showInputDialog:(NSString *)message
           keyboardType:(UIKeyboardType)keyboardType
           inputHandler:(MessageDialogInputHandler) inputHandler
      nevigativeHandler:(MessageDialogNegativeHandler) negativeHandler;

- (void)showInputDialog:(NSString *)message
           inputHandler:(MessageDialogInputHandler) inputHandler
      nevigativeHandler:(MessageDialogNegativeHandler) negativeHandler;

- (void)showInputDialog:(NSString *)message
      inputPlaceHolders:(NSArray *)placeholders
          inputsHandler:(MessageDialogInputsHandler)inputsHandler
      nevigativeHandler:(MessageDialogNegativeHandler)negativeHandler;

- (void)showSelectionWithTitle:(NSString *)title
                       message:(NSString *)msg
                          btns:(NSArray*)btns
                actionHandlers:(NSArray*)actionHandleBlocks;

- (void)showMessageDialog:(NSString*)message
           buttonPositive:(NSString*)btnPositive
          positiveHandler:(MessageDialogPositiveHandler) positiveHandleBlock;


- (UIBarButtonItem *)barButtonItemWithTarget:(id)target
                                    selector:(SEL)selector
                                   imageName:(NSString *)imageName
                                        size:(CGSize)size;

-(void)constrainView:(UIView *) view equalView:(UIView *)view2;
-(void)constrainView:(UIView *) view inScrollView:(UIView *)scrollView;


@end
