//
//  MINavigationBar.m
//  myislands
//
//  Created by alan tsoi on 29/8/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "BaseNavigationController.h"
#import "MINavigationBar.h"
#import "Constants.h"

@implementation MINavigationBar

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    UIToolbar *secToolbar = [self createSecondaryToolbar];
    [self createPrimaryLabels:secToolbar];
    
    return self;
}

-(void) setPrimaryLeftTitle:(NSString *)title {
    [self.primaryLeftButton setTitle:title forState:UIControlStateNormal];
}

-(void) setPrimaryRightTitle:(NSString *)title {
    [self.primaryRightButton setTitle:title forState:UIControlStateNormal];
}

-(void) setSecondaryLeftTitle:(NSString *)title {
    [self.secondaryLeftBarButton setTitle:title];
}

-(void) setSecondaryRightTitle:(NSString *)title {
    [self.secondaryRightBarButton setTitle:title];
}

-(void) createPrimaryLabels:(UIToolbar *)toolbar
{
    self.primaryLeftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.primaryRightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.primaryLeftButton.titleLabel setFont:[UIFont fontWithName:@"Lucida Sans Unicode" size:24]];
    [self.primaryRightButton.titleLabel setFont:[UIFont fontWithName:@"DFKai-SB" size:40]];
    
    [self.primaryLeftButton setTitleColor:action_bar_text forState:UIControlStateNormal];
    [self.primaryRightButton setTitleColor:action_bar_text forState:UIControlStateNormal];
    
    [self addSubview:self.primaryLeftButton];
    [self.primaryLeftButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.primaryLeftButton attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:dimen_nav_padding]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.primaryLeftButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:toolbar attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    
    [self addSubview:self.primaryRightButton];
    [self.primaryRightButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.primaryRightButton attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:-dimen_nav_padding]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.primaryRightButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:toolbar attribute:NSLayoutAttributeTop multiplier:1.0 constant:-5.0]];
    
    [self.primaryLeftButton addTarget:self action:@selector(onClickPrimaryButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.primaryRightButton addTarget:self action:@selector(onClickPrimaryButton:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)onClickPrimaryButton:(id)sender {
    [[BaseNavigationController sharedInstance]closeMenuWithCompletion:nil];
    [[BaseNavigationController sharedInstance]returnToIsland];
}

-(void)onSecondaryLeftBarButtonPressed:(id)sender {
    [[BaseNavigationController sharedInstance]onSecondaryLeftBarButtonPressed];
}

-(void)onSecondaryRightBarButtonPressed:(id)sender {
    [[BaseNavigationController sharedInstance]onSecondaryRightBarButtonPressed];
}

-(UIToolbar *)createSecondaryToolbar
{
    UIToolbar *toolbar;
    toolbar = [[UIToolbar alloc]initWithFrame:CGRectZero];
    //    [toolbar setBackgroundColor:[UIColor clearColor]];
    [toolbar setBackgroundImage:[[UIImage alloc]init] forToolbarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    [toolbar setBarTintColor:[UIColor clearColor]];
    [toolbar setTintColor:[UIColor clearColor]];
    [toolbar setTranslucent:YES];
    
    self.secondaryLeftBarButton = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(onSecondaryLeftBarButtonPressed:)];
    self.secondaryRightBarButton = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(onSecondaryRightBarButtonPressed:)];
    
    [self.secondaryLeftBarButton setTintColor:[UIColor whiteColor]];
    [self.secondaryRightBarButton setTintColor:main_text_color];
    [self.secondaryLeftBarButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Lucida Sans Unicode" size:14.0]} forState:UIControlStateNormal];
    [self.secondaryRightBarButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Lucida Sans Unicode" size:14.0]} forState:UIControlStateNormal];
    
    UIBarButtonItem *negativeSeparator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSeparator.width = 0;
    [toolbar setItems:[NSArray arrayWithObjects:
                       negativeSeparator,
                       self.secondaryLeftBarButton,
                       [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                       self.secondaryRightBarButton,
                       negativeSeparator,
                       nil] animated:NO];
    
    [self addSubview:toolbar];
    [toolbar setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:toolbar attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:toolbar attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:toolbar attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:toolbar attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:dimen_action_bar_height]];
    
    // Background
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[[UIImage imageNamed:@"action_bar_2_bg"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [self insertSubview:imgView belowSubview:toolbar];
    [imgView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:imgView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:imgView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:imgView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:imgView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:dimen_action_bar_height]];
    
    
    return toolbar;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

//| ----------------------------------------------------------------------------
//  A navigation bar subclass should override -sizeThatFits: to pad the fitting
//  height for the navigation bar, creating space for the extra elements that
//  will be added.  UINavigationController calls this method to retrieve the
//  size of the navigation bar, which it then uses when computing the bar's
//  frame.
//
- (CGSize)sizeThatFits:(CGSize)size
{
    CGSize navigationBarSize = [super sizeThatFits:size];
    
    // Pad the base navigation bar height by the fitting height of our button.
    //    CGSize buttonSize = [self.customButton sizeThatFits:CGSizeMake(size.width, 0)];
    navigationBarSize.height += 30;
    
    return navigationBarSize;
}


//| ----------------------------------------------------------------------------
- (void)layoutSubviews
{
    // UINavigationBar positions its elements along the bottom edge of the
    // bar's bounds.  This allows our subclass to position our custom elements
    // at the top of the navigation bar, in the extra space we created by
    // padding the height returned from -sizeThatFits:
    [super layoutSubviews];
    
    // NOTE: You do not need to account for the status bar height in your
    //       layout.  The navigation bar is positioned just below the
    //       status bar by the navigation controller.
    
    // Retrieve the button's fitting height and position the button along the
    // top edge of the navigation bar.  The button is sized to the full
    // width of the navigation bar as it will automatically center its contents.
    //    CGSize buttonSize = [self.customButton sizeThatFits:CGSizeMake(self.bounds.size.width, 0)];
    //    self.customButton.frame = CGRectMake(0, 0, self.bounds.size.width, buttonSize.height);
}


//| ----------------------------------------------------------------------------
//  Custom implementation of the setter for the customButton property.
//
- (void)setCustomButton:(UIButton *)customButton
{
    // Remove the previous button
    //    [_customButton removeFromSuperview];
    //    _customButton = customButton;
    //
    //    [self addSubview:customButton];
    //
    //    // Force our -sizeThatFits: method to be called again and flag the
    //    // navigation bar as needing layout.
    //    [self invalidateIntrinsicContentSize];
    //    [self setNeedsLayout];
}


@end
