//
//  MINavigationBar.h
//  myislands
//
//  Created by alan tsoi on 29/8/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MINavigationBar : UINavigationBar


@property UIButton *primaryLeftButton;
@property UIButton *primaryRightButton;
@property UIBarButtonItem* secondaryLeftBarButton;
@property UIBarButtonItem* secondaryRightBarButton;

-(void) setPrimaryLeftTitle:(NSString *)title;
-(void) setPrimaryRightTitle:(NSString *)title;
-(void) setSecondaryLeftTitle:(NSString *)title;
-(void) setSecondaryRightTitle:(NSString *)title;


@end
