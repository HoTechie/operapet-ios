//
//  BaseNavigationController.h
//  megaman
//
//  Created by Duncan Wong on 12/7/2016.
//  Copyright © 2016 LiSeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MINavigationBar.h"


@interface BaseNavigationController : UINavigationController
+ (instancetype)sharedInstance;

@property MINavigationBar *navbar;

-(void)replaceTopViewController:(UIViewController *)vc animated:(BOOL)animated;
-(void)navigateToPath:(NSString *)path animated:(BOOL)animated;
-(void)configUI;

@end
