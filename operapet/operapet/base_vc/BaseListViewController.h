//
//  BaseListViewController.h
//  myislands
//
//  Created by alan tsoi on 3/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "BaseListCellFactory.h"
#import "OPViewController.h"

@interface BaseListViewController : OPViewController

@property (weak, nonatomic) IBOutlet UICollectionView *defaultListView;
@property BaseListCellFactory *factory;
@property NSArray *models;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomSpace;

-(UICollectionView *)listView;
-(BaseListCellFactory *)createFactory;

@end
