//
//  BaseNavigationController.m
//  megaman
//
//  Created by Duncan Wong on 12/7/2016.
//  Copyright © 2016 LiSeng. All rights reserved.
//

#import "BaseNavigationController.h"
#import "BaseViewController.h"
#import "MIToolBar.h"

#import "SlideNavigationController.h"
#import "SessionManager.h"





@interface BaseNavigationController ()


@end

@implementation BaseNavigationController


+ (instancetype)sharedInstance {
    static id sharedInstance = NULL;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(id) init {
    self = [super init];
//    self = [super initWithNavigationBarClass:[MINavigationBar class] toolbarClass:[MIToolBar class]];
    [self configureSlideMenu];
    [self configureToolbar];
   
    return self;
}

- (void)configureToolbar {
    [self.toolbar setTranslucent:NO];
    [self.toolbar setClipsToBounds:YES];
}

- (void)configureSlideMenu {
   
}

- (void)configUI {
    [self.navigationBar setAlpha:1.0];
    [self.navigationBar setTranslucent:NO];
    [self.navigationItem setHidesBackButton:YES];
    
    UIToolbar* toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    toolbar.barStyle = UIBarStyleDefault;
    toolbar.items = [NSArray arrayWithObjects:
                     [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                     [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismissKeyboard:)],
                     nil];
    [toolbar sizeToFit];
    [[UITextView appearance]setInputAccessoryView:toolbar];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

- (void)dismissKeyboard:(UIBarButtonItem *)item {
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (MINavigationBar *)navbar {
    return (MINavigationBar *)self.navigationBar;
}

-(void)replaceTopViewController:(UIViewController *)vc animated:(BOOL)animated {
    NSMutableArray *vcs = [NSMutableArray arrayWithArray:[BaseNavigationController sharedInstance].viewControllers];
    [vcs removeLastObject];
    [vcs addObject:vc];
    [self setViewControllers:vcs animated:animated];
}

-(void)navigateToPath:(NSString *)path animated:(BOOL)animated {

}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return [self.topViewController supportedInterfaceOrientations];
}

- (BOOL)shouldAutorotate {
    return [self.topViewController shouldAutorotate];
}

@end
