//
//  BaseViewController.m
//  megaman
//
//  Created by Duncan Wong on 12/7/2016.
//  Copyright © 2016 LiSeng. All rights reserved.
//

#import "BaseViewController.h"
#import "MBProgressHUD.h"
#import "BaseNavigationController.h"
#import "Constants.h"
#import "SDWebImageManager.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    return [super initWithNibName:[self getNibName] bundle:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configUI];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)configUI {
    [self configToolBar];
    
    self.navigationItem.leftBarButtonItem = nil;
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationItem setTitle:[self getTitle]];
    [self.navigationItem setRightBarButtonItems:[self getRightBarButtonItems]];
    
    if(self.navigationController.viewControllers.count > 1) {
        [self showBackButton];
    }
}

- (void)configToolBar {
    UIButton *menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuBtn setBackgroundImage:[UIImage imageNamed:@"btn_menu"] forState:UIControlStateNormal];
    [menuBtn setFrame:CGRectMake(0, 0, 25, 25)];
    [menuBtn addTarget:self action:@selector(onRightToolbarButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * menuBarBtn = [[UIBarButtonItem alloc]initWithCustomView:menuBtn];
    
    UIButton *bookmarkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [bookmarkBtn setBackgroundImage:[UIImage imageNamed:@"btn_bookmark_list"] forState:UIControlStateNormal];
    [bookmarkBtn setFrame:CGRectMake(0, 0, 25, 25)];
    [bookmarkBtn addTarget:self action:@selector(onLeftToolbarButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * bookmarkBarBtn = [[UIBarButtonItem alloc]initWithCustomView:bookmarkBtn];
    
    UIButton *appNameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [appNameBtn setBackgroundImage:[UIImage imageNamed:@"title"] forState:UIControlStateNormal];
    [appNameBtn setFrame:CGRectMake(0, 0, 96, 26)];
    [appNameBtn addTarget:self action:@selector(onClickAppNameButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarItem *appNameBarBtn = [[UIBarButtonItem alloc]initWithCustomView:appNameBtn];
    
    [self setToolbarItems:[NSArray arrayWithObjects:
                           bookmarkBarBtn,
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           appNameBarBtn,
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           menuBarBtn,
                           nil] animated:NO];
}

-(void)constrainView:(UIView *) view equalView:(UIView *)view2{
    [view setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *con1 = [NSLayoutConstraint constraintWithItem:view2 attribute:NSLayoutAttributeCenterX relatedBy:0 toItem:view attribute:NSLayoutAttributeCenterX multiplier:1 constant:0];
    NSLayoutConstraint *con2 = [NSLayoutConstraint constraintWithItem:view2 attribute:NSLayoutAttributeCenterY relatedBy:0 toItem:view attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
    NSLayoutConstraint *con3 = [NSLayoutConstraint constraintWithItem:view2 attribute:NSLayoutAttributeWidth relatedBy:0 toItem:view attribute:NSLayoutAttributeWidth multiplier:1 constant:0];
    NSLayoutConstraint *con4 = [NSLayoutConstraint constraintWithItem:view2 attribute:NSLayoutAttributeHeight relatedBy:0 toItem:view attribute:NSLayoutAttributeHeight multiplier:1 constant:0];
    NSArray *constraints = @[con1,con2,con3,con4];
    [self.view addConstraints:constraints];
}

-(void)constrainView:(UIView *) view inScrollView:(UIView *)scrollView{
    [view setTranslatesAutoresizingMaskIntoConstraints:NO];
    [scrollView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    NSLayoutConstraint *con1 = [NSLayoutConstraint constraintWithItem:scrollView attribute:NSLayoutAttributeTop relatedBy:0 toItem:view attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    NSLayoutConstraint *con2 = [NSLayoutConstraint constraintWithItem:scrollView attribute:NSLayoutAttributeBottom relatedBy:0 toItem:view attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    NSLayoutConstraint *con3 = [NSLayoutConstraint constraintWithItem:scrollView attribute:NSLayoutAttributeLeading relatedBy:0 toItem:view attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
    NSLayoutConstraint *con4 = [NSLayoutConstraint constraintWithItem:scrollView attribute:NSLayoutAttributeTrailing relatedBy:0 toItem:view attribute:NSLayoutAttributeTrailing multiplier:1 constant:0];
    NSArray *constraints = @[con1,con2,con3,con4];
    [self.view addConstraints:constraints];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.0]];
}

-(void)leftNavBarButtonPressed {
    
}

-(void)showLeftNavBarButtonWithImageNamed:(NSString *)imgName withSize:(CGSize)size {
    self.navigationItem.leftBarButtonItem = [self barButtonItemWithTarget:self selector:@selector(leftNavBarButtonPressed) imageName:imgName size:size];
}

-(void)onClickAppNameButton:(id)sender{

}

-(void)onLeftToolbarButtonPressed:(id)sender {

}

-(void)onRightToolbarButtonPressed:(id)sender {

}

- (void)stylizeTextField:(UITextField *)txtFld {
//    txtFld.layer.borderColor = color_main_border.CGColor;
    txtFld.layer.masksToBounds=YES;
    txtFld.layer.borderWidth= 0.5f;;
}

- (void)showBackButton {
    self.navigationItem.leftBarButtonItem = [self barButtonItemWithTarget:self selector:@selector(backButtonPressed) imageName:[self backButtonImageName] size:[self backButtonSize]];
}

- (NSString *)backButtonImageName {
    return @"";
}

- (CGSize)backButtonSize {
    return CGSizeZero;
}

- (void)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showRightBarButtonItems:(NSArray *)items {
    self.navigationController.navigationItem.rightBarButtonItems = items;
}

- (UIBarButtonItem *)barButtonItemWithTarget:(id)target selector:(SEL)selector imageName:(NSString *)imageName size:(CGSize)size {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *btnImg = [UIImage imageNamed:imageName];
    CGRect lframe = CGRectMake(0, 0, size.width, size.height);
    button.contentMode = UIViewContentModeScaleAspectFit;
    button.frame = lframe;
    [button setBackgroundImage:btnImg forState:UIControlStateNormal];
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    return [[UIBarButtonItem alloc]initWithCustomView:button];
}

- (void)popViewController {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[BaseNavigationController sharedInstance]popViewControllerAnimated:YES];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString*)getNibName{
    return @"BaseViewController";
}

- (NSString*)getTitle{
    return @"";
}

- (NSArray *)getRightBarButtonItems {
    return [NSArray array];
}

- (void)updateUI{
    
}

- (void)updateData:(BOOL) forceUpdate{
    
}

- (void)showProgressHUD {
    if([MBProgressHUD HUDForView:self.view]) return;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void)hideProgressHUD {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (void)showMessageDialog:(NSString *)msg
           buttonPositive:(NSString *)btnPositive
          positiveHandler:(MessageDialogPositiveHandler)positiveHandleBlock
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        MessageDialogAlertController *ac = [MessageDialogAlertController alertControllerWithTitle:NULL
                                                                                          message:msg
                                                                                      btnPositive:btnPositive != NULL ? btnPositive : NSLocalizedString(@"msg_ok", nil)
                                                                                    btnNevigative: NULL
                                                                           btnPositiveHandleBlock:positiveHandleBlock
                                                                         btnNevigativeHandleBlock: NULL];
        
        [self presentViewController:ac animated:YES completion:nil];
    }];
}

- (void)showMessageDialog:(NSString *)msg
           buttonPositive:(NSString *)btnPositive
           buttonNegative:(NSString *)btnNevigative
          positiveHandler:(MessageDialogPositiveHandler)positiveHandleBlock
          negativeHandler:(MessageDialogNegativeHandler)nevigativeHandleBlock
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        MessageDialogAlertController *ac = [MessageDialogAlertController alertControllerWithTitle:@""
                                                                                          message:msg != NULL ? msg : NSLocalizedString(@"msg_error", nil)
                                                                                      btnPositive:btnPositive != NULL ? btnPositive : NSLocalizedString(@"msg_reload", nil)
                                                                                    btnNevigative:btnNevigative != NULL ? btnNevigative : NSLocalizedString(@"msg_cancel", nil)
                                                                           btnPositiveHandleBlock:positiveHandleBlock
                                                                         btnNevigativeHandleBlock:nevigativeHandleBlock];
        
        [self presentViewController:ac animated:YES completion:nil];
    }];
}

- (void)showInputDialog:(NSString *)message
      inputPlaceHolders:(NSArray *)placeholders
          inputsHandler:(MessageDialogInputsHandler)inputsHandler
      nevigativeHandler:(MessageDialogNegativeHandler)negativeHandler
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        MessageDialogAlertController *ac;
        ac = [MessageDialogAlertController alertControllerWithTitle:@"" message:message inputPlaceholders:placeholders
                                                  inputsHandleBlock:inputsHandler btnNevigativeHandleBlock:negativeHandler];
        [self presentViewController:ac animated:YES completion:nil];
    }];
}


- (void)showInputDialog:(NSString *)message
           inputHandler:(MessageDialogInputHandler)inputHandler
      nevigativeHandler:(MessageDialogNegativeHandler)negativeHandler
{
    [self showInputDialog:message
             keyboardType:UIKeyboardTypeDefault
             inputHandler:inputHandler
        nevigativeHandler:negativeHandler];
}

- (void)showInputDialog:(NSString *)message
           keyboardType:(UIKeyboardType)keyboardType
           inputHandler:(MessageDialogInputHandler)inputHandler
      nevigativeHandler:(MessageDialogNegativeHandler)negativeHandler
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        MessageDialogAlertController *ac;
        ac = [MessageDialogAlertController alertControllerWithTitle:@"" message:message
                                                       keyboardType:keyboardType btnPositive:ls(@"msg_ok") btnNevigative:ls(@"msg_cancel")
                                                   inputHandleBlock:inputHandler btnNevigativeHandleBlock:negativeHandler];
        
        [self presentViewController:ac animated:YES completion:nil];
    }];
}

- (void)showSelectionWithTitle:(NSString *)title
                       message:(NSString *)msg
                          btns:(NSArray *)btns
                actionHandlers:(NSArray *)actionHandleBlocks
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        NSMutableArray *_btns = [NSMutableArray arrayWithArray:btns];
        [_btns addObject:NSLocalizedString(@"msg_cancel", NULL)];
        
        
        ListDialogAlertController *ac = [ListDialogAlertController alertControllerWithTitle:title != NULL ? title : @""
                                                                                    message:msg != NULL ? msg : @""
                                                                                        btn:_btns
                                                                         actionHandleBlocks:actionHandleBlocks];
        
        [self presentViewController:ac animated:YES completion:nil];
    }];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate {
    return NO;
}

@end
