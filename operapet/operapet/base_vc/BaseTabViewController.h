//
//  BaseTabViewController.h
//  megaman
//
//  Created by Duncan Wong on 12/7/2016.
//  Copyright © 2016 LiSeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface BaseTabViewController : BaseViewController <UITabBarControllerDelegate>

@property (strong, nonatomic) IBOutlet UITabBarController *tabBarController;
@property NSMutableArray *vcArray;

-(void)setViewControllers:(NSArray *)viewControllers;

@end
