//
//  BaseListViewController.m
//  myislands
//
//  Created by alan tsoi on 3/9/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#import "BaseListViewController.h"

@interface BaseListViewController ()

@end

@implementation BaseListViewController

-(NSString *)getNibName {
    return @"BaseListViewController";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.models = [NSArray array];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configUI {
    [super configUI];
    self.factory = [self createFactory];
    self.listView.backgroundColor = [UIColor clearColor];
}

- (UICollectionView *)listView {
    return self.defaultListView;
}

- (BaseListCellFactory *)createFactory {
    return [[BaseListCellFactory alloc]initWithCollectionView:self.listView];
}

- (void)test {
    NSMutableArray *models = [NSMutableArray array];
    
    Renderer defaultRenderer = ^UICollectionViewCell *(CellModel *model, UICollectionView *parent, NSIndexPath *indexPath) {
        UICollectionViewCell *cell = [parent dequeueReusableCellWithReuseIdentifier:@"BaseListCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    };
    
    CellModel *model;
    model = [CellModel modelWithData:nil renderer:defaultRenderer action:nil];
    [models addObject:model];
    model = [CellModel modelWithData:nil renderer:defaultRenderer action:nil];
    [models addObject:model];
    
    self.factory.models = models;
}

- (NSLayoutConstraint *)constraintBottomSpace {
    return self.bottomSpace;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
