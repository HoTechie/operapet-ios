//
//  BaseTabViewController.m
//  megaman
//
//  Created by Duncan Wong on 12/7/2016.
//  Copyright © 2016 LiSeng. All rights reserved.
//

#import "BaseTabViewController.h"

@interface BaseTabViewController (){
    NSString* _currentTitle;
    NSArray *_currentRightBarButtonItems;
}

@end

@implementation BaseTabViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    return [super initWithNibName:[self getNibName] bundle:nil];
}

- (instancetype)init{
    if (self = [super init]){
        _vcArray = [[NSMutableArray alloc] init];
        _currentTitle = @"";
        _currentRightBarButtonItems = [NSArray array];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.tabBarController.view setFrame: self.view.frame];
    [self.view addSubview: self.tabBarController.view];
    
    [self.tabBarController setDelegate:self];
    [self.tabBarController.tabBar setTranslucent:NO];
    [self.tabBarController.tabBar setBarTintColor:color_tab_bar_bg];
    
}

- (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size {
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSString*)getNibName{
    return @"BaseTabViewController";
}

- (NSString *)getTitle{
    return _currentTitle;
}

- (NSArray *)getRightBarButtonItems {
    return _currentRightBarButtonItems;
}

-(void)setViewControllers:(NSArray *)viewControllers {
    [self.tabBarController setViewControllers: self.vcArray];
    
    if(viewControllers.count) {
        [self tabBarController:self.tabBarController didSelectViewController:viewControllers[0]];
        [self.tabBarController.tabBar setSelectionIndicatorImage:[self imageWithColor:color_tab_bar_bg_highlight
                                                                             size:CGSizeMake([UIScreen mainScreen].bounds.size.width / viewControllers.count, self.tabBarController.tabBar.frame.size.height)]];
    }
}

#pragma mark - UITabBarDelegate
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    BaseViewController *vc = (BaseViewController*)viewController;
    _currentTitle = [vc getTitle];
    _currentRightBarButtonItems = [vc getRightBarButtonItems];
    [self.navigationItem setTitle:[self getTitle]];
    [self.navigationItem setRightBarButtonItems:[self getRightBarButtonItems]];
}


@end
