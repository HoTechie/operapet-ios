//
//  Constants.h
//  myislands
//
//  Created by alan tsoi on 5/8/2016.
//  Copyright © 2016 hotechie. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#import "SessionManager.h"

/* API */

#define IS_PRODUCTION   NO
#define API_PRODUCTION  @"http://www.myislands.com.hk/"
#define API_SANDBOX     @"http://dev13.palmary.hk/operapet/frontend/api/"
#define API_DEBUG       @"http://192.168.11.29:8080/api/"

/* Short-hands */

#define ls(x)       NSLocalizedString(x,nil)
#define MOD(a,b)    ((((a)%(b))+(b))%(b))

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


/* colors */


#define color_action_bar_bg  UIColorFromRGB(0xfafafa)
#define color_action_bar_text  UIColorFromRGB(0x000000)
#define color_action_bar_back_btn  UIColorFromRGB(0x666666)

#define color_theme_color  UIColorFromRGB(0xf5961e)
#define color_theme_color_lighter  UIColorFromRGB(0xf9f0e5)
#define color_theme_color_darker  UIColorFromRGB(0x954e16)
#define color_theme_color_2  UIColorFromRGB(0x64ae57)
#define color_theme_color_2_darker  UIColorFromRGB(0x348244)
#define color_theme_color_3  UIColorFromRGB(0x36b6b1)
#define color_theme_color_3_darker  UIColorFromRGB(0x066e71)
#define color_theme_color_4  UIColorFromRGB(0xe76052)
#define color_theme_color_4_darker  UIColorFromRGB(0xa83f38)
#define color_theme_color_5  UIColorFromRGB(0x974999)
#define color_theme_color_5_darker  UIColorFromRGB(0x6f3975)
#define color_theme_color_6  UIColorFromRGB(0xf9b11f)
#define color_theme_color_6_darker  UIColorFromRGB(0xd87e00)
#define color_theme_color_7  UIColorFromRGB(0xef7a6a)
#define color_theme_color_7_darker  UIColorFromRGB(0xa7443c)
#define color_theme_color_8  UIColorFromRGB(0x95c03f)
#define color_theme_color_8_darker  UIColorFromRGB(0x6d9332)
#define color_theme_color_9  UIColorFromRGB(0xeb5f0a)
#define color_theme_color_9_darker  UIColorFromRGB(0xbc3e09)


#define color_main_bg_color  UIColorFromRGB(0xfafafa)
#define color_main_content_overlay  UIColorFromRGB(0x731d1d1b)
#define color_main_text_color  UIColorFromRGB(0x333333)
#define color_main_text_color_lighter  UIColorFromRGB(0x999999)
#define color_main_text_color_invert  UIColorFromRGB(0xffffff)

#define color_main_border_color  UIColorFromRGB(0x6d6e71)
#define color_main_separator  UIColorFromRGB(0xcccccc)
#define color_edit_text_bg  UIColorFromRGB(0xffffff)

#define color_menu_bg_color  UIColorFromRGB(0xf5961e)
#define color_menu_element_bg_color  UIColorFromRGB(0xffffff)
#define color_menu_element_border_color  UIColorFromRGB(0xea841b)
#define color_menu_notification_count_bg  UIColorFromRGB(0xa94e19)

#define color_tab_bg_color  UIColorFromRGB(0xfafafa)
#define color_tab_line_color  UIColorFromRGB(0xcccccc)
#define color_tab_icon_color  UIColorFromRGB(0xb4b4b4)
#define color_tab_icon_color_highlight  UIColorFromRGB(0xf5961e)

#define color_tag_bg_color  UIColorFromRGB(0xcccccc)

#define color_post_category_border  UIColorFromRGB(0xcccccc)
#define color_post_category_text_color  UIColorFromRGB(0xb3b3b3)

#define color_wall_separator  UIColorFromRGB(0xcccccc)

#define color_pet_boy  UIColorFromRGB(0x6acff6)
#define color_pet_girl  UIColorFromRGB(0xfcacd4)

#define color_pet_pin_card_bg  UIColorFromRGB(0xf2f2f2)

/* dimesnsion */

#define dimen_action_bar_height         30
#define dimen_cell_padding              3
#define dimen_nav_padding               10
#define dimem_list_padding_x            25
#define dimem_list_padding_y            13
#define dimem_list_line_spacing         13
#define dimem_list_line_spacing_large   25


#endif /* Constants_h */
